﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Text.RegularExpressions;
using CIL_Allowance_Management_System;
using CIL_Allowance_Management_System.BackEnd;
using CIL_Allowance_Management_System.Front_End;

namespace CIL_Allowance_Management_System
{
    class AdminUsers
    {


        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string cilnumber { get; set; }

        public string IssueStatus { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get;  set; }
        public DateTime attendencedate { get;  set; }
        public string AttendenceStatus { get;  set; }
        
        public string DifferenceInTime { get;  set; }
        public DateTime TimeIn { get;  set; }
        public string Department { get; set; }

        public string Designation { get; set; }
        public int early { get; set; }
        public int late { get; set; }
        public double hours { get; set; }
        public double minutes { get; set; }
        public double seconds { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        
        public string RoleName { get; set; }

        


        public DateTime ArrivalTime { get; set; }

        connection con = new connection();
        public string AllowanceToBeGiven;
        public static string RoleType;

        public void getAdmin()
        {

            

            try 
            {
                con.conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlDataReader reader;
                SqlCommand cmd = new SqlCommand("GetLoginDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Parameters.AddWithValue("@Password", password);


                reader = cmd.ExecuteReader();
                //validateLoginInput();
                bool containsSpecialCharacter = username.Any(c => !char.IsLetter(c));
                if ((string.IsNullOrEmpty(username)))
                {
                    MessageBox.Show("Please enter value for Username", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (containsSpecialCharacter)
                {
                    MessageBox.Show("Username must not have special characters");

                }
                else if ((string.IsNullOrEmpty(password)))
                {
                    MessageBox.Show("Please enter value for Password", "Error");

                }

                else
                {
                    if (reader.HasRows)
                    {
                        

                        reader.Read();

                        if (reader[5].ToString() == "Admin")
                        {
                            RoleType = "Admin";
                           
                            
                        }
                        else if (reader[5].ToString() == "Accountant")
                        {
                            RoleType = "Accountant";
                         }
                        else
                        {
                            RoleType = "Guest";


                        }

                        Homepage oHomepage = new Homepage();
                        oHomepage.ShowDialog();

                        reader.Close();


                    }
                    else
                    {
                        MessageBox.Show("Username or Password Does not exist!!");
                    }

                }


                con.conn.Close();

            }
            catch(Exception ex)
            {
                throw ex;
            }







        }

        public bool ValidateLoginInput()
        {
            try
            {
                if ((string.IsNullOrEmpty(username)))
                {
                    MessageBox.Show("Please enter value for Username", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else if ((string.IsNullOrEmpty(password)))
                {
                    MessageBox.Show("Please enter value for Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }

        public void CreateAdmin()
        {
                       
            try
            {
                SqlCommand cmd;
                cmd = new SqlCommand("RegisterAdmin", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                con.conn.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;

                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                cmd.Parameters.AddWithValue("@RoleName", RoleName);

                cmd.ExecuteNonQuery();
                con.conn.Close();
            }



            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.conn.Close();
            }

        }
        DataTable table1 = new DataTable();
        public DataTable checkIfAdminExists()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getAdminUserDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Username", username);

                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable);

                return datatable;
                con.conn.Close();

                return table1;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.conn.Close();
            }


        }
        public void UpdateAdmin()
        {

            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("UpdateAdminUser", con.conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.InsertCommand = cmd;

                    cmd.Parameters.AddWithValue("@Username", username);
                    cmd.Parameters.AddWithValue("@Password", password);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);

                    cmd.ExecuteNonQuery();

                    con.conn.Close();
                
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.conn.Close();
            }


        }
        public void DeleteAdmin()
        {
            try
            {
                con.conn.Open();

                    SqlCommand cmd = new SqlCommand("DeleteAdminUser", con.conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    cmd.Parameters.AddWithValue("@Username", username);
                    adapter.InsertCommand = cmd;
                     cmd.ExecuteNonQuery();
                    
                    con.conn.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.conn.Close();
            }


        }
        public bool validateLoginInput()
        {
            try
            {
                if ((string.IsNullOrEmpty(username)))
                {
                    MessageBox.Show("Please enter value for Username", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else if ((string.IsNullOrEmpty(password)))
                {
                    MessageBox.Show("Please enter value for Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        DataTable datatable = new DataTable();

        public DataTable SearchEmployeeInAttendence()
        {
            try
            { con.conn.Open();
                
                SqlCommand cmd = new SqlCommand("get_Employee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo", cilnumber);
                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable);
                con.conn.Close();

                return datatable;
                sd.Dispose();
            }
            catch
            {
                throw;
            }

        }
        public DataTable SearchEmployeeByDepartmentInAttendence()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("SearchByDepartment", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Department", Department);
                cmd.ExecuteNonQuery();
                
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable);
                con.conn.Close();

                return datatable;
                sd.Dispose();



            }
            catch
            {
                throw;
            }

        }


        public DataTable getEmployee()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("getEmployees", con.conn);
                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.ExecuteNonQuery();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(datatable);

                con.conn.Close();

                return datatable;
            }
            catch
            {
                throw;
            }

        }


        DataTable dataTable5 = new DataTable();
        public DataTable addNameToCombobox()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("getemployee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable5);


                con.conn.Close();
                return dataTable5;
            }
            catch
            {
                throw;
            }

        }

        DataTable dataTable6 = new DataTable();
        public DataTable addDepartmentToCombobox()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("getemployee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd); 
                sd.Fill(dataTable6);

                con.conn.Close();

                return dataTable6;
            }
            catch
            {
                throw;
            }

        }

        public DataTable addDesignationToCombobox()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("getemployee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable6);

                con.conn.Close();

                return dataTable6;
            }
            catch
            {
                throw;
            }

        }

        // DataSet dataset2 = new DataSet();
        //public DataSet addDesignationToCombobox()
        //{
        //    try
        //    {
        //         con.conn.Open();

        //        SqlCommand cmd = new SqlCommand("getemployee", con.conn);
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter sd = new SqlDataAdapter(cmd);
        //              // sd.Fill(dataset);

        //        con.conn.Close();

        //      //  return dataset;
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}
        //public DataSet addAttendenceDateToCombobox()
        //{
        //    try
        //    {
        //        con.conn.Open();

        //        SqlCommand cmd = new SqlCommand("getemployeeinAttendence", con.conn);
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter sd = new SqlDataAdapter(cmd);
        //        //sd.Fill(dataset);


        //        con.conn.Close();
        //       // return dataset;
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}
        DataTable dataTable3 = new DataTable();
        public DataTable get_EmployeebyDepartmentInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("get_EmployeebyDepartmentInAttendence", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Department", Department);


                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable3);

                cmd.ExecuteNonQuery();

                return dataTable3;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        public DataTable get_EmployeebyDesignationInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("get_EmployeebyDesignationInAttendence", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Designation", Designation);


                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable3);

                cmd.ExecuteNonQuery();

                return dataTable3;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        public DataTable get_EmployeebyFullNameInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("get_EmployeebyFullNameInAttendence", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo", cilnumber);


                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable3);

                cmd.ExecuteNonQuery();

                return dataTable3;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        DataTable dataTable4 = new DataTable();
        public DataTable getArrivalTime()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getArrivalTime", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable4);

                cmd.ExecuteNonQuery();

                return dataTable4;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }

        }
        public void updateArrivaleTime()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd;
                cmd = new SqlCommand("updateArrivalTime", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;

                cmd.Parameters.AddWithValue("@hours", hours);
                cmd.Parameters.AddWithValue("@minutes", minutes);
                cmd.Parameters.AddWithValue("@seconds", seconds);


                adapter.InsertCommand.ExecuteNonQuery();

                con.conn.Close();

                return ;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        DataTable allowance = new DataTable();
        public DataTable getAllowanceFee()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getAllowanceFee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(allowance);

                cmd.ExecuteNonQuery();

                return allowance;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }

        }
        public void updateAllowanceFee()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd;
                cmd = new SqlCommand("updateAllowanceFee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;

                cmd.Parameters.AddWithValue("@Early", early);
                cmd.Parameters.AddWithValue("@Late", late);


                adapter.InsertCommand.ExecuteNonQuery();

                con.conn.Close();

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public DataTable get_EmployeeInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("get_EmployeeInAttendence", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo", cilnumber);

                cmd.Parameters.AddWithValue("@Department", Department);

                cmd.Parameters.AddWithValue("@AttendenceDate", attendencedate);

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable3);

                cmd.ExecuteNonQuery();

                return dataTable3;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        public DataTable getAdminList()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getAdminList", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable3);

                cmd.ExecuteNonQuery();

                return dataTable3;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        DataTable attendence_date = new DataTable();
        public DataTable get_EmployeebyAttendenceDateInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd;
                cmd = new SqlCommand("get_EmployeebyAttendenceDateInAttendence", con.conn);
                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("AttendenceDate", attendencedate);
                //cmd.Parameters.AddWithValue("@AttendenceDate", StartDate);
                //cmd.Parameters.AddWithValue("@AttendenceDate", EndDate);

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(attendence_date);

                cmd.ExecuteNonQuery();

                return attendence_date;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }
        DataTable allAttendence = new DataTable();
        public DataTable getallEmployeeInAttendence()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getemployeeinAttendence", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(attendence_date);

                cmd.ExecuteNonQuery();

                return attendence_date;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }


        DataTable table = new DataTable();
        
        public DataTable checkIfAttendenceExists()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getAttendencedetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("CILNo", cilnumber);
                cmd.Parameters.AddWithValue("AttendenceDate", attendencedate);
                cmd.Parameters.AddWithValue("SurName", Surname);
                cmd.Parameters.AddWithValue("MiddleName", MiddleName);
                cmd.Parameters.AddWithValue("LastName", LastName);
                cmd.Parameters.AddWithValue("Department", Department);
                cmd.Parameters.AddWithValue("Designation", Designation);

                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable);


                con.conn.Close();

                return datatable;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool save_Attendence()
        {


            try
            {
                con.conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd;
                cmd = new SqlCommand("SaveAttendence", con.conn);

                cmd.CommandType = CommandType.StoredProcedure;
                adapter.InsertCommand = cmd;

                cmd.Parameters.AddWithValue("@SurName", Surname);
                cmd.Parameters.AddWithValue("@MiddleName", MiddleName);
                cmd.Parameters.AddWithValue("@LastName", LastName);
                cmd.Parameters.AddWithValue("@CILNo", cilnumber);
                cmd.Parameters.AddWithValue("@Department", Department);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@DifferenceInTime", (DifferenceInTime));
                cmd.Parameters.AddWithValue("@AttendenceDate", attendencedate);
                cmd.Parameters.AddWithValue("@TimeIn", Convert.ToDateTime(TimeIn));
                cmd.Parameters.AddWithValue("@AttendenceStatus", AttendenceStatus);
                cmd.Parameters.AddWithValue("@AllowanceToBeGiven", AllowanceToBeGiven);
                cmd.Parameters.AddWithValue("@IssueStatus", IssueStatus);

                adapter.InsertCommand.ExecuteNonQuery();
                return true;
                con.conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                
                con.conn.Close();


            }

        }
        
    }
}








    

