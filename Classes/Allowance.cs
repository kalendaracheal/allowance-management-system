﻿using CIL_Allowance_Management_System.BackEnd;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIL_Allowance_Management_System.Classes
{
    public class Allowance
    {
        public string CILNo { get; set; }
        public string FullName { get; set; }

        public int Early { get; set; }
        public int Late { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime AttendenceDate { get; set; }
        public string FormNo { get; set; }

        public string Department { get; set; }
        public string Designation { get; set; }

        
        public int TotalAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        connection con = new connection();

        //populating with employee records on Name Combo
        DataTable dataTable4 = new DataTable();
        public DataTable AddEmployeeFullNameToNameCombo()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("getemployee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable4);

                return dataTable4;
                sd.Dispose();

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }

        //populating employee details to forms textboxes

        DataTable dataTable5 = new DataTable("Employees");

        public DataTable ShowEmployeeDetails()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("get_Employee", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo", CILNo);


                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable5);

                cmd.ExecuteNonQuery();

                return dataTable5;

            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }

        //loading attendance details to datagrid view

        DataTable dataTable6 = new DataTable();

        public DataTable GetAttendanceDetails()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("LoadAttendanceDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CILNo", CILNo);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);



                SqlDataAdapter sd6 = new SqlDataAdapter(cmd);
                sd6.Fill(dataTable6);


                return dataTable6;

            }
            catch (Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }

        //saving allowance details

        public void SaveAllowanceDetails()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("addAllowanceDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CILNo", CILNo);
                cmd.Parameters.AddWithValue("@FullName", FullName);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@Department", Department);
                cmd.Parameters.AddWithValue("@IssueDate", IssueDate);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@AmountGiven", TotalAmount);

                cmd.ExecuteNonQuery();

            }
            catch (Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }



        //updating attendance details with issueStatus
        public void UpdateAttendanceRowsWithIssuedStatus()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("updateAttendenceWithIssuedStatus", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo",CILNo);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);

                cmd.ExecuteNonQuery();

            }
            catch (Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }


        //.........................get attendance rows with issued statement...................................

        DataTable dt4 = new DataTable();
        public DataTable GetIssuedAttendenceRows()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("IssuedAttendenceDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CILNo", CILNo);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.ExecuteNonQuery();

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dt4);

                return dt4;


            }
            catch (Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }



        ////////////////////////////////////////////GENERAL REPORT////////////////////////////////////////////

        DataTable dt = new DataTable();
        public DataTable ReadAllowanceDetails()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("GetAllAllowanceDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate",EndDate);
                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dt);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }


        /////////////////////////get all allowance details..................
        ///
        DataTable dt1 = new DataTable();
        public DataTable ReadALLallowanceDetails()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("GetAllAllowanceDetails", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StartDate",StartDate);
                cmd.Parameters.AddWithValue("@EndDate",EndDate);
                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dt1);
                return dt1;
            }
            catch(Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }


        //.........................update allowance given...............................

        public void UpdateAllowanceGiven()
        {
            try
            {
                con.conn.Open();

                SqlCommand cmd = new SqlCommand("UpdateAllowanceGiven", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Early", Early);
                cmd.Parameters.AddWithValue("@Late", Late);

                cmd.ExecuteNonQuery();

            }
            catch (Exception x)
            {
                throw x;
            }
            finally
            {
                con.conn.Close();
            }
        }



        //........................................................

        DataTable dt5 = new DataTable();
        public DataTable GetAllowanceGiven()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("GetAllowanceGiven", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dt5);

                return dt5;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }
        }

    }


}
