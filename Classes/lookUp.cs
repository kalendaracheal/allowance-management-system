﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIL_Allowance_Management_System.BackEnd;

namespace CIL_Allowance_Management_System.Classes
{
    public class lookUp
    {
        public int DataId { get; set; }
        public string departmentItem { get; set; }
        public string designationItem { get; set; }

        connection con = new connection();

        //...............

        public void addDesignation()
        {


            con.conn.Open();


            try
            {
                SqlCommand cmd = new SqlCommand("addToDesignation", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DataId", DataId);
                cmd.Parameters.AddWithValue("@designationItem", designationItem);


                cmd.ExecuteNonQuery();


                con.conn.Close();

            }
            catch
            {
                throw;
            }

        }

        //----add roles to combobox-------------------------

        DataSet RolesDataSet = new DataSet();
        public DataSet addRolesCombo()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("getRoles", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;



                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(RolesDataSet);



                return RolesDataSet;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }

        }



        //-------------add departments to combo

        public void addDepartment()
        {



            con.conn.Open();


            try
            {
                SqlCommand cmd = new SqlCommand("addToDepartment", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DataId", DataId);
                cmd.Parameters.AddWithValue("@departmentItem", departmentItem);


                cmd.ExecuteNonQuery();


                con.conn.Close();
            }
            catch
            {

                throw;
            }
        }


        //add designation to Combo

        DataSet dataSet1 = new DataSet();
        public DataSet addDesignationCombo()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("addDesignation", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
               
                

                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataSet1);



                return dataSet1;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }

        }

        //add department to combo 
        DataSet dataSet2 = new DataSet();
        public DataSet addDepartmentCombo()
        {

            try
            {

                con.conn.Open();

                SqlCommand cmd = new SqlCommand("addDepartment", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter sd2 = new SqlDataAdapter(cmd);
                sd2.Fill(dataSet2);

                return dataSet2;


            }
            catch
            {
                throw;
            }
            finally
            {
                con.conn.Close();
            }

        }
        //...............


    }
}
