﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIL_Allowance_Management_System;
using System.Data;
using System.Data.SqlClient;
using CIL_Allowance_Management_System.BackEnd;

namespace CIL_Allowance_Management_System.Classes
{
    public class Employee
    {
        public string CILNo { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Age { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public string Gender { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }

        public DateTime RegDate { get; set; }
        public DateTime UpdateDate { get; set; }




        connection con = new connection();

       //Create Employee
       public void AddEmployee()
        {
            con.conn.Open();

            SqlCommand cmd = new SqlCommand("AddEmployee", con.conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CILNo", CILNo);
            cmd.Parameters.AddWithValue("@Surname", Surname);
            cmd.Parameters.AddWithValue("@MiddleName", MiddleName);
            cmd.Parameters.AddWithValue("@LastName", LastName);
            cmd.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
            cmd.Parameters.AddWithValue("@DateOfBirth", DateOfBirth);
            cmd.Parameters.AddWithValue("@Age", Age);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Location", Location);
            cmd.Parameters.AddWithValue("@Department", Department);
            cmd.Parameters.AddWithValue("@Designation", Designation);
            cmd.Parameters.AddWithValue("@Gender", Gender);
            cmd.Parameters.AddWithValue("@RegDate", RegDate);

            cmd.ExecuteNonQuery();
        }
        
        //Read Employees

        DataTable dataTable = new DataTable("Employees");

        public DataTable EmployeeTable()
        {

            try
            {
                con.conn.Open();


                SqlCommand cmd = new SqlCommand("display", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dataTable);


                return dataTable;
                
                
            }
            catch
            {
                throw;
            }

            con.conn.Close();
            
        }



        //updating employee

        public void UpdatingEmployee()
        {
            con.conn.Open();

            SqlCommand cmd = new SqlCommand("editEmployee", con.conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CILNo", CILNo);
            cmd.Parameters.AddWithValue("@Surname", Surname);
            cmd.Parameters.AddWithValue("@MiddleName", MiddleName);
            cmd.Parameters.AddWithValue("@LastName", LastName);
            cmd.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
            cmd.Parameters.AddWithValue("@DateOfBirth", DateOfBirth);
            cmd.Parameters.AddWithValue("@Age", Age);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Location", Location);
            cmd.Parameters.AddWithValue("@Department", Department);
            cmd.Parameters.AddWithValue("@Designation", Designation);
            cmd.Parameters.AddWithValue("@Gender", Gender);
            cmd.Parameters.AddWithValue("@UpdateDate", RegDate);


            cmd.ExecuteNonQuery();
            con.conn.Close();
        }

        //Delete Employee

        public void DeleteEmployee()
        {
            con.conn.Open();

            SqlCommand cmd = new SqlCommand("DeleteEmployee", con.conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CILNo", CILNo);
           
            cmd.ExecuteNonQuery();
            con.conn.Close();
        }


        //search  employee in edit form

      
        DataTable datatable3 = new DataTable();
        public DataTable addCilToCombo()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("display", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable3);

                con.conn.Close();


                return datatable3;
            }
            catch
            {
                throw;
            }

        }

        //.............CHECK EMPLOYEE............


        DataTable dt = new DataTable();
        public DataTable CheckIfEmployeeExists()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("checkIfEmployeeExists", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo",CILNo);
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(dt);

                con.conn.Close();

                return dt;
            }
            catch
            {
                throw;
            }

        }
        //..............................................
        DataTable datatable2 = new DataTable();
        public DataTable SearchEmployeeInEditForm()
        {
            try
            {


                con.conn.Open();

                SqlCommand cmd = new SqlCommand("SearchInEditForm", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CILNo", CILNo);
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(datatable2);

                con.conn.Close();

                return datatable2;

                
            }
            catch
            {
                throw;
            }

        }

        //Search in Employee exists
        DataTable table1 = new DataTable();
        public DataTable checkIfEmployeeExists()
        {
            try
            {
                con.conn.Open();
                SqlCommand cmd = new SqlCommand("checkIfEmployeeExists", con.conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CILNo", CILNo);

                cmd.ExecuteNonQuery();
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                sd.Fill(table1);

                return table1;
               
                con.conn.Close();

                
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.conn.Close();
            }


        }

    }

}
