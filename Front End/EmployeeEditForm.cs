﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class EmployeeEditForm : MetroFramework.Forms.MetroForm
    {
        public EmployeeEditForm()
        {
            InitializeComponent();
        }

        private void EmployeeEditForm_Load(object sender, EventArgs e)
        {
           


            lookUp OlookUp = new lookUp();


            Designation_tx.DataSource = OlookUp.addDesignationCombo().Tables[0];
            Designation_tx.DisplayMember = "designationItem";
            Designation_tx.ValueMember = "DataId";




            //...........................................................................

            lookUp OlookUp2 = new lookUp();


            Department_tx.DataSource = OlookUp2.addDepartmentCombo().Tables[0];
            Department_tx.DisplayMember = "departmentItem";
            Department_tx.ValueMember = "DataId";


            //....................................................
            Employee OEmployee = new Employee();

            foreach (DataRow dr in OEmployee.addCilToCombo().Rows)
            {
                CILNo_tx.Items.Add(dr["CILNo"].ToString());
            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Update_Click(object sender, EventArgs e)
        {
            try
            {
                Employee OEmployee = new Employee();

                if (Age_tx.Text == string.Empty)
                {
                    MessageBox.Show("Fill the form please");

                    return;
                }

                OEmployee.CILNo = CILNo_tx.Text;
                OEmployee.Surname = Surname_tx.Text;
                OEmployee.MiddleName = MiddleName_tx.Text;
                OEmployee.LastName = LastName_tx.Text;
                OEmployee.PhoneNumber = PhoneNumber_tx.Text;

                OEmployee.DateOfBirth = Convert.ToDateTime(DateOfBirth_tx.Value.ToShortDateString());

                OEmployee.DateOfBirth = DateTime.Parse(DateOfBirth_tx.Value.ToShortDateString());
                OEmployee.Gender = Gender_tx.Text;
                OEmployee.Designation = Designation_tx.Text;

                OEmployee.Department = Department_tx.Text;


                OEmployee.RegDate = DateTime.Now;

                OEmployee.Location = Location_tx.Text;
                OEmployee.Age = double.Parse(Age_tx.Text);
                OEmployee.Email = Email_tx.Text;




                if (OEmployee.CILNo == "")
                {
                    MessageBox.Show("Please enter CIL Number");
                }
                else if (OEmployee.Surname == "")
                {
                    MessageBox.Show("Please enter Surname");
                }
                else if (OEmployee.LastName == "")
                {
                    MessageBox.Show("Please enter Last name");
                }
                else if (OEmployee.PhoneNumber == "")
                {
                    MessageBox.Show("Please enter Phone number");
                }
                else if (OEmployee.Department == "")
                {
                    MessageBox.Show("Please enter Department");
                }
                else if (OEmployee.Designation == "")
                {
                    MessageBox.Show("Please enter Designation");
                }

                else if (OEmployee.Gender == "")
                {
                    MessageBox.Show("Please enter Gender");
                }
                else if (OEmployee.CheckIfEmployeeExists().Rows.Count < 1)
                {
                    MessageBox.Show("Employee does not exist");
                    return;
                }
                else
                {

                    OEmployee.UpdatingEmployee();

                     CILNo_tx.Text = "";
                     Surname_tx.Text = "";
                     MiddleName_tx.Text = "";
                     LastName_tx.Text = "";
                     PhoneNumber_tx.Text = "";
                    //OEmployee.DateOfBirth = DateOfBirth_tx.Value.ToShortDateString();
                     Gender_tx.Text = "";
                     Designation_tx.Text = "";

                     Department_tx.Text ="";
                    //OEmployee.RegDate = UpdateDate_tx.Value.ToShortDateString();
                     Location_tx.Text = "";
                    //Age_tx.Text = "";
                     Email_tx.Text = "";

                    MessageBox.Show(OEmployee.Surname + " " + OEmployee.LastName + ", " + "is successfully Updated");

                }

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void DateOfBirth_tx_ValueChanged(object sender, EventArgs e)
        {

            DateTime from = DateOfBirth_tx.Value;
            DateTime to = DateTime.Now;
            TimeSpan Tspan = to - from;
            double days = Tspan.TotalDays;
            Age_tx.Text = (days / 365).ToString("0");
        }

        private void Age_tx_KeyPress(object sender, KeyPressEventArgs e)
        {
            // USER SHOULD PRESS NUMBER,DELETE AND BACKSPACE KEYBOARD KEYS
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void PhoneNumber_tx_Click(object sender, EventArgs e)
        {

        }

        private void PhoneNumber_tx_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
                DialogResult dia = MessageBox.Show("digits only");
            }
        }

        private void Search_Click(object sender, EventArgs e)
        {
           
            
        }

        private void CILNo_tx_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee OEmployee2 = new Employee();

            OEmployee2.CILNo = CILNo_tx.SelectedItem.ToString();


            OEmployee2.SearchEmployeeInEditForm();

            foreach (DataRow dr in OEmployee2.SearchEmployeeInEditForm().Rows)
            {
                Surname_tx.Text = dr["SurName"].ToString();
                MiddleName_tx.Text = dr["MiddleName"].ToString();
                LastName_tx.Text = dr["LastName"].ToString();
                Gender_tx.Text = dr["Gender"].ToString();
                Age_tx.Text = dr["Age"].ToString();
                Email_tx.Text = dr["Email"].ToString();
                Location_tx.Text = dr["Location"].ToString();
                Department_tx.Text = dr["Department"].ToString();
                Designation_tx.Text = dr["Designation"].ToString();
                PhoneNumber_tx.Text = dr["PhoneNumber"].ToString();
                DateOfBirth_tx.Text = dr["DateOfBirth"].ToString();


            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Employee OEmployee = new Employee();

                if (Age_tx.Text == string.Empty)
                {
                    MessageBox.Show("Fill the form please");

                    return;
                }

                OEmployee.CILNo = CILNo_tx.Text;
                OEmployee.Surname = Surname_tx.Text;
                OEmployee.MiddleName = MiddleName_tx.Text;
                OEmployee.LastName = LastName_tx.Text;
                OEmployee.PhoneNumber = PhoneNumber_tx.Text;
                OEmployee.DateOfBirth = Convert.ToDateTime(DateOfBirth_tx.Value.ToShortDateString());
                OEmployee.Gender = Gender_tx.Text;
                OEmployee.Designation = Designation_tx.Text;

                OEmployee.Department = Department_tx.Text;
                OEmployee.RegDate = DateTime.Now;
                OEmployee.Location = Location_tx.Text;
                OEmployee.Age = double.Parse(Age_tx.Text);
                OEmployee.Email = Email_tx.Text;




                if (OEmployee.CILNo == "")
                {
                    MessageBox.Show("Please enter CIL Number");
                }
                else if (OEmployee.Surname == "")
                {
                    MessageBox.Show("Please enter Surname");
                }
                else if (OEmployee.LastName == "")
                {
                    MessageBox.Show("Please enter Last name");
                }
                else if (OEmployee.PhoneNumber == "")
                {
                    MessageBox.Show("Please enter Phone number");
                }
                else if (OEmployee.Department == "")
                {
                    MessageBox.Show("Please enter Department");
                }
                else if (OEmployee.Designation == "")
                {
                    MessageBox.Show("Please enter Designation");
                }

                else if (OEmployee.Gender == "")
                {
                    MessageBox.Show("Please enter Gender");
                }
                else
                {

                    OEmployee.UpdatingEmployee();

                    CILNo_tx.Text = "";
                    Surname_tx.Text = "";
                    MiddleName_tx.Text = "";
                    LastName_tx.Text = "";
                    PhoneNumber_tx.Text = "";
                    //OEmployee.DateOfBirth = DateOfBirth_tx.Value.ToShortDateString();
                    Gender_tx.Text = "";
                    Designation_tx.Text = "";

                    Department_tx.Text = "";
                    //OEmployee.RegDate = UpdateDate_tx.Value.ToShortDateString();
                    Location_tx.Text = "";
                    //Age_tx.Text = "";
                    Email_tx.Text = "";

                    MessageBox.Show(OEmployee.Surname + " " + OEmployee.LastName + ", " + "is successfully Updated");

                }

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void Gender_tx_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
