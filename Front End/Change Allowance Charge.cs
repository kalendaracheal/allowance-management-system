﻿using CIL_Allowance_Management_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class Change_Allowance_Charge : MetroFramework.Forms.MetroForm
    {
       
       
        public Change_Allowance_Charge()
        {
            InitializeComponent();
        }

        private void Change_Allowance_Charge_Load(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChangeArrivalTime_Click(object sender, EventArgs e)
        {

            try
            {
                Allowance OAllowance = new Allowance();


                if (metroTextBox1.Text == "")
                {
                    MessageBox.Show("All feilds are required");
                }
                else if (metroTextBox2.Text == "")
                {
                    MessageBox.Show("All fields are required");
                }
                else
                {
                    OAllowance.Early = int.Parse(metroTextBox1.Text);
                    OAllowance.Late = int.Parse(metroTextBox2.Text);

                    OAllowance.UpdateAllowanceGiven();

                    MessageBox.Show("Allowance successfully changed");

                    metroTextBox1.Text = "";
                    metroTextBox2.Text = "";
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

            

           

        }
    }
}
