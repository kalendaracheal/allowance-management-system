﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class EditAdmin : Form
    {
        public EditAdmin()
        {
            InitializeComponent();
        }

        
        private void EditAdmin_Load(object sender, EventArgs e)
        {
            txtPhoneNumber.MaxLength = 10;
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                DataTable table2 = new DataTable();
                OAdminUsers.username = txtUsername.Text;
                OAdminUsers.password = txtPassword.Text;
                OAdminUsers.email = txtEmail.Text;
                OAdminUsers.phoneNumber = txtPhoneNumber.Text;

                if (OAdminUsers.checkIfAdminExists().Rows.Count < 1)
                {
                    MessageBox.Show(OAdminUsers.username + " doesn't Exist");

                }
                else
                {
                    bool containsSpecialCharacter = OAdminUsers.username.Any(c => !char.IsLetter(c));
                    bool isEmail = Regex.IsMatch(OAdminUsers.email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    bool containsDigits = OAdminUsers.phoneNumber.Any(c => !char.IsDigit(c));

                    if ((string.IsNullOrEmpty(OAdminUsers.username)))
                    {
                        MessageBox.Show("Please enter value for Username !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (containsSpecialCharacter)
                    {
                        MessageBox.Show("Username must not have special characters !! ");
                        return;
                    }
                    else if ((string.IsNullOrEmpty(OAdminUsers.password)))
                    {
                        MessageBox.Show("Please enter value for password !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if ((string.IsNullOrEmpty(OAdminUsers.email)))
                    {
                        MessageBox.Show("Please enter value for Email !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (!(isEmail))
                    {
                        MessageBox.Show("Email should be written with valid characters !! ");
                        return;
                    }
                    else if ((string.IsNullOrEmpty(OAdminUsers.phoneNumber)))
                    {
                        MessageBox.Show("Please enter value for Phone Number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (containsDigits)
                    {
                        MessageBox.Show("Phone Number should be a number !! ");
                        return;
                    }
                    else
                    {
                        OAdminUsers.UpdateAdmin();

                        MessageBox.Show(OAdminUsers.username + " Updated Successfully", "  ClinicMaster ADMIN ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUsername.Clear();
                        txtPassword.Clear();
                        txtEmail.Clear();
                        txtPhoneNumber.Clear();
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                AdminUsers oAdminUsers = new AdminUsers();
                DataTable table2 = new DataTable();
                oAdminUsers.username = txtUsername.Text;

                bool containsSpecialCharacter = oAdminUsers.username.Any(c => !char.IsLetter(c));
                DialogResult dr = MessageBox.Show("ARE YOU SURE YOU WANT TO DELETE USER ?", " ClinicMaster ADMIN", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (dr == DialogResult.Yes)
                {
                    if ((string.IsNullOrEmpty(oAdminUsers.username)))
                    {
                        MessageBox.Show("Please enter value for Username !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (containsSpecialCharacter)
                    {
                        MessageBox.Show("Username must not have special characters !! ");
                        return;
                    }
                    if (oAdminUsers.checkIfAdminExists().Rows.Count < 1)
                    {
                        MessageBox.Show("Admin Doesn't Exist");

                    }
                    else
                    {
                        oAdminUsers.DeleteAdmin();
                        MessageBox.Show(oAdminUsers.username + " Deleted Successfully", "  ClinicMaster ADMIN ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUsername.Clear();
                        txtPassword.Clear();
                        txtEmail.Clear();
                        txtPhoneNumber.Clear();

                    }

                }

                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ShowPassword_CheckedChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (ShowPassword.Checked)
                {
                    txtPassword.PasswordChar = '\0';
                }
                else
                {
                    txtPassword.PasswordChar = '*';
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
