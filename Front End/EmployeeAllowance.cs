﻿using CIL_Allowance_Management_System.Classes;
using DGVPrinterHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class EmployeeAllowance : MetroFramework.Forms.MetroForm
    {
        
        public EmployeeAllowance()
        {
            InitializeComponent();
            
        }

        private void EmployeeAllowance_Load(object sender, EventArgs e)
        {
            try
            {

                lookUp OlookUp = new lookUp();

                Designation_tx.DataSource = OlookUp.addDesignationCombo().Tables[0];
                Designation_tx.DisplayMember = "designationItem";
                Designation_tx.ValueMember = "DataId";




                //...........................................................................

                lookUp OlookUp2 = new lookUp();


                Department_tx.DataSource = OlookUp2.addDepartmentCombo().Tables[0];
                Department_tx.DisplayMember = "departmentItem";
                Department_tx.ValueMember = "DataId";

                //.....................................................................
                Allowance OAllowance = new Allowance();

                DataTable staff = OAllowance.AddEmployeeFullNameToNameCombo();
                staff.Rows.InsertAt(staff.NewRow(), 0);

                FullName_tx.DataSource = OAllowance.AddEmployeeFullNameToNameCombo();
                FullName_tx.DisplayMember = "FullName";
                FullName_tx.ValueMember = "CILNo";
            }
            catch(Exception x)
            {
                MessageBox.Show(x.Message);
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FullName_tx_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                Allowance OAllowance2 = new Allowance();
                OAllowance2.CILNo = FullName_tx.SelectedValue.ToString();

                OAllowance2.ShowEmployeeDetails();

                foreach (DataRow dr in OAllowance2.ShowEmployeeDetails().Rows)
                {
                    CIL_tx.Text = dr["CILNo"].ToString();
                    Department_tx.Text = dr["Department"].ToString();
                    Designation_tx.Text = dr["Designation"].ToString();
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                Allowance OAllowance2 = new Allowance();
                OAllowance2.CILNo = FullName_tx.SelectedValue.ToString();
                OAllowance2.StartDate = DateTime.Parse(StartDate_tx.Value.ToShortDateString());
                OAllowance2.EndDate = DateTime.Parse(EndDate_tx.Value.ToShortDateString());
               
                if (OAllowance2.GetIssuedAttendenceRows().Rows.Count > 0)
                {
                    MessageBox.Show("No Unpaid allowances");
                }
                if (OAllowance2.CILNo == string.Empty)
                {
                    MessageBox.Show("Please choose Employee Name");
                }
                else {

                    AttendanceResults_tx.DataSource = OAllowance2.GetAttendanceDetails();

                    totalAllowance.Text = "0";

                    for (int i = 0; i < AttendanceResults_tx.Rows.Count; i++)
                    {
                        if (AttendanceResults_tx.Rows[i].Cells["AllowanceToBeGiven"].Value != null)
                        {
                            totalAllowance.Text = Convert.ToString(double.Parse(totalAllowance.Text) + double.Parse(AttendanceResults_tx.Rows[i].Cells["AllowanceToBeGiven"].Value.ToString()));
                        }
                    }
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            
        }

        private void Total_tx_Click(object sender, EventArgs e)
        {
            
        }

       
        private void metroButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void totalAllowance_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                Allowance OAllowance3 = new Allowance();

                

                OAllowance3.CILNo = CIL_tx.Text;
                OAllowance3.FullName = FullName_tx.Text;
               
                OAllowance3.Designation = Designation_tx.Text;
                OAllowance3.Department = Department_tx.Text;
                OAllowance3.IssueDate = DateTime.Parse(IssueDate_tx.Value.ToShortDateString());
                OAllowance3.StartDate = DateTime.Parse(StartDate_tx.Value.ToShortDateString());
                OAllowance3.EndDate = DateTime.Parse(EndDate_tx.Value.ToShortDateString());

                if (CIL_tx.Text == String.Empty)
                {
                    MessageBox.Show("Please choose fullname");
                    return;
                }
                else if (OAllowance3.Designation == "")
                {
                    MessageBox.Show("Please enter Designation");
                }
                else if (OAllowance3.Department == "")
                {
                    MessageBox.Show("Please enter Department");
                }
                else if (OAllowance3.GetIssuedAttendenceRows().Rows.Count > 0)
                {
                    MessageBox.Show("No unpaid alllowance.");
                }
                else if(MessageBox.Show("You are about to make an irreversible action.Are you sure you want to save?", "Confirm Issueing", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    OAllowance3.TotalAmount = int.Parse(totalAllowance.Text);

                    OAllowance3.SaveAllowanceDetails();

                    OAllowance3.IssueDate = DateTime.Parse(IssueDate_tx.Value.ToShortDateString());
                    OAllowance3.StartDate = DateTime.Parse(StartDate_tx.Value.ToShortDateString());

                    OAllowance3.UpdateAttendanceRowsWithIssuedStatus();
                    MessageBox.Show("You have successfully issued allowance to" + OAllowance3.FullName);

                    CIL_tx.Text = "";
                    FullName_tx.Text = "";
                    Designation_tx.Text = "";
                    Department_tx.Text = "";
                    AttendanceResults_tx.DataSource = null;
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Allowance OAllowance = new Allowance();
            DGVPrinter printer = new DGVPrinter();

            printer.Title = FullName_tx.Text;
           
           // printer.SubTitle = "Allowance  from" + " " + OAllowance.StartDate + " " + "to" + " " + OAllowance.EndDate;
            printer.SubTitle = "Total Allowance Issued" + " = " + totalAllowance.Text + "UGX";
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Center;
            printer.Footer = "ClinicMaster Allowance Report"; //footer
            printer.FooterSpacing = 15;

            //land scape setting
            printer.printDocument.DefaultPageSettings.Landscape = true;
            printer.PrintDataGridView(AttendanceResults_tx);
        }
    }
}
