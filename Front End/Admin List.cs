﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class Admin_List : Form
    {
        public Admin_List()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            AdminUsers OAdminUsers = new AdminUsers();
 dataGridviewAdmin.DataSource = OAdminUsers.getAdminList();

        }

        private void dataGridviewAdmin_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Admin_List_Load(object sender, EventArgs e)
        {
            AdminUsers OAdminUsers = new AdminUsers();
            dataGridviewAdmin.DataSource = OAdminUsers.getAdminList();
        }
    }
}
