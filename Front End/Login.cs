﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CIL_Allowance_Management_System.Front_End;
using CIL_Allowance_Management_System.BackEnd;

namespace CIL_Allowance_Management_System
{
    public partial class Login : Form
    {
        public string username { get; private set; }
        public string password { get; private set; }

        connection con = new connection();
        public Login()
        {
            InitializeComponent();
        }

        

        private void Close_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }
        }
       
       
        public void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void Login_Load_1(object sender, EventArgs e)
        {
        
        }


        private void Loginbtn_Click(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                OAdminUsers.username = txtUsername.Text;
                OAdminUsers.password = txtPassword.Text;
                OAdminUsers.getAdmin();
                
              

                
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }
        }

       
        private void linkCreateAccount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {

                Admin_Users_Registration OAdmin_Users_Registration = new Admin_Users_Registration();
                OAdmin_Users_Registration.ShowDialog();

            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }      
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }
    }
}
