﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class EmployeeRegistrationForm : MetroFramework.Forms.MetroForm
    {
        public EmployeeRegistrationForm()
        {
            InitializeComponent();
        }

        private void EmployeeRegistrationForm_Load(object sender, EventArgs e)
        {
            lookUp OlookUp = new lookUp();


            Designation_tx.DataSource = OlookUp.addDesignationCombo().Tables[0];
            Designation_tx.DisplayMember = "designationItem";
            Designation_tx.ValueMember = "DataId";




            //...........................................................................

            lookUp OlookUp2 = new lookUp();


            Department_tx.DataSource = OlookUp2.addDepartmentCombo().Tables[0];
            Department_tx.DisplayMember = "departmentItem";
            Department_tx.ValueMember = "DataId";

        }

        private void Close_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

             private void Age_tx_Click(object sender, EventArgs e)
        {

        }

        private void DateOfBirth_tx_ValueChanged(object sender, EventArgs e)
        {

            DateTime from = DateOfBirth_tx.Value;
            DateTime to = DateTime.Now;
            TimeSpan Tspan = to - from;
            double days = Tspan.TotalDays;
            Age_tx.Text = (days / 365).ToString("0");
        }

        private void Age_tx_KeyPress(object sender, KeyPressEventArgs e)
        {
            // USER SHOULD PRESS NUMBER,DELETE AND BACKSPACE KEYBOARD KEYS
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void PhoneNumber_tx_KeyPress(object sender, KeyPressEventArgs e)
        {

            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
                DialogResult dia = MessageBox.Show("digits only");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Employee OEmployee = new Employee();

                if (Age_tx.Text == string.Empty)
                {
                    MessageBox.Show("Fill the form please");

                    return;
                }

                OEmployee.CILNo = CIL_tx.Text;
                OEmployee.Surname = Surname_tx.Text;
                OEmployee.MiddleName = MiddleName_tx.Text;
                OEmployee.LastName = LastName_tx.Text;
                OEmployee.PhoneNumber = PhoneNumber_tx.Text;
                OEmployee.DateOfBirth = DateTime.Parse(DateOfBirth_tx.Value.ToShortDateString());
                OEmployee.Gender = Gender_tx.Text;
                OEmployee.Designation = Designation_tx.Text;

                OEmployee.Department = Department_tx.Text;
                OEmployee.RegDate = DateTime.Parse(RegDate_tx.Value.ToShortDateString());
                OEmployee.Location = Location_tx.Text;
                OEmployee.Age = double.Parse(Age_tx.Text);
                OEmployee.Email = Email_tx.Text;




                if (OEmployee.CILNo == "")
                {
                    MessageBox.Show("Please enter CIL Number");
                }
               
                else if (OEmployee.Surname == "")
                {
                    MessageBox.Show("Please enter Surname");
                }
                else if (OEmployee.LastName == "")
                {
                    MessageBox.Show("Please enter Last name");
                }
                else if (OEmployee.PhoneNumber == "")
                {
                    MessageBox.Show("Please enter Phone number");
                }
                else if (OEmployee.Department == "")
                {
                    MessageBox.Show("Please enter Department");
                }
                else if (OEmployee.Designation == "")
                {
                    MessageBox.Show("Please enter Designation");
                }

                else if (OEmployee.Gender == "")
                {
                    MessageBox.Show("Please enter Gender");
                }
                else if (OEmployee.CheckIfEmployeeExists().Rows.Count > 0)
                {
                    MessageBox.Show("This Employee exists");

                    return;
                }
                {

                    OEmployee.AddEmployee();

                    CIL_tx.Text = "";
                    Surname_tx.Text = "";
                    MiddleName_tx.Text = "";
                    LastName_tx.Text = "";
                    PhoneNumber_tx.Text = "";
                    
                    Gender_tx.Text = "";
                    Designation_tx.Text = "";

                    Department_tx.Text = "";
                    //OEmployee.RegDate = UpdateDate_tx.Value.ToShortDateString();
                    Location_tx.Text = "";
                    Age_tx.Text = "";
                    Email_tx.Text = "";

                    MessageBox.Show(OEmployee.Surname + " " + OEmployee.LastName + ", " + "is successfully added");

                }

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void caption_Click(object sender, EventArgs e)
        {

        }
    }
}
