﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class EmployeeAllowance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.EndDate_tx = new System.Windows.Forms.DateTimePicker();
            this.StartDate_tx = new System.Windows.Forms.DateTimePicker();
            this.IssueDate_tx = new System.Windows.Forms.DateTimePicker();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.Designation_tx = new MetroFramework.Controls.MetroComboBox();
            this.Department_tx = new MetroFramework.Controls.MetroComboBox();
            this.CIL_tx = new MetroFramework.Controls.MetroTextBox();
            this.Surnam = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.caption = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.FullName_tx = new MetroFramework.Controls.MetroComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.totalAllowance = new MetroFramework.Controls.MetroLabel();
            this.AttendanceResults_tx = new MetroFramework.Controls.MetroGrid();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.AttendanceResults_tx)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel9.Location = new System.Drawing.Point(444, 178);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(68, 19);
            this.metroLabel9.TabIndex = 54;
            this.metroLabel9.Text = "End Date";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.Location = new System.Drawing.Point(225, 177);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(76, 19);
            this.metroLabel8.TabIndex = 53;
            this.metroLabel8.Text = "Start Date";
            // 
            // EndDate_tx
            // 
            this.EndDate_tx.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.EndDate_tx.Location = new System.Drawing.Point(518, 178);
            this.EndDate_tx.Name = "EndDate_tx";
            this.EndDate_tx.Size = new System.Drawing.Size(96, 20);
            this.EndDate_tx.TabIndex = 52;
            // 
            // StartDate_tx
            // 
            this.StartDate_tx.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.StartDate_tx.Location = new System.Drawing.Point(317, 177);
            this.StartDate_tx.Name = "StartDate_tx";
            this.StartDate_tx.Size = new System.Drawing.Size(101, 20);
            this.StartDate_tx.TabIndex = 51;
            // 
            // IssueDate_tx
            // 
            this.IssueDate_tx.Enabled = false;
            this.IssueDate_tx.Location = new System.Drawing.Point(535, 71);
            this.IssueDate_tx.Name = "IssueDate_tx";
            this.IssueDate_tx.Size = new System.Drawing.Size(200, 20);
            this.IssueDate_tx.TabIndex = 50;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(462, 116);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(89, 19);
            this.metroLabel7.TabIndex = 49;
            this.metroLabel7.Text = "Department";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(105, 125);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(88, 19);
            this.metroLabel6.TabIndex = 48;
            this.metroLabel6.Text = "Designation";
            // 
            // Designation_tx
            // 
            this.Designation_tx.Enabled = false;
            this.Designation_tx.FormattingEnabled = true;
            this.Designation_tx.ItemHeight = 23;
            this.Designation_tx.Items.AddRange(new object[] {
            "Programmer",
            "Marketier"});
            this.Designation_tx.Location = new System.Drawing.Point(199, 125);
            this.Designation_tx.Name = "Designation_tx";
            this.Designation_tx.Size = new System.Drawing.Size(160, 29);
            this.Designation_tx.TabIndex = 47;
            this.Designation_tx.UseSelectable = true;
            // 
            // Department_tx
            // 
            this.Department_tx.Enabled = false;
            this.Department_tx.FormattingEnabled = true;
            this.Department_tx.ItemHeight = 23;
            this.Department_tx.Items.AddRange(new object[] {
            "Software Department",
            "Technical Support"});
            this.Department_tx.Location = new System.Drawing.Point(575, 116);
            this.Department_tx.Name = "Department_tx";
            this.Department_tx.Size = new System.Drawing.Size(160, 29);
            this.Department_tx.TabIndex = 46;
            this.Department_tx.UseSelectable = true;
            // 
            // CIL_tx
            // 
            // 
            // 
            // 
            this.CIL_tx.CustomButton.Image = null;
            this.CIL_tx.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.CIL_tx.CustomButton.Name = "";
            this.CIL_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.CIL_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.CIL_tx.CustomButton.TabIndex = 1;
            this.CIL_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.CIL_tx.CustomButton.UseSelectable = true;
            this.CIL_tx.CustomButton.Visible = false;
            this.CIL_tx.Enabled = false;
            this.CIL_tx.Lines = new string[0];
            this.CIL_tx.Location = new System.Drawing.Point(199, 96);
            this.CIL_tx.MaxLength = 32767;
            this.CIL_tx.Name = "CIL_tx";
            this.CIL_tx.PasswordChar = '\0';
            this.CIL_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.CIL_tx.SelectedText = "";
            this.CIL_tx.SelectionLength = 0;
            this.CIL_tx.SelectionStart = 0;
            this.CIL_tx.ShortcutsEnabled = true;
            this.CIL_tx.Size = new System.Drawing.Size(160, 23);
            this.CIL_tx.TabIndex = 45;
            this.CIL_tx.UseSelectable = true;
            this.CIL_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.CIL_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Surnam
            // 
            this.Surnam.AutoSize = true;
            this.Surnam.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Surnam.Location = new System.Drawing.Point(133, 96);
            this.Surnam.Name = "Surnam";
            this.Surnam.Size = new System.Drawing.Size(49, 19);
            this.Surnam.TabIndex = 42;
            this.Surnam.Text = "CILNo";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(457, 72);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(72, 19);
            this.metroLabel2.TabIndex = 39;
            this.metroLabel2.Text = "IssueDate";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(121, 68);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(72, 19);
            this.metroLabel1.TabIndex = 38;
            this.metroLabel1.Text = "FullName";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(86, 600);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 16);
            this.label6.TabIndex = 64;
            this.label6.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // caption
            // 
            this.caption.AutoSize = true;
            this.caption.Font = new System.Drawing.Font("Consolas", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.caption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.caption.Location = new System.Drawing.Point(513, 598);
            this.caption.Name = "caption";
            this.caption.Size = new System.Drawing.Size(200, 18);
            this.caption.TabIndex = 63;
            this.caption.Text = "Your Health Data Partner";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(540, 555);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(128, 29);
            this.btnClose.TabIndex = 62;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FullName_tx
            // 
            this.FullName_tx.FormattingEnabled = true;
            this.FullName_tx.ItemHeight = 23;
            this.FullName_tx.Location = new System.Drawing.Point(199, 61);
            this.FullName_tx.Name = "FullName_tx";
            this.FullName_tx.Size = new System.Drawing.Size(158, 29);
            this.FullName_tx.TabIndex = 65;
            this.FullName_tx.UseSelectable = true;
            this.FullName_tx.SelectedIndexChanged += new System.EventHandler(this.FullName_tx_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Blue;
            this.button1.Location = new System.Drawing.Point(52, 555);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 29);
            this.button1.TabIndex = 66;
            this.button1.Text = "ISSUE";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(290, 224);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(128, 29);
            this.button2.TabIndex = 67;
            this.button2.Text = "LOAD";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // totalAllowance
            // 
            this.totalAllowance.AutoSize = true;
            this.totalAllowance.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.totalAllowance.Location = new System.Drawing.Point(444, 234);
            this.totalAllowance.Name = "totalAllowance";
            this.totalAllowance.Size = new System.Drawing.Size(49, 19);
            this.totalAllowance.TabIndex = 69;
            this.totalAllowance.Text = "..........";
            this.totalAllowance.Click += new System.EventHandler(this.totalAllowance_Click);
            // 
            // AttendanceResults_tx
            // 
            this.AttendanceResults_tx.AllowUserToResizeRows = false;
            this.AttendanceResults_tx.BackgroundColor = System.Drawing.Color.Silver;
            this.AttendanceResults_tx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AttendanceResults_tx.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.AttendanceResults_tx.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AttendanceResults_tx.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AttendanceResults_tx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AttendanceResults_tx.DefaultCellStyle = dataGridViewCellStyle2;
            this.AttendanceResults_tx.EnableHeadersVisualStyles = false;
            this.AttendanceResults_tx.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.AttendanceResults_tx.GridColor = System.Drawing.Color.Silver;
            this.AttendanceResults_tx.Location = new System.Drawing.Point(10, 261);
            this.AttendanceResults_tx.Name = "AttendanceResults_tx";
            this.AttendanceResults_tx.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AttendanceResults_tx.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.AttendanceResults_tx.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.AttendanceResults_tx.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AttendanceResults_tx.Size = new System.Drawing.Size(964, 288);
            this.AttendanceResults_tx.TabIndex = 70;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(320, 555);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(128, 29);
            this.button3.TabIndex = 71;
            this.button3.Text = "PRINT";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // EmployeeAllowance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 625);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.AttendanceResults_tx);
            this.Controls.Add(this.totalAllowance);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.FullName_tx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.caption);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.EndDate_tx);
            this.Controls.Add(this.StartDate_tx);
            this.Controls.Add(this.IssueDate_tx);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.Designation_tx);
            this.Controls.Add(this.Department_tx);
            this.Controls.Add(this.CIL_tx);
            this.Controls.Add(this.Surnam);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeAllowance";
            this.Text = "Employee Allowance";
            this.Load += new System.EventHandler(this.EmployeeAllowance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AttendanceResults_tx)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.DateTimePicker EndDate_tx;
        private System.Windows.Forms.DateTimePicker StartDate_tx;
        private System.Windows.Forms.DateTimePicker IssueDate_tx;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroComboBox Designation_tx;
        private MetroFramework.Controls.MetroComboBox Department_tx;
        private MetroFramework.Controls.MetroTextBox CIL_tx;
        private MetroFramework.Controls.MetroLabel Surnam;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label caption;
        private System.Windows.Forms.Button btnClose;
        private MetroFramework.Controls.MetroComboBox FullName_tx;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private MetroFramework.Controls.MetroLabel totalAllowance;
        private MetroFramework.Controls.MetroGrid AttendanceResults_tx;
        private System.Windows.Forms.Button button3;
    }
}