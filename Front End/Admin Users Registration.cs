﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using CIL_Allowance_Management_System.Front_End;
using CIL_Allowance_Management_System.BackEnd;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System
{
    public partial class Admin_Users_Registration : Form
    {
        public string email { get; private set; }
        public string phoneNumber { get; private set; }
        public string username { get; private set; }
        public string password { get; private set; }
        connection con = new connection();
        public Admin_Users_Registration()
        {
            InitializeComponent();
            
        }

        private void Close_Click(object sender, EventArgs e)
        {
            try
            {
                // Application.ExitThread();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Register_Click(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();

                OAdminUsers.username = txtUsername.Text;
                OAdminUsers.password = txtPassword.Text;
                OAdminUsers.email = txtEmail.Text;
                OAdminUsers.phoneNumber = txtPhoneNumber.Text;
                OAdminUsers.RoleName = txtroles.Text;
                bool isEmail = Regex.IsMatch(OAdminUsers.email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                bool containsSpecialCharacter = OAdminUsers.username.Any(c => !char.IsLetter(c));
                bool containsDigits = OAdminUsers.phoneNumber.Any(c => !char.IsDigit(c));
                if ((string.IsNullOrEmpty(OAdminUsers.username)))
                {
                    MessageBox.Show("Please enter value for Username !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (containsSpecialCharacter)
                {
                    MessageBox.Show("Username must not have special characters !! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else { }

                if (OAdminUsers.checkIfAdminExists().Rows.Count < 1)
                {
                   
                    if ((string.IsNullOrEmpty(OAdminUsers.password)))
                    {
                        MessageBox.Show("Please enter value for Password !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if ((string.IsNullOrEmpty(OAdminUsers.email)))
                    {
                        MessageBox.Show("Please enter value for Email !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (!(isEmail))
                    {
                        MessageBox.Show("Email should be written with valid characters !! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if ((string.IsNullOrEmpty(OAdminUsers.phoneNumber)))
                    {
                        MessageBox.Show("Please enter value for Phone Number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (containsDigits)
                    {
                        MessageBox.Show("Phone Number should be a number !! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    else
                    {
                        OAdminUsers.CreateAdmin();

                        MessageBox.Show(OAdminUsers.username + " registered Successfully", "  ClinicMaster ADMIN ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUsername.Clear();
                        txtPassword.Clear();
                        txtEmail.Clear();
                        txtPhoneNumber.Clear();
                        
                    }
                }

                else
                {
                    MessageBox.Show(OAdminUsers.username+ " already Exists");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public void validateLoginInput()
        {
            try
            {
                if ((string.IsNullOrEmpty(username)))
                {
                    MessageBox.Show("Please enter value for Username", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return ;
                }
                //else if (containsSpecialCharacter)
                //{
                //    MessageBox.Show("Username must not have special characters ");
                //    return true;
                //}
                else if ((string.IsNullOrEmpty(password)))
                {
                    MessageBox.Show("Please enter value for Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return ;
                }

                else
                {
                    return ;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public void validateRegisterUser()
        {
            try{
                bool isEmail = Regex.IsMatch(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            bool containsSpecialCharacter = username.Any(c => !char.IsLetter(c));
            bool containsLowerCase = email.Any(c => !char.IsLower(c));
            bool containsDigits = phoneNumber.Any(c => !char.IsDigit(c));
            if ((string.IsNullOrEmpty(username)))
            {
                MessageBox.Show("Please enter value for Username !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (containsSpecialCharacter)
            {
                MessageBox.Show("Username must not have special characters !! ");
                return;
            }
            else if ((string.IsNullOrEmpty(email)))
            {
                MessageBox.Show("Please enter value for Email !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //else if (containsLowerCase)
            //{
            //    MessageBox.Show("Email should be written in Lower case !! ");
            //    return;
            //}
            else if (!(isEmail))
            {
                MessageBox.Show("Email should be written with valid characters !! ");
                return;
            }
            else if ((string.IsNullOrEmpty(phoneNumber)))
            {
                MessageBox.Show("Please enter value for Phone Number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (containsDigits)
            {
                MessageBox.Show("Please enter digits only !! ","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else
            {
                return;
            }
        }
        catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void Admin_Users_Registration_Load(object sender, EventArgs e)
        {
            try 
            {
                txtPhoneNumber.MaxLength = 10;

                lookUp OlookUp = new lookUp();

                txtroles.DataSource = OlookUp.addRolesCombo().Tables[0];
                txtroles.DisplayMember = "RoleName";
                txtroles.ValueMember = "RoleId";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      



        private void ShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ShowPassword.Checked)
                {
                    txtPassword.PasswordChar = '\0';
                }
                else
                {
                    txtPassword.PasswordChar = '*';
                }
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))

                {
                    MessageBox.Show("please enter digits only !!");
                    e.Handled = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}
