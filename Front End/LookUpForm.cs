﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class LookUpForm : MetroFramework.Forms.MetroForm
    {
        public LookUpForm()
        {
            InitializeComponent();
        }

        private void LookUpForm_Load(object sender, EventArgs e)
        {

        }

     

        private void btnAddDepartment_Click(object sender, EventArgs e)
        {
            AddDepartment OAddDepartment = new AddDepartment();
            OAddDepartment.ShowDialog();
        }

        private void btnAddDesignation_Click(object sender, EventArgs e)
        {
            AddDesignation OAddDesignation = new AddDesignation();

            OAddDesignation.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChangeArrivalTime_Click(object sender, EventArgs e)
        {
            Change_Arrival_Time OChange_Arrival_Time = new Change_Arrival_Time();
            OChange_Arrival_Time.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Change_Allowance_Charge OChange_Allowance_Charge = new Change_Allowance_Charge();
            OChange_Allowance_Charge.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Change_Allowance_Charge CAC = new Change_Allowance_Charge();
            CAC.ShowDialog();
        }
    }
}
