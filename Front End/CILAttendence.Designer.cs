﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class CILAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CILAttendence));
            this.lblDesignation = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.lblAllowanceToBeGiven = new System.Windows.Forms.Label();
            this.lblAttendenceStatus = new System.Windows.Forms.Label();
            this.lblDifferenceInTime = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTimeIn = new System.Windows.Forms.Label();
            this.lblAttendenceDate = new System.Windows.Forms.Label();
            this.SAVEbtn = new System.Windows.Forms.Button();
            this.CLOSEbtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.btnshowAttendenceList = new System.Windows.Forms.Button();
            this.cboName = new System.Windows.Forms.ComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.lblcilnumber = new System.Windows.Forms.Label();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.lblArrivalTime = new System.Windows.Forms.Label();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.lblIssueStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDesignation
            // 
            this.lblDesignation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblDesignation.Location = new System.Drawing.Point(262, 499);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(84, 20);
            this.lblDesignation.TabIndex = 86;
            this.lblDesignation.Text = "Designation";
            // 
            // lblDepartment
            // 
            this.lblDepartment.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartment.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblDepartment.Location = new System.Drawing.Point(262, 448);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(128, 20);
            this.lblDepartment.TabIndex = 85;
            this.lblDepartment.Text = "Department Name";
            // 
            // lblAllowanceToBeGiven
            // 
            this.lblAllowanceToBeGiven.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAllowanceToBeGiven.AutoSize = true;
            this.lblAllowanceToBeGiven.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceToBeGiven.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblAllowanceToBeGiven.Location = new System.Drawing.Point(742, 498);
            this.lblAllowanceToBeGiven.Name = "lblAllowanceToBeGiven";
            this.lblAllowanceToBeGiven.Size = new System.Drawing.Size(74, 20);
            this.lblAllowanceToBeGiven.TabIndex = 84;
            this.lblAllowanceToBeGiven.Text = "Allowance";
            // 
            // lblAttendenceStatus
            // 
            this.lblAttendenceStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAttendenceStatus.AutoSize = true;
            this.lblAttendenceStatus.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttendenceStatus.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblAttendenceStatus.Location = new System.Drawing.Point(739, 432);
            this.lblAttendenceStatus.Name = "lblAttendenceStatus";
            this.lblAttendenceStatus.Size = new System.Drawing.Size(127, 20);
            this.lblAttendenceStatus.TabIndex = 83;
            this.lblAttendenceStatus.Text = "Attendence Status";
            // 
            // lblDifferenceInTime
            // 
            this.lblDifferenceInTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDifferenceInTime.AutoSize = true;
            this.lblDifferenceInTime.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifferenceInTime.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblDifferenceInTime.Location = new System.Drawing.Point(739, 369);
            this.lblDifferenceInTime.Name = "lblDifferenceInTime";
            this.lblDifferenceInTime.Size = new System.Drawing.Size(110, 20);
            this.lblDifferenceInTime.TabIndex = 82;
            this.lblDifferenceInTime.Text = "Time Difference";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(155, 637);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(307, 16);
            this.label5.TabIndex = 72;
            this.label5.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Consolas", 10.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label6.Location = new System.Drawing.Point(566, 637);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(200, 17);
            this.label6.TabIndex = 71;
            this.label6.Text = "Your Health Data Partner";
            // 
            // lblTimeIn
            // 
            this.lblTimeIn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTimeIn.AutoSize = true;
            this.lblTimeIn.Font = new System.Drawing.Font("Meiryo", 9.75F);
            this.lblTimeIn.ForeColor = System.Drawing.Color.Black;
            this.lblTimeIn.Location = new System.Drawing.Point(739, 312);
            this.lblTimeIn.Name = "lblTimeIn";
            this.lblTimeIn.Size = new System.Drawing.Size(53, 20);
            this.lblTimeIn.TabIndex = 70;
            this.lblTimeIn.Text = "TimeIn";
            this.lblTimeIn.Click += new System.EventHandler(this.lblTimeIn_Click);
            // 
            // lblAttendenceDate
            // 
            this.lblAttendenceDate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAttendenceDate.AutoSize = true;
            this.lblAttendenceDate.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttendenceDate.ForeColor = System.Drawing.Color.Black;
            this.lblAttendenceDate.Location = new System.Drawing.Point(739, 254);
            this.lblAttendenceDate.Name = "lblAttendenceDate";
            this.lblAttendenceDate.Size = new System.Drawing.Size(114, 20);
            this.lblAttendenceDate.TabIndex = 69;
            this.lblAttendenceDate.Text = "AttendenceDate";
            // 
            // SAVEbtn
            // 
            this.SAVEbtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SAVEbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SAVEbtn.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SAVEbtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.SAVEbtn.Location = new System.Drawing.Point(181, 565);
            this.SAVEbtn.Name = "SAVEbtn";
            this.SAVEbtn.Size = new System.Drawing.Size(120, 31);
            this.SAVEbtn.TabIndex = 68;
            this.SAVEbtn.Text = "SAVE";
            this.SAVEbtn.UseVisualStyleBackColor = true;
            this.SAVEbtn.Click += new System.EventHandler(this.SAVEbtn_Click);
            // 
            // CLOSEbtn
            // 
            this.CLOSEbtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.CLOSEbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CLOSEbtn.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLOSEbtn.ForeColor = System.Drawing.Color.Red;
            this.CLOSEbtn.Location = new System.Drawing.Point(743, 567);
            this.CLOSEbtn.Name = "CLOSEbtn";
            this.CLOSEbtn.Size = new System.Drawing.Size(99, 31);
            this.CLOSEbtn.TabIndex = 67;
            this.CLOSEbtn.Text = "CLOSE";
            this.CLOSEbtn.UseVisualStyleBackColor = true;
            this.CLOSEbtn.Click += new System.EventHandler(this.CLOSEbtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(646, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 21.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(260, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 34);
            this.label1.TabIndex = 64;
            this.label1.Text = "REGISTER ATTENDENCE ";
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.Location = new System.Drawing.Point(140, 238);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(47, 19);
            this.metroLabel7.TabIndex = 63;
            this.metroLabel7.Text = "CILNo";
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(140, 449);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(83, 19);
            this.metroLabel6.TabIndex = 62;
            this.metroLabel6.Text = "Department";
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(140, 499);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(82, 19);
            this.metroLabel5.TabIndex = 61;
            this.metroLabel5.Text = "Designation";
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(578, 312);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(54, 19);
            this.metroLabel4.TabIndex = 60;
            this.metroLabel4.Text = "Time In";
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Location = new System.Drawing.Point(576, 432);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(121, 19);
            this.metroLabel2.TabIndex = 59;
            this.metroLabel2.Text = "Attendence Status";
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(569, 498);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(146, 19);
            this.metroLabel1.TabIndex = 90;
            this.metroLabel1.Text = "Allowance To Be Given";
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(578, 369);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(119, 19);
            this.metroLabel3.TabIndex = 91;
            this.metroLabel3.Text = "Difference In Time";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblLastName
            // 
            this.lblLastName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblLastName.Location = new System.Drawing.Point(262, 401);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(73, 20);
            this.lblLastName.TabIndex = 129;
            this.lblLastName.Text = "LastName";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMiddleName.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblMiddleName.Location = new System.Drawing.Point(262, 346);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(92, 20);
            this.lblMiddleName.TabIndex = 128;
            this.lblMiddleName.Text = "Middle Name";
            // 
            // lblSurname
            // 
            this.lblSurname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSurname.AutoSize = true;
            this.lblSurname.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblSurname.Location = new System.Drawing.Point(262, 289);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(68, 20);
            this.lblSurname.TabIndex = 127;
            this.lblSurname.Text = "SurName";
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel10.Location = new System.Drawing.Point(140, 401);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(74, 19);
            this.metroLabel10.TabIndex = 126;
            this.metroLabel10.Text = "Last Name";
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel9.Location = new System.Drawing.Point(140, 346);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(91, 19);
            this.metroLabel9.TabIndex = 125;
            this.metroLabel9.Text = "Middle Name";
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.Location = new System.Drawing.Point(140, 291);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(63, 19);
            this.metroLabel8.TabIndex = 124;
            this.metroLabel8.Text = "Surname";
            // 
            // btnshowAttendenceList
            // 
            this.btnshowAttendenceList.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnshowAttendenceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnshowAttendenceList.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshowAttendenceList.ForeColor = System.Drawing.Color.Maroon;
            this.btnshowAttendenceList.Location = new System.Drawing.Point(434, 563);
            this.btnshowAttendenceList.Name = "btnshowAttendenceList";
            this.btnshowAttendenceList.Size = new System.Drawing.Size(190, 33);
            this.btnshowAttendenceList.TabIndex = 131;
            this.btnshowAttendenceList.Text = "Show Attendence List";
            this.btnshowAttendenceList.UseVisualStyleBackColor = true;
            this.btnshowAttendenceList.Click += new System.EventHandler(this.btnshowAttendenceList_Click);
            // 
            // cboName
            // 
            this.cboName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cboName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboName.FormattingEnabled = true;
            this.cboName.Items.AddRange(new object[] {
            "......................................."});
            this.cboName.Location = new System.Drawing.Point(257, 186);
            this.cboName.Name = "cboName";
            this.cboName.Size = new System.Drawing.Size(151, 21);
            this.cboName.TabIndex = 136;
            this.cboName.SelectedIndexChanged += new System.EventHandler(this.cboName_SelectedIndexChanged);
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel11.Location = new System.Drawing.Point(140, 185);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(45, 19);
            this.metroLabel11.TabIndex = 137;
            this.metroLabel11.Text = "Name";
            // 
            // lblcilnumber
            // 
            this.lblcilnumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblcilnumber.AutoSize = true;
            this.lblcilnumber.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcilnumber.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblcilnumber.Location = new System.Drawing.Point(262, 237);
            this.lblcilnumber.Name = "lblcilnumber";
            this.lblcilnumber.Size = new System.Drawing.Size(48, 20);
            this.lblcilnumber.TabIndex = 143;
            this.lblcilnumber.Text = "CILNo";
            // 
            // metroLabel12
            // 
            this.metroLabel12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.Location = new System.Drawing.Point(576, 255);
            this.metroLabel12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(112, 19);
            this.metroLabel12.TabIndex = 144;
            this.metroLabel12.Text = "Attendence Date";
            // 
            // lblArrivalTime
            // 
            this.lblArrivalTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblArrivalTime.AutoSize = true;
            this.lblArrivalTime.Font = new System.Drawing.Font("Meiryo", 9.75F);
            this.lblArrivalTime.ForeColor = System.Drawing.Color.Black;
            this.lblArrivalTime.Location = new System.Drawing.Point(739, 199);
            this.lblArrivalTime.Name = "lblArrivalTime";
            this.lblArrivalTime.Size = new System.Drawing.Size(77, 20);
            this.lblArrivalTime.TabIndex = 145;
            this.lblArrivalTime.Text = "Arrivaltime";
            // 
            // metroLabel13
            // 
            this.metroLabel13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel13.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel13.Location = new System.Drawing.Point(577, 199);
            this.metroLabel13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(81, 19);
            this.metroLabel13.TabIndex = 146;
            this.metroLabel13.Text = "Arrival Time";
            // 
            // metroLabel14
            // 
            this.metroLabel14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.metroLabel14.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel14.Location = new System.Drawing.Point(577, 541);
            this.metroLabel14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(82, 19);
            this.metroLabel14.TabIndex = 148;
            this.metroLabel14.Text = "Issue Status";
            this.metroLabel14.Visible = false;
            // 
            // lblIssueStatus
            // 
            this.lblIssueStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblIssueStatus.AutoSize = true;
            this.lblIssueStatus.Font = new System.Drawing.Font("Meiryo", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIssueStatus.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblIssueStatus.Location = new System.Drawing.Point(739, 544);
            this.lblIssueStatus.Name = "lblIssueStatus";
            this.lblIssueStatus.Size = new System.Drawing.Size(86, 20);
            this.lblIssueStatus.TabIndex = 147;
            this.lblIssueStatus.Text = "Issue Status";
            this.lblIssueStatus.Visible = false;
            // 
            // CILAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 662);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.lblIssueStatus);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.lblArrivalTime);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.lblcilnumber);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.cboName);
            this.Controls.Add(this.btnshowAttendenceList);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.lblMiddleName);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.lblDesignation);
            this.Controls.Add(this.lblDepartment);
            this.Controls.Add(this.lblAllowanceToBeGiven);
            this.Controls.Add(this.lblAttendenceStatus);
            this.Controls.Add(this.lblDifferenceInTime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblTimeIn);
            this.Controls.Add(this.lblAttendenceDate);
            this.Controls.Add(this.SAVEbtn);
            this.Controls.Add(this.CLOSEbtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel2);
            this.ForeColor = System.Drawing.Color.Navy;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CILAttendence";
            this.Text = "Register Attendence";
            this.Load += new System.EventHandler(this.CILAttendence_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDesignation;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Label lblAllowanceToBeGiven;
        private System.Windows.Forms.Label lblAttendenceStatus;
        private System.Windows.Forms.Label lblDifferenceInTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTimeIn;
        private System.Windows.Forms.Label lblAttendenceDate;
        private System.Windows.Forms.Button SAVEbtn;
        private System.Windows.Forms.Button CLOSEbtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.Label lblSurname;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.Button btnshowAttendenceList;
        private System.Windows.Forms.ComboBox cboName;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.Label lblcilnumber;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private System.Windows.Forms.Label lblArrivalTime;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private System.Windows.Forms.Label lblIssueStatus;
    }
}