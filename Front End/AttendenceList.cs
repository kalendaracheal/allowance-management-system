﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class AttendenceList : Form
    {
        DataTable attendenceList = new DataTable();
        public AttendenceList()
        {
            InitializeComponent();
        }

        private void AttendenceList_Load(object sender, EventArgs e)
        {
            try
            {

                AdminUsers OAdminUsers = new AdminUsers();
               dataGridAttendence.DataSource = OAdminUsers.getallEmployeeInAttendence();

              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
      
    
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


     
        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                OAdminUsers.attendencedate = Convert.ToDateTime(dateTimePicker2.Value.ToShortDateString());
                dataGridAttendence.DataSource = OAdminUsers.get_EmployeebyAttendenceDateInAttendence();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdminUsers OAdminUsers = new AdminUsers();
            

            if (cboSearch.SelectedIndex == 0)
            {
                
                DataTable staffs = OAdminUsers.addNameToCombobox();
                staffs.Rows.InsertAt(staffs.NewRow(), 0);
                cbo_Search.DataSource = staffs;

                cbo_Search.DataSource = staffs;
                cbo_Search.DisplayMember = "FullName";
                cbo_Search.ValueMember = "CILNo";
            }
          
            else if (cboSearch.SelectedIndex == 1)
            {
                DataTable staffs = OAdminUsers.addDepartmentToCombobox();
                staffs.Rows.InsertAt(staffs.NewRow(), 0);
                cbo_Search.DataSource = staffs;

                cbo_Search.DataSource = staffs;
                cbo_Search.DisplayMember = "Department";
                cbo_Search.ValueMember = "Department";

            }
            else if (cboSearch.SelectedIndex == 2)
            {
                DataTable staffs = OAdminUsers.addDesignationToCombobox();
                staffs.Rows.InsertAt(staffs.NewRow(), 0);
                cbo_Search.DataSource = staffs;

                cbo_Search.DataSource = staffs;
                cbo_Search.DisplayMember = "Designation";
                cbo_Search.ValueMember = "Designation";

            }
            else { }

        }

        private void cbo_Search_SelectedIndexChanged(object sender, EventArgs e)
        {

            AdminUsers OAdminUsers = new AdminUsers();

            OAdminUsers.cilnumber = cbo_Search.SelectedValue.ToString();
            OAdminUsers.attendencedate = Convert.ToDateTime(dateTimePicker2.Value.ToShortDateString());
           
            if (cboSearch.SelectedIndex == 0)
            {

                OAdminUsers.cilnumber = cbo_Search.SelectedValue.ToString();
                dataGridAttendence.DataSource = OAdminUsers.get_EmployeebyFullNameInAttendence();
                
            }
          
            else if (cboSearch.SelectedIndex == 1)
            {

                OAdminUsers.Department = cbo_Search.SelectedValue.ToString();
                dataGridAttendence.DataSource = OAdminUsers.get_EmployeebyDepartmentInAttendence();

            }
            else if (cboSearch.SelectedIndex == 2)
            {

                OAdminUsers.Designation = cbo_Search.SelectedValue.ToString();
                dataGridAttendence.DataSource = OAdminUsers.get_EmployeebyDesignationInAttendence();

            }
            else { }



        }

        private void dataGridAttendence_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
