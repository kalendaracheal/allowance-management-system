﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class DeleteEmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CILNo_tx = new MetroFramework.Controls.MetroComboBox();
            this.Delete_tx = new MetroFramework.Controls.MetroButton();
            this.Surname_tx = new MetroFramework.Controls.MetroTextBox();
            this.middleName_tx = new MetroFramework.Controls.MetroTextBox();
            this.lastName_tx = new MetroFramework.Controls.MetroTextBox();
            this.Department_tx = new MetroFramework.Controls.MetroTextBox();
            this.Designation_tx = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose CIL Number";
            // 
            // CILNo_tx
            // 
            this.CILNo_tx.FormattingEnabled = true;
            this.CILNo_tx.ItemHeight = 23;
            this.CILNo_tx.Location = new System.Drawing.Point(265, 85);
            this.CILNo_tx.Name = "CILNo_tx";
            this.CILNo_tx.Size = new System.Drawing.Size(172, 29);
            this.CILNo_tx.TabIndex = 1;
            this.CILNo_tx.UseSelectable = true;
            this.CILNo_tx.SelectedIndexChanged += new System.EventHandler(this.CILNo_tx_SelectedIndexChanged);
            // 
            // Delete_tx
            // 
            this.Delete_tx.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.Delete_tx.Highlight = true;
            this.Delete_tx.Location = new System.Drawing.Point(156, 357);
            this.Delete_tx.Name = "Delete_tx";
            this.Delete_tx.Size = new System.Drawing.Size(75, 31);
            this.Delete_tx.Style = MetroFramework.MetroColorStyle.Blue;
            this.Delete_tx.TabIndex = 2;
            this.Delete_tx.Text = "DELETE";
            this.Delete_tx.UseCustomBackColor = true;
            this.Delete_tx.UseSelectable = true;
            this.Delete_tx.UseStyleColors = true;
            this.Delete_tx.Click += new System.EventHandler(this.Delete_tx_Click);
            // 
            // Surname_tx
            // 
            // 
            // 
            // 
            this.Surname_tx.CustomButton.Image = null;
            this.Surname_tx.CustomButton.Location = new System.Drawing.Point(150, 1);
            this.Surname_tx.CustomButton.Name = "";
            this.Surname_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Surname_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Surname_tx.CustomButton.TabIndex = 1;
            this.Surname_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Surname_tx.CustomButton.UseSelectable = true;
            this.Surname_tx.CustomButton.Visible = false;
            this.Surname_tx.Lines = new string[0];
            this.Surname_tx.Location = new System.Drawing.Point(265, 138);
            this.Surname_tx.MaxLength = 32767;
            this.Surname_tx.Name = "Surname_tx";
            this.Surname_tx.PasswordChar = '\0';
            this.Surname_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Surname_tx.SelectedText = "";
            this.Surname_tx.SelectionLength = 0;
            this.Surname_tx.SelectionStart = 0;
            this.Surname_tx.ShortcutsEnabled = true;
            this.Surname_tx.Size = new System.Drawing.Size(172, 23);
            this.Surname_tx.TabIndex = 3;
            this.Surname_tx.UseSelectable = true;
            this.Surname_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Surname_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // middleName_tx
            // 
            // 
            // 
            // 
            this.middleName_tx.CustomButton.Image = null;
            this.middleName_tx.CustomButton.Location = new System.Drawing.Point(150, 1);
            this.middleName_tx.CustomButton.Name = "";
            this.middleName_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.middleName_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.middleName_tx.CustomButton.TabIndex = 1;
            this.middleName_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.middleName_tx.CustomButton.UseSelectable = true;
            this.middleName_tx.CustomButton.Visible = false;
            this.middleName_tx.Lines = new string[0];
            this.middleName_tx.Location = new System.Drawing.Point(265, 176);
            this.middleName_tx.MaxLength = 32767;
            this.middleName_tx.Name = "middleName_tx";
            this.middleName_tx.PasswordChar = '\0';
            this.middleName_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.middleName_tx.SelectedText = "";
            this.middleName_tx.SelectionLength = 0;
            this.middleName_tx.SelectionStart = 0;
            this.middleName_tx.ShortcutsEnabled = true;
            this.middleName_tx.Size = new System.Drawing.Size(172, 23);
            this.middleName_tx.TabIndex = 4;
            this.middleName_tx.UseSelectable = true;
            this.middleName_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.middleName_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lastName_tx
            // 
            // 
            // 
            // 
            this.lastName_tx.CustomButton.Image = null;
            this.lastName_tx.CustomButton.Location = new System.Drawing.Point(150, 1);
            this.lastName_tx.CustomButton.Name = "";
            this.lastName_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lastName_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lastName_tx.CustomButton.TabIndex = 1;
            this.lastName_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lastName_tx.CustomButton.UseSelectable = true;
            this.lastName_tx.CustomButton.Visible = false;
            this.lastName_tx.Lines = new string[0];
            this.lastName_tx.Location = new System.Drawing.Point(265, 217);
            this.lastName_tx.MaxLength = 32767;
            this.lastName_tx.Name = "lastName_tx";
            this.lastName_tx.PasswordChar = '\0';
            this.lastName_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lastName_tx.SelectedText = "";
            this.lastName_tx.SelectionLength = 0;
            this.lastName_tx.SelectionStart = 0;
            this.lastName_tx.ShortcutsEnabled = true;
            this.lastName_tx.Size = new System.Drawing.Size(172, 23);
            this.lastName_tx.TabIndex = 5;
            this.lastName_tx.UseSelectable = true;
            this.lastName_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lastName_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Department_tx
            // 
            // 
            // 
            // 
            this.Department_tx.CustomButton.Image = null;
            this.Department_tx.CustomButton.Location = new System.Drawing.Point(150, 1);
            this.Department_tx.CustomButton.Name = "";
            this.Department_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Department_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Department_tx.CustomButton.TabIndex = 1;
            this.Department_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Department_tx.CustomButton.UseSelectable = true;
            this.Department_tx.CustomButton.Visible = false;
            this.Department_tx.Lines = new string[0];
            this.Department_tx.Location = new System.Drawing.Point(265, 259);
            this.Department_tx.MaxLength = 32767;
            this.Department_tx.Name = "Department_tx";
            this.Department_tx.PasswordChar = '\0';
            this.Department_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Department_tx.SelectedText = "";
            this.Department_tx.SelectionLength = 0;
            this.Department_tx.SelectionStart = 0;
            this.Department_tx.ShortcutsEnabled = true;
            this.Department_tx.Size = new System.Drawing.Size(172, 23);
            this.Department_tx.TabIndex = 6;
            this.Department_tx.UseSelectable = true;
            this.Department_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Department_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Designation_tx
            // 
            // 
            // 
            // 
            this.Designation_tx.CustomButton.Image = null;
            this.Designation_tx.CustomButton.Location = new System.Drawing.Point(150, 1);
            this.Designation_tx.CustomButton.Name = "";
            this.Designation_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Designation_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Designation_tx.CustomButton.TabIndex = 1;
            this.Designation_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Designation_tx.CustomButton.UseSelectable = true;
            this.Designation_tx.CustomButton.Visible = false;
            this.Designation_tx.Lines = new string[0];
            this.Designation_tx.Location = new System.Drawing.Point(265, 301);
            this.Designation_tx.MaxLength = 32767;
            this.Designation_tx.Name = "Designation_tx";
            this.Designation_tx.PasswordChar = '\0';
            this.Designation_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Designation_tx.SelectedText = "";
            this.Designation_tx.SelectionLength = 0;
            this.Designation_tx.SelectionStart = 0;
            this.Designation_tx.ShortcutsEnabled = true;
            this.Designation_tx.Size = new System.Drawing.Size(172, 23);
            this.Designation_tx.TabIndex = 7;
            this.Designation_tx.UseSelectable = true;
            this.Designation_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Designation_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(184, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(153, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Middle Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(173, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Last name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(164, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Department";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(153, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 18);
            this.label6.TabIndex = 12;
            this.label6.Text = "Designation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10.25F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 410);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(307, 16);
            this.label7.TabIndex = 74;
            this.label7.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Consolas", 10.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label8.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label8.Location = new System.Drawing.Point(330, 409);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(200, 17);
            this.label8.TabIndex = 73;
            this.label8.Text = "Your Health Data Partner";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(333, 357);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 31);
            this.btnClose.TabIndex = 75;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DeleteEmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 432);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Designation_tx);
            this.Controls.Add(this.Department_tx);
            this.Controls.Add(this.lastName_tx);
            this.Controls.Add(this.middleName_tx);
            this.Controls.Add(this.Surname_tx);
            this.Controls.Add(this.Delete_tx);
            this.Controls.Add(this.CILNo_tx);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeleteEmployeeForm";
            this.Text = "Delete Employee";
            this.Load += new System.EventHandler(this.DeleteEmployeeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox CILNo_tx;
        private MetroFramework.Controls.MetroButton Delete_tx;
        private MetroFramework.Controls.MetroTextBox Surname_tx;
        private MetroFramework.Controls.MetroTextBox middleName_tx;
        private MetroFramework.Controls.MetroTextBox lastName_tx;
        private MetroFramework.Controls.MetroTextBox Department_tx;
        private MetroFramework.Controls.MetroTextBox Designation_tx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnClose;
    }
}