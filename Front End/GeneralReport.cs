﻿using CIL_Allowance_Management_System.Classes;
using ClosedXML.Excel;
using DGVPrinterHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using DVGPrinterHelper;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class GeneralReport : MetroFramework.Forms.MetroForm
    {
        public GeneralReport()
        {
            InitializeComponent();
        }

        private void GeneralReport_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void load_tx_Click(object sender, EventArgs e)
        {
            Allowance OAllowance = new Allowance();

            OAllowance.StartDate = DateTime.Parse(startDate_tx.Value.ToShortDateString());
            OAllowance.EndDate = DateTime.Parse(endDate_tx.Value.ToShortDateString());

            allowanceDetails_tx.DataSource = OAllowance.ReadALLallowanceDetails();

            int total = 0;

            for (int i = 0; i < allowanceDetails_tx.Rows.Count; i++)
            {
                if (allowanceDetails_tx.Rows[i].Cells["AmountGiven"].Value != null)
                {
                    total += int.Parse(allowanceDetails_tx.Rows[i].Cells["AmountGiven"].Value.ToString());
                }
            }

            totalAmount_tx.Text = total.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                Allowance OAllowance = new Allowance();

                foreach (DataGridViewColumn col in allowanceDetails_tx.Columns)
                {
                    OAllowance.ReadALLallowanceDetails().Columns.Add(col.HeaderText, col.ValueType);
                }

                //adding rows
                foreach (DataGridViewRow row in allowanceDetails_tx.Rows)
                {
                    OAllowance.ReadALLallowanceDetails().Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        OAllowance.ReadALLallowanceDetails().Rows[OAllowance.ReadALLallowanceDetails().Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                //exporting to excell
                string folderPath = "C:\\Excel\\";

                //SaveFileDialog ofd = new SaveFileDialog();
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(OAllowance.ReadALLallowanceDetails(), "AllowanceDetails");
                    wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message, "Technical Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Allowance OAllowance = new Allowance();
            DGVPrinter printer = new DGVPrinter();

            printer.Title = "ClinicMaster Allowance Details";
            printer.SubTitle = "Allowance details given out to Employees from" + " "+ OAllowance.StartDate + " " +"to" + " " + OAllowance.EndDate ;
            printer.SubTitle = "Total Amount" + " = " + totalAmount_tx.Text + "UGX";
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Center;
            printer.Footer = "ClinicMaster Allowance Report"; //footer
            printer.FooterSpacing = 15;

            //land scape setting
            printer.printDocument.DefaultPageSettings.Landscape = true;
            printer.PrintDataGridView(allowanceDetails_tx);

        }
    }
}
