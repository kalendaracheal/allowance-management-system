﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class Admin_List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridviewAdmin = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.caption = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridviewAdmin)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridviewAdmin
            // 
            this.dataGridviewAdmin.AllowUserToAddRows = false;
            this.dataGridviewAdmin.AllowUserToDeleteRows = false;
            this.dataGridviewAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridviewAdmin.BackgroundColor = System.Drawing.Color.LightSteelBlue;
            this.dataGridviewAdmin.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridviewAdmin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridviewAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridviewAdmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridviewAdmin.Location = new System.Drawing.Point(12, 36);
            this.dataGridviewAdmin.Name = "dataGridviewAdmin";
            this.dataGridviewAdmin.ReadOnly = true;
            this.dataGridviewAdmin.ShowEditingIcon = false;
            this.dataGridviewAdmin.Size = new System.Drawing.Size(884, 402);
            this.dataGridviewAdmin.TabIndex = 1;
            this.dataGridviewAdmin.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridviewAdmin_CellContentClick);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 451);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // caption
            // 
            this.caption.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.caption.AutoSize = true;
            this.caption.Font = new System.Drawing.Font("Consolas", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.caption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.caption.Location = new System.Drawing.Point(391, 450);
            this.caption.Name = "caption";
            this.caption.Size = new System.Drawing.Size(200, 18);
            this.caption.TabIndex = 22;
            this.caption.Text = "Your Health Data Partner";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(698, 444);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(128, 31);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Admin_List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 479);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.caption);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dataGridviewAdmin);
            this.Name = "Admin_List";
            this.Text = "Admin_List";
            this.Load += new System.EventHandler(this.Admin_List_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridviewAdmin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridviewAdmin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label caption;
        private System.Windows.Forms.Button btnClose;
    }
}