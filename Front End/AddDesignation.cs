﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class AddDesignation : MetroFramework.Forms.MetroForm
    {
        public AddDesignation()
        {
            InitializeComponent();
        }

        private void AddDesignation_Load(object sender, EventArgs e)
        {

        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            try
            {
                lookUp OlookUp = new lookUp();
                OlookUp.DataId = int.Parse(metroTextBox2.Text);
                OlookUp.designationItem = metroTextBox1.Text;

                OlookUp.addDesignation();

                MessageBox.Show("item added");
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                lookUp OlookUp = new lookUp();
                OlookUp.DataId = int.Parse(metroTextBox2.Text);
                OlookUp.designationItem = metroTextBox1.Text;

                OlookUp.addDesignation();

                metroTextBox1.Text = "";
                metroTextBox2.Text = "";

                MessageBox.Show("item added");
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
