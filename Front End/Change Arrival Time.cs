﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class Change_Arrival_Time : Form
    {
        public Change_Arrival_Time()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                OAdminUsers.hours = Convert.ToDouble(txthour.Text);
                OAdminUsers.minutes = Convert.ToDouble(txtminutes.Text);
                OAdminUsers.seconds = Convert.ToDouble(txtseconds.Text);
                OAdminUsers.updateArrivaleTime();

                MessageBox.Show("Arrival Time Updated Successfully!! ", "ClinicMater", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Change_Arrival_Time_Load(object sender, EventArgs e)
        {
            try
            {
                txthour.MaxLength = 2;
                txtminutes.MaxLength = 2;
                txtseconds.MaxLength = 2;
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       
        //!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back))
        private void txthour_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
               
                if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))

                {
                    MessageBox.Show("please enter digits only !!");
                    e.Handled = true;
                }
               
               

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtminutes_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))

                {
                    MessageBox.Show("please enter digits only !!");
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtseconds_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))

                {
                    MessageBox.Show("please enter digits only !!");
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txthour_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int hr = Convert.ToInt32(txthour.Text);
                if (hr > 23)
                {
                    MessageBox.Show("Value doesn't exceed 23 !!");
                    txthour.Clear();
                    return;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtminutes_TextChanged(object sender, EventArgs e)
        {
           try
            {
                    int min = Convert.ToInt32(txtminutes.Text);
                if (min > 59)
                {
                    MessageBox.Show("Value doesn't exceed 59 !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtminutes.Clear();
                    return;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        private void txtseconds_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int sec = Convert.ToInt32(txtseconds.Text);
                if (sec > 59)
                {
                    MessageBox.Show("Value doesn't exceed 59 !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtseconds.Clear();
                    return;
                }
            }
            
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}
    }
}
