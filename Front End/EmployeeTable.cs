﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIL_Allowance_Management_System.Classes;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class EmployeeTable : MetroFramework.Forms.MetroForm
    {
        public EmployeeTable()
        {
            InitializeComponent();
        }

        private void EmployeeTable_Load(object sender, EventArgs e)
        {
            Employee OEmployee = new Employee();

            EmployeeTable_tx.DataSource = OEmployee.EmployeeTable();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Employee OEmployee2 = new Employee();
            if (e.KeyChar == (Char)13)
            {
                DataView dv = OEmployee2.EmployeeTable().DefaultView;
                dv.RowFilter = String.Format("SurName like '%{0}%'", metroTextBox1.Text)  ;
                EmployeeTable_tx.DataSource = dv.ToTable();
            }
        }

        private void EmployeeTable_tx_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void metroTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void metroTextBox2_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            Employee OEmployee3 = new Employee();
            if (e.KeyChar == (Char)13)
            {
                DataView dv = OEmployee3.EmployeeTable().DefaultView;
                dv.RowFilter = String.Format("Department like '%{0}%'", metroTextBox2.Text);
                EmployeeTable_tx.DataSource = dv.ToTable();

            }
        }

        private void metroTextBox3_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            Employee OEmployee4 = new Employee();
            if (e.KeyChar == (Char)13)
            {
                DataView dv = OEmployee4.EmployeeTable().DefaultView;
                dv.RowFilter = String.Format("Designation like '%{0}%'", metroTextBox3.Text);
                EmployeeTable_tx.DataSource = dv.ToTable();
            }
        }
    }
}
