﻿using CIL_Allowance_Management_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{

    public partial class CILAttendence : Form
    {
        

        public SqlConnection cnn { get; private set; }

        public CILAttendence()
        {
            InitializeComponent();
            //lblArrivalTime.Text = dateTimePicker1.Value.ToString("hh:mm tt");

            //DateTime ArrivalTime = Convert.ToDateTime(lblArrivalTime.Text);

        }

        private void SAVEbtn_Click(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                DataTable table = new DataTable();

                OAdminUsers.MiddleName = lblMiddleName.Text;
                OAdminUsers.LastName = lblLastName.Text;
                OAdminUsers.Surname = lblSurname.Text;
                OAdminUsers.cilnumber = lblcilnumber.Text;
                OAdminUsers.attendencedate = Convert.ToDateTime(lblAttendenceDate.Text);
                OAdminUsers.DifferenceInTime = lblDifferenceInTime.Text;
                OAdminUsers.TimeIn = Convert.ToDateTime(lblTimeIn.Text);
                OAdminUsers.Designation = lblDesignation.Text;
                OAdminUsers.Department = lblDepartment.Text;
                OAdminUsers.AttendenceStatus = lblAttendenceStatus.Text;
                OAdminUsers.AllowanceToBeGiven = lblAllowanceToBeGiven.Text;
                OAdminUsers.IssueStatus = lblIssueStatus.Text;

                if (OAdminUsers.checkIfAttendenceExists().Rows.Count < 1)
                {
                    if (lblcilnumber.Text == "CILNo")
                    {
                        MessageBox.Show("Please select a value for Name", "ClinicMater", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {

                        OAdminUsers.save_Attendence();
                        MessageBox.Show("Attendence Saved successfully !!", "ClinicMater", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Attendence Already Exists","ClinicMater", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }

        private void CLOSEbtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();

            }
              catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }         
        }


        private void CILAttendence_Load(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                //ADDING ITEM TO THE COMBO BOX

                DataTable staff = OAdminUsers.addNameToCombobox();


                staff.Rows.InsertAt(staff.NewRow(), 0);

                //cboName.Items.Add(" ");
                cboName.DataSource = staff;
                cboName.DisplayMember = "FullName";
                cboName.ValueMember = "CILNo";
                //cboName.Items.Insert(0, " ");
                //foreach (DataRow dr in OAdminUsers.addNameToCombobox().Rows)
                //{
                //    cboName.Items.Add(dr["FullName"].ToString() + " " + dr["CILNo"].ToString());
                //    cboName.DisplayMember = "FullName";
                //    cboName.ValueMember = "CILNo";
                //    // OAdminUsers.cilnumber = dr["CILNo"].ToString();
                //}



                DateTime Current = DateTime.Now;
                lblAttendenceDate.Text = Current.ToShortDateString();

                DateTime Time_In = DateTime.Now;
                lblTimeIn.Text = Time_In.ToShortTimeString();

                //   DateTime ArrivalTime = DateTime.Today.AddHours(8).AddMinutes(15);
                foreach (DataRow dr in OAdminUsers.getArrivalTime().Rows)
                {
                    OAdminUsers.hours = Convert.ToDouble(dr["hours"]);
                    OAdminUsers.minutes = Convert.ToDouble(dr["minutes"]);
                    OAdminUsers.seconds = Convert.ToDouble(dr["seconds"]);

                    DateTime ArrivalTime = DateTime.Today.AddHours(OAdminUsers.hours).AddMinutes(OAdminUsers.minutes).AddSeconds(OAdminUsers.seconds);
                    //   lblArrivalTime.Text = ArrivalTime.ToShortTimeString();
                    TimeSpan DifferenceInTime = Current - ArrivalTime;

                    lblDifferenceInTime.Text = DifferenceInTime.ToString();

                    //if (Current <= ArrivalTime)
                    //{
                    //    int Early = 5000;
                    //    lblAllowanceToBeGiven.Text = Early.ToString();
                    //    lblAttendenceStatus.Text = "On Time";
                    //}

                    //else
                    //{
                    //    int Late = 2000;
                    //    lblAllowanceToBeGiven.Text = Late.ToString();
                    //    lblAttendenceStatus.Text = "Late";
                    //}
                    Allowance OAllowance = new Allowance();

                    foreach (DataRow row in OAllowance.GetAllowanceGiven().Rows)
                    {
                        OAllowance.Early = Convert.ToInt32(row["Early"]);
                        OAllowance.Late = Convert.ToInt32(row["Late"]);


                        if (Current <= ArrivalTime)
                        {
                            //int Early = 5000;
                            //  lblAllowanceToBeGiven.Text = Early.ToString();
                            lblAttendenceStatus.Text = "Early";
                            lblAllowanceToBeGiven.Text = Convert.ToString(OAllowance.Early);

                        }

                        else
                        {
                            //int Late = 2000;
                            //  lblAllowanceToBeGiven.Text = Late.ToString();
                            lblAttendenceStatus.Text = "Late";
                            lblAllowanceToBeGiven.Text = Convert.ToString(OAllowance.Late);

                        }

                    }
                    lblArrivalTime.Text = Convert.ToDouble((dr["hours"].ToString())) + ":" + Convert.ToDouble((dr["minutes"].ToString())) + "AM";

                }
               
                    // Timer for Time in
                    Timer timer1 = new Timer();
                timer1.Interval = 1000;//ticks every 1 second
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Start();

                //Timer for Difference in time
                Timer timer2 = new Timer();
                timer2.Interval = 1000;//ticks every 1 second
                timer2.Tick += new EventHandler(timer2_Tick);
                timer2.Start();

                


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

       
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime datenow = DateTime.Now;
                lblTimeIn.Text = datenow.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void lblTimeIn_Click(object sender, EventArgs e)
        {

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                AdminUsers OAdminUsers = new AdminUsers();
                DateTime Current = DateTime.Now;
                // DateTime ArrivalTime = DateTime.Today.AddHours(8).AddMinutes(15);

               

                foreach (DataRow dr in OAdminUsers.getArrivalTime().Rows)
                {
                    OAdminUsers.hours = Convert.ToDouble(dr["hours"]);
                    OAdminUsers.minutes = Convert.ToDouble(dr["minutes"]);                    
                    OAdminUsers.seconds = Convert.ToDouble(dr["seconds"]);
                   DateTime ArrivalTime = DateTime.Today.AddHours(OAdminUsers.hours).AddMinutes(OAdminUsers.minutes).AddSeconds(OAdminUsers.seconds);

                    TimeSpan DifferenceInTime = Current - ArrivalTime;
                    lblDifferenceInTime.Text = DifferenceInTime.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnshowAttendenceList_Click(object sender, EventArgs e)
        {
            try
            {
                AttendenceList OAttendenceList = new AttendenceList();
                OAttendenceList.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      

        private void cboName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {                AdminUsers OAdminUsers = new AdminUsers();
                 OAdminUsers.cilnumber = cboName.SelectedValue.ToString();
                //  OAdminUsers.SearchEmployeeInAttendence();
                    foreach (DataRow dr in OAdminUsers.SearchEmployeeInAttendence().Rows)
                    {
                    
                    lblcilnumber.Text = dr["CILNo"].ToString();
                        lblSurname.Text = dr["SurName"].ToString();
                        lblMiddleName.Text = dr["MiddleName"].ToString();
                        lblLastName.Text = dr["LastName"].ToString();
                        lblDepartment.Text = dr["Department"].ToString();
                        lblDesignation.Text = dr["Designation"].ToString();
                    }
               
            }
             catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       
    }
}
