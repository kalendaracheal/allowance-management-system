﻿using CIL_Allowance_Management_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class DeleteEmployeeForm : MetroFramework.Forms.MetroForm
    {
        public DeleteEmployeeForm()
        {
            InitializeComponent();
        }

        private void DeleteEmployeeForm_Load(object sender, EventArgs e)
        {
            Employee OEmployee = new Employee();

            foreach (DataRow dr in OEmployee.addCilToCombo().Rows)
            {
                CILNo_tx.Items.Add(dr["CILNo"].ToString());
            }

        }

        private void Delete_tx_Click(object sender, EventArgs e)
        {
            try
            {
                if (CILNo_tx.Text != String.Empty) 
                {
                    if (MessageBox.Show("Are you sure you want to delete?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)==DialogResult.Yes) 
                    {

                        Employee OEmployee2 = new Employee();
                        OEmployee2.CILNo = CILNo_tx.Text;
                        OEmployee2.Surname = Surname_tx.Text;
                        OEmployee2.MiddleName = middleName_tx.Text;
                        OEmployee2.LastName = lastName_tx.Text;
                        OEmployee2.Department = Department_tx.Text;
                        OEmployee2.Designation = Designation_tx.Text;

                        OEmployee2.DeleteEmployee();

                        CILNo_tx.Text = "";
                        Surname_tx.Text = "";
                        middleName_tx.Text = "";
                        lastName_tx.Text = "";
                        Department_tx.Text = "";
                        Designation_tx.Text = "";

                        MessageBox.Show(" Empyloyee  deleted successfully");
                    }
                }
                else
                {
                    MessageBox.Show("Please choose a CILNo");
                }
            }
            catch(Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void CILNo_tx_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee OEmployee = new Employee();

            OEmployee.CILNo = CILNo_tx.SelectedItem.ToString();

            foreach (DataRow dr in OEmployee.SearchEmployeeInEditForm().Rows)
            {
                Surname_tx.Text = dr["SurName"].ToString();
                middleName_tx.Text = dr["MiddleName"].ToString();
                lastName_tx.Text = dr["LastName"].ToString();
                Department_tx.Text = dr["Department"].ToString();
                Designation_tx.Text = dr["Designation"].ToString();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
