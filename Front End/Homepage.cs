﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.Front_End
{
    public partial class Homepage : Form
    {
        public Homepage()
        {
            InitializeComponent();
        }

        private void Homepage_Load(object sender, EventArgs e)
        {
            if (AdminUsers.RoleType == "Admin"  )
            {
                attendanceToolStripMenuItem.Visible = true;
                allowanceToolStripMenuItem.Visible = true;
                lookUpToolStripMenuItem.Visible = true;
                adminToolStripMenuItem.Visible = true;
            }
            else if (AdminUsers.RoleType == "Accountant")
            {
                employeeToolStripMenuItem.Enabled = false;
                attendanceToolStripMenuItem.Enabled = false;
                allowanceToolStripMenuItem.Visible = true;
                lookUpToolStripMenuItem.Visible = false;
                adminToolStripMenuItem.Visible = false;
            }
            else
            {
                employeeToolStripMenuItem.Visible = false;
                attendanceToolStripMenuItem.Visible = true;
                allowanceToolStripMenuItem.Visible = false;
                lookUpToolStripMenuItem.Visible = false;
                adminToolStripMenuItem.Visible = false;
            }

        }

        private void newEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeRegistrationForm OEmployeeRegistrationForm = new EmployeeRegistrationForm();
            OEmployeeRegistrationForm.ShowDialog();
        }

        private void editEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeEditForm OEmployeeEditForm = new EmployeeEditForm();
            OEmployeeEditForm.ShowDialog();
        }

        private void employeeTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeTable OEmployeeTable = new EmployeeTable();
            OEmployeeTable.ShowDialog();

        }

        private void registerAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CILAttendence OCILAttendance = new CILAttendence();
            OCILAttendance.ShowDialog();

        }

        private void attendanceListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AttendenceList OAttendanceList = new AttendenceList();
            OAttendanceList.ShowDialog();
        }

        private void employeeAllowanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeAllowance OEmployeeAllowance = new EmployeeAllowance();
            OEmployeeAllowance.ShowDialog();
        }

        private void generalReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GeneralReport OGeneralReport = new GeneralReport();
            OGeneralReport.ShowDialog();
            
        }

        private void lookUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LookUpForm OLookUpForm = new LookUpForm();
            OLookUpForm.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void registerAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Admin_Users_Registration OAdmin_Users_Registration = new Admin_Users_Registration();
            OAdmin_Users_Registration.ShowDialog();
        }

        private void editAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditAdmin OEditAdmin = new EditAdmin();
            OEditAdmin.ShowDialog();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteEmployeeForm ODeleteEmployeeForm = new DeleteEmployeeForm();
            ODeleteEmployeeForm.ShowDialog();
        }

        private void linkLabelLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Login OLogin = new Login();
            OLogin.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void logoutbtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login OLogin = new Login();
            OLogin.ShowDialog();
            
        }
    }
}
