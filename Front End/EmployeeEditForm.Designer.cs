﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class EmployeeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.PhoneNumber_tx = new MetroFramework.Controls.MetroTextBox();
            this.Location_tx = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.DateOfBirth_tx = new System.Windows.Forms.DateTimePicker();
            this.Designation_tx = new MetroFramework.Controls.MetroComboBox();
            this.Gender_tx = new MetroFramework.Controls.MetroComboBox();
            this.Department_tx = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.Email_tx = new MetroFramework.Controls.MetroTextBox();
            this.Surname_tx = new MetroFramework.Controls.MetroTextBox();
            this.Age_tx = new MetroFramework.Controls.MetroTextBox();
            this.LastName_tx = new MetroFramework.Controls.MetroTextBox();
            this.MiddleName_tx = new MetroFramework.Controls.MetroTextBox();
            this.CILNo_tx = new MetroFramework.Controls.MetroComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.caption = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel13.Location = new System.Drawing.Point(469, 350);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(66, 19);
            this.metroLabel13.TabIndex = 54;
            this.metroLabel13.Text = "Location";
            // 
            // PhoneNumber_tx
            // 
            // 
            // 
            // 
            this.PhoneNumber_tx.CustomButton.Image = null;
            this.PhoneNumber_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.PhoneNumber_tx.CustomButton.Name = "";
            this.PhoneNumber_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.PhoneNumber_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.PhoneNumber_tx.CustomButton.TabIndex = 1;
            this.PhoneNumber_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.PhoneNumber_tx.CustomButton.UseSelectable = true;
            this.PhoneNumber_tx.CustomButton.Visible = false;
            this.PhoneNumber_tx.Lines = new string[0];
            this.PhoneNumber_tx.Location = new System.Drawing.Point(178, 281);
            this.PhoneNumber_tx.MaxLength = 32767;
            this.PhoneNumber_tx.Name = "PhoneNumber_tx";
            this.PhoneNumber_tx.PasswordChar = '\0';
            this.PhoneNumber_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PhoneNumber_tx.SelectedText = "";
            this.PhoneNumber_tx.SelectionLength = 0;
            this.PhoneNumber_tx.SelectionStart = 0;
            this.PhoneNumber_tx.ShortcutsEnabled = true;
            this.PhoneNumber_tx.Size = new System.Drawing.Size(192, 23);
            this.PhoneNumber_tx.TabIndex = 53;
            this.PhoneNumber_tx.UseSelectable = true;
            this.PhoneNumber_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.PhoneNumber_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.PhoneNumber_tx.Click += new System.EventHandler(this.PhoneNumber_tx_Click);
            this.PhoneNumber_tx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PhoneNumber_tx_KeyPress);
            // 
            // Location_tx
            // 
            // 
            // 
            // 
            this.Location_tx.CustomButton.Image = null;
            this.Location_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.Location_tx.CustomButton.Name = "";
            this.Location_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Location_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Location_tx.CustomButton.TabIndex = 1;
            this.Location_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Location_tx.CustomButton.UseSelectable = true;
            this.Location_tx.CustomButton.Visible = false;
            this.Location_tx.Lines = new string[0];
            this.Location_tx.Location = new System.Drawing.Point(557, 350);
            this.Location_tx.MaxLength = 32767;
            this.Location_tx.Name = "Location_tx";
            this.Location_tx.PasswordChar = '\0';
            this.Location_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Location_tx.SelectedText = "";
            this.Location_tx.SelectionLength = 0;
            this.Location_tx.SelectionStart = 0;
            this.Location_tx.ShortcutsEnabled = true;
            this.Location_tx.Size = new System.Drawing.Size(192, 23);
            this.Location_tx.TabIndex = 51;
            this.Location_tx.UseSelectable = true;
            this.Location_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Location_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel11.Location = new System.Drawing.Point(91, 376);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(36, 19);
            this.metroLabel11.TabIndex = 50;
            this.metroLabel11.Text = "Age";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel10.Location = new System.Drawing.Point(462, 162);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(89, 19);
            this.metroLabel10.TabIndex = 49;
            this.metroLabel10.Text = "Department";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel9.Location = new System.Drawing.Point(477, 115);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(45, 19);
            this.metroLabel9.TabIndex = 48;
            this.metroLabel9.Text = "Email";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.Location = new System.Drawing.Point(462, 224);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(88, 19);
            this.metroLabel8.TabIndex = 47;
            this.metroLabel8.Text = "Designation";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(477, 285);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(58, 19);
            this.metroLabel7.TabIndex = 46;
            this.metroLabel7.Text = "Gender";
            // 
            // DateOfBirth_tx
            // 
            this.DateOfBirth_tx.Location = new System.Drawing.Point(178, 324);
            this.DateOfBirth_tx.Name = "DateOfBirth_tx";
            this.DateOfBirth_tx.Size = new System.Drawing.Size(200, 20);
            this.DateOfBirth_tx.TabIndex = 45;
            this.DateOfBirth_tx.ValueChanged += new System.EventHandler(this.DateOfBirth_tx_ValueChanged);
            // 
            // Designation_tx
            // 
            this.Designation_tx.FormattingEnabled = true;
            this.Designation_tx.ItemHeight = 23;
            this.Designation_tx.Items.AddRange(new object[] {
            "Programmer",
            "Administrater",
            "Business Analyst",
            "Marketier"});
            this.Designation_tx.Location = new System.Drawing.Point(557, 224);
            this.Designation_tx.Name = "Designation_tx";
            this.Designation_tx.Size = new System.Drawing.Size(183, 29);
            this.Designation_tx.TabIndex = 43;
            this.Designation_tx.UseSelectable = true;
            // 
            // Gender_tx
            // 
            this.Gender_tx.FormattingEnabled = true;
            this.Gender_tx.ItemHeight = 23;
            this.Gender_tx.Items.AddRange(new object[] {
            "Male",
            "Female",
            "NA"});
            this.Gender_tx.Location = new System.Drawing.Point(557, 282);
            this.Gender_tx.Name = "Gender_tx";
            this.Gender_tx.Size = new System.Drawing.Size(183, 29);
            this.Gender_tx.TabIndex = 42;
            this.Gender_tx.UseSelectable = true;
            this.Gender_tx.SelectedIndexChanged += new System.EventHandler(this.Gender_tx_SelectedIndexChanged);
            // 
            // Department_tx
            // 
            this.Department_tx.FormattingEnabled = true;
            this.Department_tx.ItemHeight = 23;
            this.Department_tx.Items.AddRange(new object[] {
            "Software Department",
            "Technical Support",
            "Sales and marketing"});
            this.Department_tx.Location = new System.Drawing.Point(557, 162);
            this.Department_tx.Name = "Department_tx";
            this.Department_tx.Size = new System.Drawing.Size(183, 29);
            this.Department_tx.TabIndex = 41;
            this.Department_tx.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(78, 325);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(94, 19);
            this.metroLabel6.TabIndex = 40;
            this.metroLabel6.Text = "Date of Birth";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(91, 285);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(79, 19);
            this.metroLabel5.TabIndex = 39;
            this.metroLabel5.Text = "Phone No.";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(91, 238);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(79, 19);
            this.metroLabel4.TabIndex = 38;
            this.metroLabel4.Text = "Last Name";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(73, 197);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(97, 19);
            this.metroLabel3.TabIndex = 37;
            this.metroLabel3.Text = "Middle name";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(102, 158);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(68, 19);
            this.metroLabel2.TabIndex = 36;
            this.metroLabel2.Text = "Surname";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(91, 115);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(57, 19);
            this.metroLabel1.TabIndex = 35;
            this.metroLabel1.Text = "CIL No.";
            // 
            // Email_tx
            // 
            // 
            // 
            // 
            this.Email_tx.CustomButton.Image = null;
            this.Email_tx.CustomButton.Location = new System.Drawing.Point(219, 1);
            this.Email_tx.CustomButton.Name = "";
            this.Email_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Email_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Email_tx.CustomButton.TabIndex = 1;
            this.Email_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Email_tx.CustomButton.UseSelectable = true;
            this.Email_tx.CustomButton.Visible = false;
            this.Email_tx.Lines = new string[0];
            this.Email_tx.Location = new System.Drawing.Point(557, 115);
            this.Email_tx.MaxLength = 32767;
            this.Email_tx.Name = "Email_tx";
            this.Email_tx.PasswordChar = '\0';
            this.Email_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Email_tx.SelectedText = "";
            this.Email_tx.SelectionLength = 0;
            this.Email_tx.SelectionStart = 0;
            this.Email_tx.ShortcutsEnabled = true;
            this.Email_tx.Size = new System.Drawing.Size(241, 23);
            this.Email_tx.TabIndex = 34;
            this.Email_tx.UseSelectable = true;
            this.Email_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Email_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Surname_tx
            // 
            // 
            // 
            // 
            this.Surname_tx.CustomButton.Image = null;
            this.Surname_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.Surname_tx.CustomButton.Name = "";
            this.Surname_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Surname_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Surname_tx.CustomButton.TabIndex = 1;
            this.Surname_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Surname_tx.CustomButton.UseSelectable = true;
            this.Surname_tx.CustomButton.Visible = false;
            this.Surname_tx.Lines = new string[0];
            this.Surname_tx.Location = new System.Drawing.Point(178, 158);
            this.Surname_tx.MaxLength = 32767;
            this.Surname_tx.Name = "Surname_tx";
            this.Surname_tx.PasswordChar = '\0';
            this.Surname_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Surname_tx.SelectedText = "";
            this.Surname_tx.SelectionLength = 0;
            this.Surname_tx.SelectionStart = 0;
            this.Surname_tx.ShortcutsEnabled = true;
            this.Surname_tx.Size = new System.Drawing.Size(192, 23);
            this.Surname_tx.TabIndex = 33;
            this.Surname_tx.UseSelectable = true;
            this.Surname_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Surname_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Age_tx
            // 
            // 
            // 
            // 
            this.Age_tx.CustomButton.Image = null;
            this.Age_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.Age_tx.CustomButton.Name = "";
            this.Age_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Age_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Age_tx.CustomButton.TabIndex = 1;
            this.Age_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Age_tx.CustomButton.UseSelectable = true;
            this.Age_tx.CustomButton.Visible = false;
            this.Age_tx.Lines = new string[0];
            this.Age_tx.Location = new System.Drawing.Point(178, 376);
            this.Age_tx.MaxLength = 32767;
            this.Age_tx.Name = "Age_tx";
            this.Age_tx.PasswordChar = '\0';
            this.Age_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Age_tx.SelectedText = "";
            this.Age_tx.SelectionLength = 0;
            this.Age_tx.SelectionStart = 0;
            this.Age_tx.ShortcutsEnabled = true;
            this.Age_tx.Size = new System.Drawing.Size(192, 23);
            this.Age_tx.TabIndex = 32;
            this.Age_tx.UseSelectable = true;
            this.Age_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Age_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.Age_tx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Age_tx_KeyPress);
            // 
            // LastName_tx
            // 
            // 
            // 
            // 
            this.LastName_tx.CustomButton.Image = null;
            this.LastName_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.LastName_tx.CustomButton.Name = "";
            this.LastName_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.LastName_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.LastName_tx.CustomButton.TabIndex = 1;
            this.LastName_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.LastName_tx.CustomButton.UseSelectable = true;
            this.LastName_tx.CustomButton.Visible = false;
            this.LastName_tx.Lines = new string[0];
            this.LastName_tx.Location = new System.Drawing.Point(178, 238);
            this.LastName_tx.MaxLength = 32767;
            this.LastName_tx.Name = "LastName_tx";
            this.LastName_tx.PasswordChar = '\0';
            this.LastName_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.LastName_tx.SelectedText = "";
            this.LastName_tx.SelectionLength = 0;
            this.LastName_tx.SelectionStart = 0;
            this.LastName_tx.ShortcutsEnabled = true;
            this.LastName_tx.Size = new System.Drawing.Size(192, 23);
            this.LastName_tx.TabIndex = 31;
            this.LastName_tx.UseSelectable = true;
            this.LastName_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.LastName_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // MiddleName_tx
            // 
            // 
            // 
            // 
            this.MiddleName_tx.CustomButton.Image = null;
            this.MiddleName_tx.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.MiddleName_tx.CustomButton.Name = "";
            this.MiddleName_tx.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.MiddleName_tx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.MiddleName_tx.CustomButton.TabIndex = 1;
            this.MiddleName_tx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MiddleName_tx.CustomButton.UseSelectable = true;
            this.MiddleName_tx.CustomButton.Visible = false;
            this.MiddleName_tx.Lines = new string[0];
            this.MiddleName_tx.Location = new System.Drawing.Point(178, 197);
            this.MiddleName_tx.MaxLength = 32767;
            this.MiddleName_tx.Name = "MiddleName_tx";
            this.MiddleName_tx.PasswordChar = '\0';
            this.MiddleName_tx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.MiddleName_tx.SelectedText = "";
            this.MiddleName_tx.SelectionLength = 0;
            this.MiddleName_tx.SelectionStart = 0;
            this.MiddleName_tx.ShortcutsEnabled = true;
            this.MiddleName_tx.Size = new System.Drawing.Size(192, 23);
            this.MiddleName_tx.TabIndex = 30;
            this.MiddleName_tx.UseSelectable = true;
            this.MiddleName_tx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.MiddleName_tx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // CILNo_tx
            // 
            this.CILNo_tx.FormattingEnabled = true;
            this.CILNo_tx.ItemHeight = 23;
            this.CILNo_tx.Location = new System.Drawing.Point(178, 115);
            this.CILNo_tx.Name = "CILNo_tx";
            this.CILNo_tx.Size = new System.Drawing.Size(192, 29);
            this.CILNo_tx.TabIndex = 58;
            this.CILNo_tx.UseSelectable = true;
            this.CILNo_tx.SelectedIndexChanged += new System.EventHandler(this.CILNo_tx_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(521, 452);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 36);
            this.button1.TabIndex = 60;
            this.button1.Text = "CLOSE";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Close_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(42)))), ((int)(((byte)(122)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(207, 452);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(135, 36);
            this.btnUpdate.TabIndex = 59;
            this.btnUpdate.Text = "UPDATE";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(145, 528);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 16);
            this.label6.TabIndex = 62;
            this.label6.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // caption
            // 
            this.caption.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.caption.AutoSize = true;
            this.caption.Font = new System.Drawing.Font("Consolas", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.caption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.caption.Location = new System.Drawing.Point(485, 526);
            this.caption.Name = "caption";
            this.caption.Size = new System.Drawing.Size(200, 18);
            this.caption.TabIndex = 61;
            this.caption.Text = "Your Health Data Partner";
            // 
            // EmployeeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 556);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.caption);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.CILNo_tx);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.PhoneNumber_tx);
            this.Controls.Add(this.Location_tx);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.DateOfBirth_tx);
            this.Controls.Add(this.Designation_tx);
            this.Controls.Add(this.Gender_tx);
            this.Controls.Add(this.Department_tx);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.Email_tx);
            this.Controls.Add(this.Surname_tx);
            this.Controls.Add(this.Age_tx);
            this.Controls.Add(this.LastName_tx);
            this.Controls.Add(this.MiddleName_tx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeEditForm";
            this.Text = "Edit Employee";
            this.Load += new System.EventHandler(this.EmployeeEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroTextBox PhoneNumber_tx;
        private MetroFramework.Controls.MetroTextBox Location_tx;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.DateTimePicker DateOfBirth_tx;
        private MetroFramework.Controls.MetroComboBox Designation_tx;
        private MetroFramework.Controls.MetroComboBox Gender_tx;
        private MetroFramework.Controls.MetroComboBox Department_tx;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox Email_tx;
        private MetroFramework.Controls.MetroTextBox Surname_tx;
        private MetroFramework.Controls.MetroTextBox Age_tx;
        private MetroFramework.Controls.MetroTextBox LastName_tx;
        private MetroFramework.Controls.MetroTextBox MiddleName_tx;
        private MetroFramework.Controls.MetroComboBox CILNo_tx;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label caption;
    }
}