﻿
namespace CIL_Allowance_Management_System.Front_End
{
    partial class LookUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddDepartment = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.caption = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAddDesignation = new System.Windows.Forms.Button();
            this.btnChangeArrivalTime = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddDepartment
            // 
            this.btnAddDepartment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDepartment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(42)))), ((int)(((byte)(122)))));
            this.btnAddDepartment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnAddDepartment.ForeColor = System.Drawing.Color.White;
            this.btnAddDepartment.Location = new System.Drawing.Point(173, 73);
            this.btnAddDepartment.Name = "btnAddDepartment";
            this.btnAddDepartment.Size = new System.Drawing.Size(191, 29);
            this.btnAddDepartment.TabIndex = 69;
            this.btnAddDepartment.Text = "Add Department";
            this.btnAddDepartment.UseVisualStyleBackColor = false;
            this.btnAddDepartment.Click += new System.EventHandler(this.btnAddDepartment_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(7, 350);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 16);
            this.label6.TabIndex = 68;
            this.label6.Text = "Licensed to ClinicMaster INTERNATIONAL 2021";
            // 
            // caption
            // 
            this.caption.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.caption.AutoSize = true;
            this.caption.Font = new System.Drawing.Font("Consolas", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.caption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.caption.Location = new System.Drawing.Point(316, 350);
            this.caption.Name = "caption";
            this.caption.Size = new System.Drawing.Size(200, 18);
            this.caption.TabIndex = 67;
            this.caption.Text = "Your Health Data Partner";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(411, 302);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 29);
            this.btnClose.TabIndex = 66;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddDesignation
            // 
            this.btnAddDesignation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDesignation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(42)))), ((int)(((byte)(122)))));
            this.btnAddDesignation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnAddDesignation.ForeColor = System.Drawing.Color.White;
            this.btnAddDesignation.Location = new System.Drawing.Point(173, 141);
            this.btnAddDesignation.Name = "btnAddDesignation";
            this.btnAddDesignation.Size = new System.Drawing.Size(191, 29);
            this.btnAddDesignation.TabIndex = 70;
            this.btnAddDesignation.Text = "Add Designation";
            this.btnAddDesignation.UseVisualStyleBackColor = false;
            this.btnAddDesignation.Click += new System.EventHandler(this.btnAddDesignation_Click);
            // 
            // btnChangeArrivalTime
            // 
            this.btnChangeArrivalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeArrivalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(42)))), ((int)(((byte)(122)))));
            this.btnChangeArrivalTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeArrivalTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnChangeArrivalTime.ForeColor = System.Drawing.Color.White;
            this.btnChangeArrivalTime.Location = new System.Drawing.Point(173, 197);
            this.btnChangeArrivalTime.Name = "btnChangeArrivalTime";
            this.btnChangeArrivalTime.Size = new System.Drawing.Size(191, 29);
            this.btnChangeArrivalTime.TabIndex = 71;
            this.btnChangeArrivalTime.Text = "Change ArrivalTime";
            this.btnChangeArrivalTime.UseVisualStyleBackColor = false;
            this.btnChangeArrivalTime.Click += new System.EventHandler(this.btnChangeArrivalTime_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(42)))), ((int)(((byte)(122)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(173, 247);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 29);
            this.button1.TabIndex = 72;
            this.button1.Text = "Change Allowance Given";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // LookUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 378);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnChangeArrivalTime);
            this.Controls.Add(this.btnAddDesignation);
            this.Controls.Add(this.btnAddDepartment);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.caption);
            this.Controls.Add(this.btnClose);
            this.MaximizeBox = false;
            this.Name = "LookUpForm";
            this.Text = "LookUpForm";
            this.Load += new System.EventHandler(this.LookUpForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAddDepartment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label caption;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAddDesignation;
        private System.Windows.Forms.Button btnChangeArrivalTime;
        private System.Windows.Forms.Button button1;
    }
}