
--this script belongs to clinicMaster

use master
GO

if exists (select name from sysdatabases where name = 'AllowanceDB')
    drop database AllowanceDB
GO

create database AllowanceDB;
GO