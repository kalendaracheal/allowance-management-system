
use AllowanceDB
go

if exists (select * from sysobjects where name = 'Attendence')
	drop table Attendence
go
if exists (select * from sysobjects where name = 'AllowanceDetails')
	drop table AllowanceDetails
go
if exists (select * from sysobjects where name = 'AllowanceGiven')
	drop table AllowanceGiven
go
if exists (select * from sysobjects where name = 'ArrivalTime')
	drop table ArrivalTime
go
--------------------CREATE TABLE Attendence------------------------------------------------------------------------------------------------------
USE AllowanceDB
GO

CREATE TABLE Attendence
(
AttendenceDate Date not null ,
AttendenceNo int IDENTITY(1,1) PRIMARY KEY,
SurName varchar(50) not null,
MiddleName varchar(50) null,
LastName varchar(50) not null,
CILNo varchar(20) not null ,
Department varchar(40) not null,
Designation varchar(50),
TimeIn Time,
DifferenceInTime varchar(50) not null,
AttendenceStatus varchar(30) not null,
AllowanceToBeGiven int not null,
IssueStatus varchar(20) 
);
go

--------------------CREATE PROCEDURE SaveAttendence------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'SaveAttendence')
drop procedure SaveAttendence
go

CREATE PROCEDURE SaveAttendence
@AttendenceDate DATE,
@TimeIn TIME,
@SurName varchar(50),
@MiddleName varchar (50),
@LastName varchar (50),
@CILNo varchar(20),
@Department varchar(40),
@Designation varchar(50) ,
@DifferenceInTime varchar(50),
@AttendenceStatus varchar(30),
@AllowanceToBeGiven int	,
@IssueStatus varchar(20)

AS
IF EXISTS(SELECT * FROM Employees Where CILNO = @CILNo and SurName = @SurName and MiddleName = @MiddleName and LastName = @LastName)
BEGIN
INSERT INTO Attendence
(AttendenceDate, TimeIn, SurName, MiddleName, LastName, CILNo, Department, Designation, DifferenceInTime, AttendenceStatus,AllowanceToBeGiven,IssueStatus)
VALUES(@AttendenceDate,@TimeIn,@SurName,@MiddleName,@LastName, @CILNo, @Department, @Designation, @DifferenceInTime, @AttendenceStatus, @AllowanceToBeGiven,@IssueStatus)

END

GO
 
--------------------CREATE PROCEDURE getAttendenceDetails-----------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'getAttendencedetails')
drop procedure getAttendencedetails
go

CREATE PROC getAttendencedetails
@CILNo varchar (20), 
@AttendenceDate Date ,
@SurName varchar(50),
@MiddleName varchar(50),
@LastName varchar(50),
@Department varchar (50),
@Designation varchar (50)
AS
IF EXISTS(SELECT CILNo, SurName, MiddleName, LastName, Department, Designation FROM Employees)
 BEGIN
Select * from Attendence where CILNo = @CILNo AND AttendenceDate = @AttendenceDate
END
GO
	


--------------------CREATE PROCEDURE getEmployeeinAttendence-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'getemployeeinAttendence')
drop procedure getemployeeinAttendence
go

	CREATE PROC getemployeeinAttendence
	AS
	BEGIN
	select * FROM Attendence
	END
	GO
	
	--------------------CREATE Table ArrivalTime-------------------------------------------------------------------------------------------------------------------------------------------------
	CREATE TABLE ArrivalTime
	(
	ArrrivalTimeID INT IDENTITY(1,1) PRIMARY KEY,
	hours int NOT NULL,
	minutes int NOT NULL,
	seconds int NOT NULL
	);
	go

	insert into ArrivalTime (hours, minutes, seconds) values(8,00,00)
--------------------CREATE PROCEDURE updateArrivalTime-------------------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'updateArrivalTime')
drop procedure updateArrivalTime
go
	CREATE PROC updateArrivalTime
	@hours int,
	@minutes int,
	@seconds int
	AS
	BEGIN	
	UPDATE ArrivalTime
	SET hours = @hours, minutes = @minutes,seconds = @seconds
	END
	GO

	
--------------------CREATE PROCEDURE getArrivalTime-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'getArrivalTime')
drop procedure getArrivalTime
go

	CREATE PROC getArrivalTime
	
	AS
	BEGIN
	SELECT * FROM ArrivalTime
	END
	GO


--------------------CREATE PROCEDURE get_EmployeebyFullNameInAttendence-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'get_EmployeeInAttendence')
drop procedure get_EmployeeInAttendence
go

	CREATE proc get_EmployeeInAttendence
	@CILNo varchar(50),
	@Department varchar(50)	,
	@AttendenceDate Date	
	As
	BEGIN
	   select AttendenceDate, AttendenceNo ,SurName, MiddleName,LastName,CILNo,Department,Designation,TimeIn,DifferenceInTime
	   AS TimeDifference,AttendenceStatus, AllowanceToBeGiven AS Allowance,IssueStatus from Attendence 
	   where CILNo = @CILNo AND Department  = @Department AND AttendenceDate =@AttendenceDate
	END
	Go



--------------------CREATE PROCEDURE get_EmployeebyFullNameInAttendence-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'get_EmployeebyFullNameInAttendence')
drop procedure get_EmployeebyFullNameInAttendence
go
	CREATE proc get_EmployeebyFullNameInAttendence
	@CILNo varchar(50)	
	As
	BEGIN
	   select AttendenceDate, AttendenceNo AS No, (SurName+' '+ MiddleName+' '+LastName)
	   AS FullName,CILNo,Department,Designation,TimeIn,DifferenceInTime AS TimeDifference,AttendenceStatus, AllowanceToBeGiven 
	   AS Allowance from Attendence 
	   where CILNo = @CILNo;
	END
	Go
--------------------CREATE PROCEDURE get_EmployeebyDepartmentInAttendence-------------------------------------------------------------------------------------------------------------------------------------------------
	
if exists (select * from sysobjects where name = 'get_EmployeebyDepartmentInAttendence')
drop procedure get_EmployeebyDepartmentInAttendence
go
	CREATE proc get_EmployeebyDepartmentInAttendence
	@Department varchar(50)	
	As
	BEGIN
	   select AttendenceDate, AttendenceNo AS No, (SurName+' '+ MiddleName+' '+LastName) 
	   AS FullName,CILNo,Department,Designation,TimeIn,DifferenceInTime AS 
	   TimeDifference,AttendenceStatus, AllowanceToBeGiven AS Allowance from Attendence where Department = @Department
    END
	Go
--------------------CREATE PROCEDURE get_EmployeebyAttendenceDateInAttendence-------------------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'get_EmployeebyAttendenceDateInAttendence')
drop procedure get_EmployeebyAttendenceDateInAttendence
go

	CREATE proc get_EmployeebyAttendenceDateInAttendence
	@AttendenceDate Date
	As
	BEGIN
	   select AttendenceDate, AttendenceNo, SurName, MiddleName,LastName,CILNo,Department,Designation,TimeIn,DifferenceInTime 
	   AS TimeDifference,AttendenceStatus, AllowanceToBeGiven AS Allowance 
	   from Attendence 
	   where AttendenceDate = @AttendenceDate
    END
	Go


--------------------CREATE PROCEDURE get_EmployeebyDesignationInAttendence-------------------------------------------------------------------------------------------------------------------------------------------------
	
if exists (select * from sysobjects where name = 'get_EmployeebyDesignationInAttendence')
drop procedure get_EmployeebyDesignationInAttendence
go
	CREATE proc get_EmployeebyDesignationInAttendence
	@Designation varchar(50)	
	As
	BEGIN
	   select AttendenceDate, AttendenceNo AS No, (SurName+' '+ MiddleName+' '+LastName) 
	   AS FullName,CILNo,Department,Designation,TimeIn,DifferenceInTime AS TimeDifference,AttendenceStatus, AllowanceToBeGiven
	   AS Allowance from Attendence where Designation = @Designation
    END
	Go

--------------------CREATE PROCEDURE getEmployees-------------------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'getEmployees')
drop procedure getEmployees
go

	CREATE PROC getEmployees
	As
	BEGIN
	   select AttendenceDate, AttendenceNo AS No, (SurName+' '+ MiddleName+' '+LastName) 
	   AS FullName,CILNo,Department,Designation,TimeIn,DifferenceInTime AS TimeDifference,AttendenceStatus, 
	   AllowanceToBeGiven AS Allowance from Attendence
    END
	Go

--------------------------------ALLOWANCE---------------------------------------------------------------

if exists (select * from sysobjects where name = 'LoadAttendanceDetails')
drop procedure LoadAttendanceDetails
go

CREATE procedure LoadAttendanceDetails
    @CILNo varchar(50),
	@StartDate date,
	@EndDate date
   AS
   BEGIN
     select * from Attendence where CILNo = @CILNo and
	 AttendenceDate >= @StartDate and AttendenceDate <= @EndDate order by AttendenceDate 
    END
	go

	----------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'AllowanceDetails')
drop procedure AllowanceDetails
go

create table AllowanceDetails(
  AllowanceNo int identity(1,1) PRIMARY KEY,
  CILNo varchar(50),
  FullName varchar(100),
  Designation varchar(50),
  Department varchar(50),
  IssueDate date,
  AmountGiven int,
  Startdate date,
  EndDate date

  )
  go

  -------------------------------------------------------------------------------------
  
CREATE TABLE AllowanceGiven
	 (
	 AllowanceId int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	 Early int,
	 Late int 
	 )
	 GO

insert into  AllowanceGiven VALUES(5000, 2000)
go
--------------------------------------------------------------------------------------------
 
 if exists (select * from sysobjects where name = 'addAllowanceDetails')
drop procedure addAllowanceDetails
go
 
 create procedure addAllowanceDetails
    @CILNo varchar(50),
    @FullName varchar(100),
    @Designation varchar(50),
    @Department varchar(50),
    @IssueDate date,
    @AmountGiven int,
    @Startdate date,
    @EndDate date
  AS
  BEGIN
  insert into AllowanceDetails
   (CILNo,FullName,Designation,Department,IssueDate,AmountGiven,StartDate,EndDate)
   values(@CILNo,@FullName,@Designation,@Department,@IssueDate,@AmountGiven,@Startdate,@EndDate)
 
 END
 GO
 ----------------------------------------------------------------------------------------
 
if exists (select * from sysobjects where name = 'GetAllAllowanceDetails')
drop procedure GetAllAllowanceDetails
go

CREATE PROCEDURE GetAllAllowanceDetails
     @StartDate date,
	 @EndDate date
	
	  AS
	  BEGIN
	  select * from AllowanceDetails where
	  IssueDate >= @StartDate and IssueDate <= @EndDate 
	  END
	  go
--------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'updateAttendenceWithIssuedStatus')
drop procedure updateAttendenceWithIssuedStatus
go

create procedure updateAttendenceWithIssuedStatus
   @StartDate date,
   @EndDate date,
   @CILNo varchar(20)
   as
   begin
     update Attendence set IssueStatus = 'Issued' 
	 where
	 CILNo = @CILNo
	 and 
	 AttendenceDate >= @StartDate and AttendenceDate <= @EndDate 
   end
   go

   --------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'IssuedAttendenceDetails')
drop procedure IssuedAttendenceDetails
go

   create procedure IssuedAttendenceDetails
  @StartDate date,
  @CILNo varchar(20),
  @EndDate date
   as
   begin
    select * from Attendence 
	where IssueStatus = 'Issued'
	and
	CILNo = @CILNo
	and 
	AttendenceDate >= @StartDate and AttendenceDate <= @EndDate 
	order by AttendenceDate
	end 
	go
--------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'UpdateAllowanceGiven')
drop procedure UpdateAllowanceGiven
go

create procedure UpdateAllowanceGiven
 @Early int,
 @Late int
 as
  begin
   update AllowanceGiven set
   Early = @Early , Late = @Late
   end
   go
-----------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetAllowanceGiven')
drop procedure GetAllowanceGiven
go

create procedure GetAllowanceGiven
 
  as
  begin
    select * from AllowanceGiven

  end
  ------------------------------------------------------------------------------------------------------