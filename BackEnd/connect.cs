﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIL_Allowance_Management_System.BackEnd
{
    public partial class connect : Form
    {
        public connect()
        {
            InitializeComponent();
        }

        private void btnconnect_Click(object sender, EventArgs e)
        {
            string connectionString = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", txtservername,txtdatabase,txtuser,txtpassword);
            try
            {
                ConnectionHelper helper = new ConnectionHelper(connectionString);
                if (helper.IsConnection)
                    MessageBox.Show("Connection succeeded");
            }
            catch (Exception x)
            {

                MessageBox.Show(x.Message);
            }
                
        }

        private void connect_Load(object sender, EventArgs e)
        {
            txtservername.Items.Add(".");
            txtservername.Items.Add("(local)");
            txtservername.Items.Add(@".\SQLEXPRESS");
            txtservername.Items.Add((@"\{0}\SQLSERVER", Environment.MachineName));

        }
    }
}
