﻿
namespace CIL_Allowance_Management_System.BackEnd
{
    partial class connect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtservername = new MetroFramework.Controls.MetroComboBox();
            this.txtdatabase = new MetroFramework.Controls.MetroTextBox();
            this.txtuser = new MetroFramework.Controls.MetroTextBox();
            this.txtpassword = new MetroFramework.Controls.MetroTextBox();
            this.btnconnect = new MetroFramework.Controls.MetroButton();
            this.label = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // txtservername
            // 
            this.txtservername.FormattingEnabled = true;
            this.txtservername.ItemHeight = 24;
            this.txtservername.Location = new System.Drawing.Point(122, 65);
            this.txtservername.Name = "txtservername";
            this.txtservername.Size = new System.Drawing.Size(253, 30);
            this.txtservername.TabIndex = 1;
            this.txtservername.UseSelectable = true;
            // 
            // txtdatabase
            // 
            // 
            // 
            // 
            this.txtdatabase.CustomButton.Image = null;
            this.txtdatabase.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.txtdatabase.CustomButton.Name = "";
            this.txtdatabase.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtdatabase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtdatabase.CustomButton.TabIndex = 1;
            this.txtdatabase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtdatabase.CustomButton.UseSelectable = true;
            this.txtdatabase.CustomButton.Visible = false;
            this.txtdatabase.Lines = new string[0];
            this.txtdatabase.Location = new System.Drawing.Point(122, 139);
            this.txtdatabase.MaxLength = 32767;
            this.txtdatabase.Name = "txtdatabase";
            this.txtdatabase.PasswordChar = '\0';
            this.txtdatabase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtdatabase.SelectedText = "";
            this.txtdatabase.SelectionLength = 0;
            this.txtdatabase.SelectionStart = 0;
            this.txtdatabase.ShortcutsEnabled = true;
            this.txtdatabase.Size = new System.Drawing.Size(253, 23);
            this.txtdatabase.TabIndex = 2;
            this.txtdatabase.UseSelectable = true;
            this.txtdatabase.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtdatabase.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtuser
            // 
            // 
            // 
            // 
            this.txtuser.CustomButton.Image = null;
            this.txtuser.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.txtuser.CustomButton.Name = "";
            this.txtuser.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtuser.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtuser.CustomButton.TabIndex = 1;
            this.txtuser.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtuser.CustomButton.UseSelectable = true;
            this.txtuser.CustomButton.Visible = false;
            this.txtuser.Lines = new string[0];
            this.txtuser.Location = new System.Drawing.Point(122, 215);
            this.txtuser.MaxLength = 32767;
            this.txtuser.Name = "txtuser";
            this.txtuser.PasswordChar = '\0';
            this.txtuser.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtuser.SelectedText = "";
            this.txtuser.SelectionLength = 0;
            this.txtuser.SelectionStart = 0;
            this.txtuser.ShortcutsEnabled = true;
            this.txtuser.Size = new System.Drawing.Size(253, 23);
            this.txtuser.TabIndex = 3;
            this.txtuser.UseSelectable = true;
            this.txtuser.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtuser.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtpassword
            // 
            // 
            // 
            // 
            this.txtpassword.CustomButton.Image = null;
            this.txtpassword.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.txtpassword.CustomButton.Name = "";
            this.txtpassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtpassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtpassword.CustomButton.TabIndex = 1;
            this.txtpassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtpassword.CustomButton.UseSelectable = true;
            this.txtpassword.CustomButton.Visible = false;
            this.txtpassword.Lines = new string[0];
            this.txtpassword.Location = new System.Drawing.Point(122, 291);
            this.txtpassword.MaxLength = 32767;
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '\0';
            this.txtpassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtpassword.SelectedText = "";
            this.txtpassword.SelectionLength = 0;
            this.txtpassword.SelectionStart = 0;
            this.txtpassword.ShortcutsEnabled = true;
            this.txtpassword.Size = new System.Drawing.Size(253, 23);
            this.txtpassword.TabIndex = 4;
            this.txtpassword.UseSelectable = true;
            this.txtpassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtpassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnconnect
            // 
            this.btnconnect.Location = new System.Drawing.Point(145, 387);
            this.btnconnect.Name = "btnconnect";
            this.btnconnect.Size = new System.Drawing.Size(140, 43);
            this.btnconnect.TabIndex = 5;
            this.btnconnect.Text = "Connect";
            this.btnconnect.UseSelectable = true;
            this.btnconnect.Click += new System.EventHandler(this.btnconnect_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(34, 75);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(49, 20);
            this.label.TabIndex = 6;
            this.label.Text = "server";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(34, 294);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(67, 20);
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "password";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(34, 218);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(36, 20);
            this.metroLabel2.TabIndex = 8;
            this.metroLabel2.Text = "user";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(34, 139);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(64, 20);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "database";
            // 
            // connect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 503);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.btnconnect);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.txtuser);
            this.Controls.Add(this.txtdatabase);
            this.Controls.Add(this.txtservername);
            this.Name = "connect";
            this.Text = "connect";
            this.Load += new System.EventHandler(this.connect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox txtservername;
        private MetroFramework.Controls.MetroTextBox txtdatabase;
        private MetroFramework.Controls.MetroTextBox txtuser;
        private MetroFramework.Controls.MetroTextBox txtpassword;
        private MetroFramework.Controls.MetroButton btnconnect;
        private MetroFramework.Controls.MetroLabel label;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
    }
}