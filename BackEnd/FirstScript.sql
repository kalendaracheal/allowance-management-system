
use AllowanceDB
go

if exists (select * from sysobjects where name = 'AdminUsers')
	drop table AdminUsers
go

if exists (select * from sysobjects where name = 'Employees')
	drop table Employees
go

if exists (select * from sysobjects where name = 'lookUpDesignation')
	drop table lookUpDesignation
go

if exists (select * from sysobjects where name = 'lookUpDepartment')
	drop table lookUpDepartment
go


if exists (select * from sysobjects where name = 'Roles')
	drop table Roles
go

------------------------------ROLES.......................................................................
create table Roles
(
   RoleId int identity(1,1) primary key,
   RoleName varchar(50),
   RoleDes varchar(50)
)

--------------------------------USERS-------------------------------------------------------------------------


CREATE TABLE AdminUsers
(
UserID INT IDENTITY(1,1) PRIMARY KEY, 
Username varchar(50) NOT NULL, 
Password varchar(50), 
Email varchar(50) NOT NULL, 
PhoneNumber BIGINT NOT NULL,
RoleName varchar(50)
);
go


--------------------CREATE PROCEDURE RegisterAdmin------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'RegisterAdmin')
drop procedure RegisterAdmin
go

CREATE PROC RegisterAdmin
@Username varchar(50),
@Password varchar(50),
@Email varchar(50),
@PhoneNumber bigint,
@RoleName varchar(50)
AS
BEGIN
INSERT INTO AdminUsers(Username, Password, Email, PhoneNumber, RoleName)
VALUES(@Username , @Password, @Email, @PhoneNumber, @RoleName)
END
GO


exec RegisterAdmin 'setup','pass','setup@gmail.com','076656733','Admin'
-----------------------------------insertroles-------------------------
if exists (select * from sysobjects where name = 'InsertRoles')
drop procedure InsertRoles
go

CREATE PROC InsertRoles
@RoleName varchar(50),
@RoleDes varchar(50)

AS
BEGIN
INSERT INTO Roles(RoleName, RoleDes)
VALUES(@RoleName , @RoleDes)
END
GO

exec InsertRoles 'Admin','Has access to every Thing'
exec InsertRoles 'Guest','Accesses the attemdence tab only'
exec InsertRoles 'Accountant','Accesses the allowance and Employee tab only'

--------------------------------------getRoles-------------------------
if exists (select * from sysobjects where name = 'getRoles')
drop procedure getRoles
go

CREATE PROC getRoles
AS
BEGIN
Select * from Roles
END
GO
--------------------CREATE PROCEDURE GetloginDetails------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'GetLoginDetails')
drop procedure GetLoginDetails
go

CREATE PROC GetLoginDetails
@Username varchar(50),
@Password varchar(50)
AS
BEGIN
SELECT * FROM AdminUsers WHERE Username = @Username AND Password = @Password
END
GO

--------------------CREATE PROCEDURE UpdateAdminUser------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'UpdateAdminUser')
drop procedure UpdateAdminUser
go
CREATE PROC UpdateAdminUser
@Username varchar(50),
@Password varchar(50),
@Email varchar(50),
@PhoneNumber bigint
AS
BEGIN
UPDATE AdminUsers
SET Username = @Username, Password = @Password, Email = @Email, PhoneNumber = @PhoneNumber 
WHERE Username = @Username
END
GO
----------------------------------------------delete admin user-----------------------------------
if exists (select * from sysobjects where name = 'DeleteAdminUser')
drop procedure DeleteAdminUser
go

CREATE PROC DeleteAdminUser
@Username varchar(50)
AS
BEGIN

DELETE FROM AdminUsers 
WHERE Username = @Username
END
GO

--------------------CREATE PROCEDURE getAdminUserDetails-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'getAdminUserDetails')
drop procedure getAdminUserDetails
go

CREATE PROC getAdminUserDetails
@Username varchar(50)
AS
 BEGIN
Select * from AdminUsers where Username = @Username 
END
GO

--------------------CREATE PROCEDURE getAdminList-------------------------------------------------------------------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'getAdminList')
drop procedure getAdminList
go

	CREATE PROC getAdminList
	AS
	BEGIN
	Select UserID, Username, Password ,Email,PhoneNumber FROM AdminUsers
	END
	GO

----------------------------------------EMPLOYEES------------------------------------------------------------------

CREATE TABLE Employees
(
CILNo varchar(20),
SurName varchar(50),
MiddleName varchar(50),
LastName varchar(50),
Gender varchar(10),
DateOfBirth date,
Age int,
PhoneNumber varchar(10),
Email varchar(50), 
Location varchar(50),
Designation varchar(50),
Department varchar(50),
FingerPrintImage image,
RegDate date
PRIMARY KEY(CILNo)
);

--inserting Employee procedure----------------------------------------------------------------------
if exists (select * from sysobjects where name = 'AddEmployee')
drop procedure AddEmployee
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure AddEmployee
   @CILNo varchar(20),
   @Surname varchar(50),
   @MiddleName varchar(50),
   @LastName varchar(50),
   @Gender varchar(10),
   @DateOfBirth date,
   @Age int,
   @PhoneNumber varchar(10),
   @Email varchar(50), 
   @Location varchar(50),
   @Designation varchar(50),
   @Department varchar(50),
   @RegDate date
   
AS
BEGIN
  insert into Employees
  (CILNo, SurName, MiddleName, LastName, Gender, DateOfBirth, Age, PhoneNumber, Email, Location, Designation, Department,RegDate)
  values(@CILNo, @Surname, @MiddleName, @LastName, @Gender, @DateOfBirth,@Age , @PhoneNumber, @Email, @Location, @Designation, @Department, @RegDate)
END
 GO

 

 --procedure for updating---------------------------------------------------------------------------------

 if exists (select * from sysobjects where name = 'editEmployee')
drop procedure editEmployee
go

 create procedure editEmployee
   @CILNo varchar(20),
   @Surname varchar(50),
   @MiddleName varchar(50),
   @LastName varchar(50),
   @Gender varchar(10),
   @DateOfBirth date,
   @Age int,
   @PhoneNumber varchar(10),
   @Email varchar(50), 
   @Location varchar(50),
   @Designation varchar(50),
   @Department varchar(50),
   @UpdateDate date
   AS
   BEGIN
     update Employees set SurName = @Surname, MiddleName = @MiddleName, LastName=@LastName,
	 Gender=@Gender, DateOfBirth=@DateOfBirth, Age=@Age, PhoneNumber=@PhoneNumber, Email=@Email, 
	 Location=@Location, Department=@Department, Designation=@Designation, RegDate=@UpdateDate 
	 where CILNo = @CILNo;
	END
	GO

	

	--procedure for displaying data
if exists (select * from sysobjects where name = 'display')
drop procedure display
go

create procedure display  
AS
BEGIN
  select * from Employees
END
 GO

 ---------------------------------------------------------------------------------------------

 if exists (select * from sysobjects where name = 'DeleteEmployee')
drop procedure DeleteEmployee
go
 
 create procedure DeleteEmployee
  @CILNo varchar(50)
AS
BEGIN
  delete from Employees where CILNo = @CILNo;
END
GO
	

--procedure for searching in edit form-----------------------------------------------------------------
 
 if exists (select * from sysobjects where name = 'GetEmployeeByCILNo')
drop procedure GetEmployeeByCILNO
go
 
 create procedure GetEmployeeByCILNO
   @CILNo varchar(20),
   @Surname varchar(50),
   @MiddleName varchar(50),
   @LastName varchar(50),
   @Gender varchar(10),
   @DateOfBirth date,
   @Age int,
   @PhoneNumber varchar(10),
   @Email varchar(50), 
   @Location varchar(50),
   @Designation varchar(50),
   @Department varchar(50),
   @RegDate date
   AS
   BEGIN
     select  SurName, MiddleName,LastName,
	 Gender, DateOfBirth, Age, PhoneNumber, Email, 
	 Location, Department, Designation, RegDate
	 from Employees where CILNo = @CILNo;
	END
	GO

	--drop procedure Search-------------------------------------------------------------
if exists (select * from sysobjects where name = 'SearchInEditForm')
drop procedure SearchInEditForm
go

  create proc SearchInEditForm
	@CILNo varchar(50)
	
	As
	BEGIN
	   select  CILNo,SurName, MiddleName, LastName,
	 Gender, DateOfBirth, Age, PhoneNumber, Email, 
	 Location, Department, Designation
	 from Employees where CILNo = @CILNo;

    END
	Go

	-------------------------------------------------------------------------------------
	if exists (select * from sysobjects where name = 'checkIfEmployeeExists')
   drop procedure checkIfEmployeeExists
    go
	create procedure checkIfEmployeeExists
	  @CILNo varchar(50)
	  AS
	  BEGIN
	   select * from Employees where CILNo = @CILNo
      END

	  go
-------------------------------------------------------------------------------------------------------
  if exists (select * from sysobjects where name = 'get_Employee')
  drop procedure get_Employee
  go

   CREATE proc get_Employee
	@CILNo varchar(50)
	
	As
	BEGIN
	   select  CILNo,SurName, MiddleName, LastName,
	 Department, Designation
	 from Employees where CILNo = @CILNo;
    END
	Go

------------------------------------------------------------------------------------------------------
--------------------CREATE PROCEDURE getDepartment-------------------------------------------------------------------------------------------------------------------------------------------------
    if exists (select * from sysobjects where name = 'getDepartment')
   drop procedure getDepartment
   go
	CREATE PROC getDepartment
	as
	begin
	select (SurName+' '+MiddleName+' '+LastName) as FullName, CILNo, Department,Designation FROM Employees
	END
	GO
	

--------------------CREATE PROCEDURE getDesignation-------------------------------------------------------------------------------------------------------------------------------------------------
     if exists (select * from sysobjects where name = 'getDesignation')
   drop procedure getDesignation
   go
	CREATE PROC getDesignation
	as
	begin
	select (SurName+' '+MiddleName+' '+LastName) as FullName, CILNo, Department,Designation FROM Employees
	END
	go

	----------------------------
		
--------------------CREATE PROCEDURE getemployee-------------------------------------------------------------------------------------------------------------------------------------------------
   if exists (select * from sysobjects where name = 'getemployee')
   drop procedure getemployee
   go

	CREATE PROC getemployee
	AS
	BEGIN
	select (SurName+' '+MiddleName+' '+LastName) as FullName, CILNo, Department,Designation
	FROM Employees
	END
	GO

---------------------LOOKUP--------------------------------------------------------------------------

create table lookUpDesignation
(
  DataId int  PRIMARY KEY,
  designationItem varchar(50)
  )

  go
  ------------------------------------------------------------------------------------------
    if exists (select * from sysobjects where name = 'addToDesignation')
drop procedure addToDesignation
go
	
	create procedure addToDesignation
          @DataId int, 
          @designationItem varchar(50)
    AS
    BEGIN
      insert into lookUpDesignation 
	  (DataId, designationItem) 
	  values(@DataId,@designationItem)
	END
	GO

	
	----------------------------------------------------------------------------------------
if exists (select * from sysobjects where name = 'addDesignation')
drop procedure addDesignation
go 
 
 create procedure addDesignation
    AS
    BEGIN
   select * from lookUpDesignation
	END
	GO

-------------------------------------------------------------------------------------------------------------	
create table lookUpDepartment
(
  DataId int  PRIMARY KEY,
  departmentItem varchar(50)
  )
  go
-------------------------------------------------------------------------------------------------------------	
 if exists (select * from sysobjects where name = 'addToDepartment')
drop procedure addToDepartment
go

  create procedure addToDepartment
  @DataId int, 
  @departmentItem varchar(50)
  AS
  BEGIN
    insert into lookUpDepartment 
	(DataId, departmentItem) 
	values(@DataId,@departmentItem)
	END
	GO

	
	----------------------------------------------------------------------------------------------------------------

	if exists (select * from sysobjects where name = 'addDepartment')
    drop procedure addDepartment
    go
    create procedure addDepartment
 
    AS
    BEGIN
    select * from lookUpDepartment
	END
	GO




