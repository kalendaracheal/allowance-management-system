﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIL_Allowance_Management_System.BackEnd
{
    public class ConnectionHelper
    {
        SqlConnection conn;

        public  ConnectionHelper(string connectionString)
        {
            conn = new SqlConnection(connectionString);
        }

        public bool IsConnection
        {
            get
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                return true;
            }
        }
    }
}
