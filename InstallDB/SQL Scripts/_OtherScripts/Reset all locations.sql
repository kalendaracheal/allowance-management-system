--------run cursor below, but be very carefull--------------------------------------------------------------

declare @IssuedStiockTypeID varchar(10)
declare @ReceivedStiockTypeID varchar(10)
declare @StockTypeID varchar(10)

---Replace with LocationID  you are to reset to Zero
declare @LocationID varchar(10)
declare @ItemCategoryID varchar(10)
declare @ItemCode varchar(20)
declare @BatchUnitsAtHand int
declare @TranDate smalldatetime
declare @ClientMachine varchar(40)
declare @Notes varchar(200)
declare @BatchNo varchar(20)
declare @ExpiryDate smalldatetime

set @TranDate = dbo.GetShortDate(getdate())
set @ClientMachine = host_name()
set @IssuedStiockTypeID = dbo.GetLookupDataID('StockType', 'I')
set @ReceivedStiockTypeID = dbo.GetLookupDataID('StockType', 'R')

DECLARE InventoryLocationBatches_Cursor INSENSITIVE CURSOR FOR

select ItemCode, ItemCategoryID,BatchUnitsAtHand, LocationID, BatchNo, ExpiryDate from InventoryLocationBatches where  not BatchUnitsAtHand = 0
order by LocationID 
OPEN InventoryLocationBatches_Cursor
FETCH NEXT FROM InventoryLocationBatches_Cursor INTO @ItemCode, @ItemCategoryID, @BatchUnitsAtHand, @LocationID, @BatchNo, @ExpiryDate
WHILE (@@FETCH_STATUS <> -1)
	BEGIN

if @BatchUnitsAtHand < 0
	begin
	 set @StockTypeID =  @ReceivedStiockTypeID
	 set @BatchUnitsAtHand = abs(@BatchUnitsAtHand)
	end
else
	begin
	 set @StockTypeID =  @IssuedStiockTypeID
	end
set @Notes = 'Stock '+dbo.GetLookupDataDes(@StockTypeID)+' To Reset Stock at '+dbo.GetLookupDataDes(@LocationID)


exec uspInsertInventory @LocationID,@ItemCategoryID,@ItemCode, @TranDate, @StockTypeID, @BatchUnitsAtHand, @Notes, '46MAN','Admin', @ClientMachine, @BatchNo, @ExpiryDate

FETCH NEXT FROM InventoryLocationBatches_Cursor INTO @ItemCode, @ItemCategoryID,@BatchUnitsAtHand, @LocationID, @BatchNo, @ExpiryDate 
	END
CLOSE InventoryLocationBatches_Cursor
deallocate InventoryLocationBatches_Cursor

-----------------------------------------------------------------------------------------------------------------------------------------------------
---select * from Inventory  where Balance>0 should return 0 rows