use ClinicMaster
go
------------------------------------------------------------------------------------------------------
-------------- INTVisits --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Edit INTVisits -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspEditINTVisits')
	drop proc uspEditINTVisits
go

create proc uspEditINTVisits(
@AgentNo varchar(20),
@VisitNo varchar(20),
@MemberLimit money = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @BillMode varchar(100)
declare @BillNo varchar(20)
declare @InsuranceNo varchar(20)
declare @MemberCardNo varchar(30)
declare @MainMemberName varchar(41)
declare @ClaimReferenceNo varchar(30)
declare @VisitDate smalldatetime
declare @SyncStatus bit

-----------------------------------------------------------------------------------------------------------------------------------
set @BillMode = (select dbo.GetLookupDataDes(BillModesID) from Visits where VisitNo = @VisitNo)
set @BillNo = (select BillNo from Visits where VisitNo = @VisitNo)
set @InsuranceNo = (select InsuranceNo from Visits where VisitNo = @VisitNo)
set @MemberCardNo = (select MemberCardNo from Visits where VisitNo = @VisitNo)
set @MainMemberName = (select MainMemberName from Visits where VisitNo = @VisitNo)
set @ClaimReferenceNo = (select ClaimReferenceNo from Visits where VisitNo = @VisitNo)
set @VisitDate = (select VisitDate from Visits where VisitNo = @VisitNo)
set @SyncStatus = 1

if not exists(select AgentNo from INTAgents where AgentNo  = @AgentNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'INTAgents')
		return 1
	end

if not exists(select VisitNo from Visits where VisitNo  = @VisitNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Visits')
		return 1
	end

if exists(select AgentNo from INTVisits where AgentNo = @AgentNo and VisitNo = @VisitNo)
	begin
		update INTVisits set
        MemberLimit = @MemberLimit, BillMode = @BillMode, BillNo = @BillNo, InsuranceNo = @InsuranceNo, MemberCardNo = @MemberCardNo, MainMemberName = @MainMemberName, ClaimReferenceNo = @ClaimReferenceNo, VisitDate = @VisitDate, SyncStatus = @SyncStatus
       where AgentNo = @AgentNo and VisitNo = @VisitNo
	end

else
begin
insert into INTVisits
(AgentNo, VisitNo, MemberLimit, BillMode, BillNo, InsuranceNo, MemberCardNo, MainMemberName, ClaimReferenceNo, VisitDate, SyncStatus)
values
(@AgentNo, @VisitNo, @MemberLimit, @BillMode, @BillNo, @InsuranceNo, @MemberCardNo, @MainMemberName, @ClaimReferenceNo, @VisitDate, @SyncStatus)
return 0
end
go

/******************************************************************************************************
exec uspEditINTVisits 'SMART', 'P100182280001'
exec uspEditINTVisits 'SMART', 'P100182280001', 20000
******************************************************************************************************/
-- select * from Visits
-- select * from INTAgents
-- select * from INTVisits
-- delete from INTVisits

-------------- Get INTVisits -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetINTVisits')
	drop proc uspGetINTVisits
go

create proc uspGetINTVisits(
@AgentNo varchar(20),
@VisitNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select AgentNo from INTVisits where AgentNo = @AgentNo and VisitNo = @VisitNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'Visit No', @VisitNo, 'INTVisits')
		return 1
	end
else
begin
	select AgentNo, VisitNo,  isnull(MemberLimit, 0) as MemberLimit, BillMode, BillNo, InsuranceNo, MemberCardNo, MainMemberName, ClaimReferenceNo, VisitDate, SyncStatus, UserID, ClientMachine, RecordDateTime
	from INTVisits

	where AgentNo = @AgentNo and VisitNo = @VisitNo
return 0
end
go

/******************************************************************************************************
exec uspGetINTVisits 'SMART', 'P160004550001'
******************************************************************************************************/
-- select * from INTVisits
-- delete from INTVisits

------------------------------------------------------------------------------------------------------
-------------- INTAdmissions --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Edit INTAdmissions -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspEditINTAdmissions')
	drop proc uspEditINTAdmissions
go

create proc uspEditINTAdmissions(
@AgentNo varchar(20),
@AdmissionNo varchar(20),
@MemberLimit money = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @BillMode varchar(100)
declare @BillNo varchar(20)
declare @InsuranceNo varchar(20)
declare @MemberCardNo varchar(30)
declare @MainMemberName varchar(41)
declare @ClaimReferenceNo varchar(30)
declare @AdmissionDateTime smalldatetime
declare @SyncStatus bit
-----------------------------------------------------------------------------------------------------------------------------------
set @BillMode = (select dbo.GetLookupDataDes(BillModesID) from Admissions where AdmissionNo = @AdmissionNo)
set @BillNo = (select BillNo from Admissions where AdmissionNo = @AdmissionNo)
set @InsuranceNo = (select InsuranceNo from Admissions where AdmissionNo = @AdmissionNo)
set @MemberCardNo = (select MemberCardNo from Admissions where AdmissionNo = @AdmissionNo)
set @MainMemberName = (select MainMemberName from Admissions where AdmissionNo = @AdmissionNo)
set @ClaimReferenceNo = (select ClaimReferenceNo from Admissions where AdmissionNo = @AdmissionNo)
set @AdmissionDateTime = (select AdmissionDateTime from Admissions where AdmissionNo = @AdmissionNo)
set @SyncStatus = 1

if not exists(select AgentNo from INTAgents where AgentNo  = @AgentNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'INTAgents')
		return 1
	end

if not exists(select AdmissionNo from Admissions where AdmissionNo  = @AdmissionNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Admission No', @AdmissionNo, 'Admissions')
		return 1
	end

if exists(select AgentNo from INTAdmissions where AgentNo = @AgentNo and AdmissionNo = @AdmissionNo)
	begin
		update INTAdmissions set
        MemberLimit =  @MemberLimit, AdmissionDateTime = @AdmissionDateTime, BillMode = @BillMode, BillNo = @BillNo, InsuranceNo = @InsuranceNo, MemberCardNo = @MemberCardNo, MainMemberName = @MainMemberName, ClaimReferenceNo = @ClaimReferenceNo, SyncStatus = @SyncStatus
        where AgentNo = @AgentNo and AdmissionNo = @AdmissionNo
	return 0
	end

else
begin
	insert into INTAdmissions
	(AgentNo, AdmissionNo, MemberLimit, AdmissionDateTime, BillMode, BillNo, InsuranceNo, MemberCardNo, MainMemberName, ClaimReferenceNo, SyncStatus)
	values
	(@AgentNo, @AdmissionNo, @MemberLimit, @AdmissionDateTime, @BillMode, @BillNo, @InsuranceNo, @MemberCardNo, @MainMemberName, @ClaimReferenceNo, @SyncStatus)
return 0
end
go

/******************************************************************************************************
exec uspEditINTAdmissions 'SMART', 'H1707001'
exec uspEditINTAdmissions 'SMART', 'H1707001', 3000

******************************************************************************************************/
-- select * from Admissions
-- select * from INTAdmissions
-- delete from INTAdmissions


-------------- Get INTAdmissions -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetINTAdmissions')
	drop proc uspGetINTAdmissions
go

create proc uspGetINTAdmissions(
@AgentNo varchar(20),
@AdmissionNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select AgentNo from INTAdmissions where AgentNo = @AgentNo and AdmissionNo = @AdmissionNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'Admission No', @AdmissionNo, 'INTAdmissions')
		return 1
	end
else
begin
	select AgentNo, AdmissionNo, isnull(MemberLimit, 0) as MemberLimit, AdmissionDateTime, BillMode, BillNo, InsuranceNo, MemberCardNo, MainMemberName, ClaimReferenceNo, SyncStatus, UserID, ClientMachine, RecordDateTime
	from INTAdmissions

	where AgentNo = @AgentNo and AdmissionNo = @AdmissionNo
return 0
end
go

/******************************************************************************************************
exec uspGetINTAdmissions 'SMART', 'H1707001'

******************************************************************************************************/
-- select * from INTAdmissions
-- delete from INTAdmissions



------------------------------------------------------------------------------------------------------
-------------- INTItems --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Edit INTItems -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspEditINTItems')
	drop proc uspEditINTItems
go

create proc uspEditINTItems(
@AgentNo varchar(20),
@VisitNo varchar(20),
@ItemCode varchar(20),
@ItemCategoryID varchar(10),
@SyncStatus bit = 1,
@ReferenceNo varchar(100) = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @ItemCategory varchar(100)
declare @ItemName varchar(800)
declare @OriginalQuantity int
declare @OriginalPrice money
declare @InvoiceNo varchar(20)

-------------------------------------------------------------------------------------------------------------------------------------------
set @ItemCategory = (select DataDes from LookupData where DataID =  @ItemCategoryID)
set @ItemName = (select ItemName from Items where VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @OriginalQuantity = (select OriginalQuantity from Items where VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @OriginalPrice = (select OriginalPrice from Items where VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @InvoiceNo = (select InvoiceNo from Items where VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)

if not exists(select AgentNo from INTAgents where AgentNo  = @AgentNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'INTAgents')
		return 1
	end

if not exists(select VisitNo from Items where VisitNo  = @VisitNo and ItemCode  = @ItemCode and ItemCategoryID  = @ItemCategoryID)
	begin
		set @ErrorMSG = 'The %s: %s and %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo,  'Item Code', @ItemCode, 'Item Category ID', @ItemCategoryID, 'Items')
		return 1
	end


if exists(select AgentNo from INTItems where AgentNo = @AgentNo and VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
	begin
		update INTItems set
		ItemCategory = @ItemCategory, ItemName = @ItemName, OriginalQuantity = @OriginalQuantity, OriginalPrice = @OriginalPrice,
		 InvoiceNo = @InvoiceNo, ReferenceNo = @ReferenceNo, SyncStatus = @SyncStatus
		where AgentNo = @AgentNo and VisitNo = @VisitNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID
return 0
	end

else
begin
insert into INTItems
(AgentNo, VisitNo, ItemCode, ItemCategoryID, ItemCategory, ItemName, OriginalQuantity, OriginalPrice, InvoiceNo, ReferenceNo, SyncStatus)
values
(@AgentNo, @VisitNo, @ItemCode, @ItemCategoryID, @ItemCategory, @ItemName, @OriginalQuantity, @OriginalPrice, @InvoiceNo, @ReferenceNo, @SyncStatus)
return 0
end
go

/******************************************************************************************************
exec uspEditINTItems 'SMART', 'P100182280001', '10C11', '7S'
******************************************************************************************************/
-- select * from Items
-- select * from INTItems
-- delete from INTItems

-------------- Get Not Synced Items -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetNotSyncedItems')
	drop proc uspGetNotSyncedItems
go

create proc uspGetNotSyncedItems(
@AgentNo varchar(20),
@VisitNo varchar(20) = null
)with encryption as

declare @NotPaidStatusID varchar(10)
declare @OfferedItemStatusID varchar(10)
declare @ProcessingItemStatusID varchar(10)
declare @DoneItemStatusID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)

-------------------------------------------------------------------------------------------------------------------------------------------
set @OfferedItemStatusID = dbo.GetLookupDataID('ItemStatus','O')
set @DoneItemStatusID = dbo.GetLookupDataID('ItemStatus','D')
set @ProcessingItemStatusID = dbo.GetLookupDataID('ItemStatus','R')
set @NotPaidStatusID = dbo.GetLookupDataID('PayStatus','NP')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'
-------------------------------------------------------------------------------------------------------------------------------------------
begin
if @VisitNo is not null
begin
   select 1 as AutoInclude, Items.VisitNo, Visits.BillNo, Items.ItemCode, Items.ItemName, Items.InvoiceNo, 		
		Quantity, UnitPrice, ((Quantity * UnitPrice)- isnull(CashAmount,0)) as Amount, Quantity * UnitPrice as TotalAmount, Items.OriginalQuantity, Items.OriginalPrice, 
		Items.OriginalQuantity * Items.OriginalPrice as OriginalAmount, ItemDetails, Items.ItemCategoryID, dbo.GetLookupDataDes(Items.ItemCategoryID) as ItemCategory,
		PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, 
		ItemStatusID, dbo.GetLookupDataDes(ItemStatusID) as ItemStatus, INTItems.SyncStatus
		from Items
		inner join Visits on Visits.VisitNo =  Items.VisitNo
		inner join INTVisits on INTVisits.VisitNo = Items.VisitNo
		left outer join ItemsCASH on ItemsCASH.VisitNo = Items.VisitNo and ItemsCASH.ItemCode = Items.ItemCode 
		and ItemsCASH.ItemCategoryID = Items.ItemCategoryID
		left outer join INTItems on INTItems.VisitNo = Items.VisitNo and INTItems.ItemCode = Items.ItemCode 
		and INTItems.ItemCategoryID = Items.ItemCategoryID 
		and INTItems.AgentNo = @AgentNo
		where 
		Items.VisitNo = @VisitNo and ItemStatusID in (@OfferedItemStatusID, @DoneItemStatusID, @ProcessingItemStatusID) 
		and PayStatusID = @NotPaidStatusID and (INTItems.SyncStatus is null or INTItems.SyncStatus = 0)
		and not (Items.ItemCategoryID = @ExtraChargeItemCategoryID and Items.ItemCode = @CoPayItemCode)
	end
else
begin
   select 1 as AutoInclude, Items.VisitNo, Visits.BillNo, Items.ItemCode, Items.ItemName, Items.InvoiceNo, 		
		Quantity, UnitPrice, ((Quantity * UnitPrice)- isnull(CashAmount,0)) as Amount, Quantity * UnitPrice as TotalAmount, Items.OriginalQuantity, Items.OriginalPrice, 
		Items.OriginalQuantity * Items.OriginalPrice as OriginalAmount, ItemDetails, Items.ItemCategoryID, dbo.GetLookupDataDes(Items.ItemCategoryID) as ItemCategory,
		PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, 
		ItemStatusID, dbo.GetLookupDataDes(ItemStatusID) as ItemStatus, INTItems.SyncStatus
		from Items
		inner join Visits on Visits.VisitNo =  Items.VisitNo
		inner join INTVisits on INTVisits.VisitNo = Items.VisitNo
		left outer join ItemsCASH on ItemsCASH.VisitNo = Items.VisitNo and ItemsCASH.ItemCode = Items.ItemCode and ItemsCASH.ItemCategoryID = Items.ItemCategoryID
		left outer join INTItems on INTItems.VisitNo = Items.VisitNo and INTItems.ItemCode = Items.ItemCode and INTItems.ItemCategoryID = Items.ItemCategoryID 
		and INTItems.AgentNo = @AgentNo
		where ItemStatusID in (@OfferedItemStatusID, @DoneItemStatusID, @ProcessingItemStatusID)
		and PayStatusID = @NotPaidStatusID and (INTItems.SyncStatus is null or INTItems.SyncStatus = 0)
		and not (Items.ItemCategoryID = @ExtraChargeItemCategoryID and Items.ItemCode = @CoPayItemCode)

	end
return 0
end
go

/******************************************************************************************************
exec uspGetNotSyncedItems 'SMART'
exec uspGetNotSyncedItems 'SMART', '2100748001'
******************************************************************************************************/
-- select * from Items
-- select * from INTItems
-- delete from INTItems

-------------- Get Not Synced Items -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetCountNotSyncedItems')
	drop proc uspGetCountNotSyncedItems
go

create proc uspGetCountNotSyncedItems(
@AgentNo varchar(20),
@Records int = null output
)with encryption as

declare @NotPaidStatusID varchar(10)
declare @OfferedItemStatusID varchar(10)
declare @DoneItemStatusID varchar(10)
declare @ProcessingItemStatusID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)

-------------------------------------------------------------------------------------------------------------------------------------------
set @OfferedItemStatusID = dbo.GetLookupDataID('ItemStatus','O')
set @DoneItemStatusID = dbo.GetLookupDataID('ItemStatus','D')
set @ProcessingItemStatusID = dbo.GetLookupDataID('ItemStatus','R')
set @NotPaidStatusID = dbo.GetLookupDataID('PayStatus','NP')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'
-------------------------------------------------------------------------------------------------------------------------------------------

begin
  set @Records = (select isnull(count(Items.VisitNo), 0)
		from Items
		inner join Visits on Visits.VisitNo =  Items.VisitNo
		inner join INTVisits on INTVisits.VisitNo = Items.VisitNo
		left outer join INTItems on INTItems.VisitNo = Items.VisitNo and INTItems.ItemCode = Items.ItemCode and INTItems.ItemCategoryID = Items.ItemCategoryID 
		and INTItems.AgentNo = @AgentNo
		where ItemStatusID in (@OfferedItemStatusID, @DoneItemStatusID, @ProcessingItemStatusID) 
		and PayStatusID = @NotPaidStatusID and (INTItems.SyncStatus is null or INTItems.SyncStatus = 0)
		and not (Items.ItemCategoryID = @ExtraChargeItemCategoryID and Items.ItemCode = @CoPayItemCode))

return 0
end
go

/******************************************************************************************************
exec uspGetCountNotSyncedItems 'SMART'
******************************************************************************************************/
-- select * from Items
-- select * from INTItems
-- delete from INTItems
------------------------------------------------------------------------------------------------------
-------------- INTExtraBillItems --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Edit INTExtraBillItems -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspEditINTExtraBillItems')
	drop proc uspEditINTExtraBillItems
go

create proc uspEditINTExtraBillItems(
@AgentNo varchar(20),
@ExtraBillNo varchar(20),
@ItemCode varchar(20),
@ItemCategoryID varchar(10),
@SyncStatus bit = 1,
@ReferenceNo varchar(100) = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @ItemName varchar(800)
declare @BillMode varchar(100)
declare @BillNo varchar(20)
declare @BillToCustomerNo varchar(20)
declare @BillToCustomerName varchar(41)
declare @OriginalQuantity int
declare @OriginalPrice money
declare @ExtraBillRecordDateTime smalldatetime

--------------------------------------------------------------------------------------------------------------------
set @ItemName = (select ItemName from ExtraBillItems where ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @OriginalQuantity = (select OriginalQuantity from ExtraBillItems where ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @OriginalPrice = (select OriginalPrice from ExtraBillItems where ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @BillMode = (select dbo.GetLookupDataDes(BillModesID) from ExtraBills
                  inner join ExtraBillItems on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
                  where ExtraBillItems.ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @BillNo = (select BillNo from  ExtraBills inner join ExtraBillItems on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
                  where ExtraBillItems.ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
set @BillToCustomerNo = (select dbo.GetBillToCustomerNoByExtraBillNo(ExtraBillNo) from ExtraBills where ExtraBillNo = @ExtraBillNo)
set @BillToCustomerNo = REPLACE(@BillToCustomerNo, '-', '')
set @BillToCustomerName = (select dbo.GetBillToCustomerNameByExtraBillNo(ExtraBillNo) from ExtraBills where ExtraBillNo = @ExtraBillNo)
set @ExtraBillRecordDateTime = (select RecordDateTime from ExtraBillItems where ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)

if not exists(select AgentNo from INTAgents where AgentNo  = @AgentNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'INTAgents')
		return 1
	end

if not exists(select ExtraBillNo from ExtraBillItems where ExtraBillNo  = @ExtraBillNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'ExtraBillNo', @ExtraBillNo, 'ExtraBillItems')
		return 1
	end

if not exists(select ItemCode from ExtraBillItems where ItemCode  = @ItemCode)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'ItemCode', @ItemCode, 'ExtraBillItems')
		return 1
	end

if not exists(select ItemCategoryID from ExtraBillItems where ItemCategoryID  = @ItemCategoryID)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'ItemCategoryID', @ItemCategoryID, 'ExtraBillItems')
		return 1
	end

if exists(select AgentNo from INTExtraBillItems 
  where AgentNo = @AgentNo and ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID)
	begin
		update INTExtraBillItems set
		ItemName = @ItemName, BillMode = @BillMode, BillNo = @BillNo, BillToCustomerNo = @BillToCustomerNo, 
		BillToCustomerName = @BillToCustomerName, OriginalQuantity = @OriginalQuantity, OriginalPrice = @OriginalPrice,
		 ExtraBillRecordDateTime = @ExtraBillRecordDateTime, ReferenceNo = @ReferenceNo, SyncStatus = @SyncStatus
		where AgentNo = @AgentNo and ExtraBillNo = @ExtraBillNo and ItemCode = @ItemCode and ItemCategoryID = @ItemCategoryID
		return 0
	end

else
 begin
	insert into INTExtraBillItems
	(AgentNo, ExtraBillNo, ItemCode, ItemCategoryID, ItemName, BillMode, BillNo, BillToCustomerNo, BillToCustomerName, 
	OriginalQuantity, OriginalPrice, ExtraBillRecordDateTime, ReferenceNo, SyncStatus)
	values
	(@AgentNo, @ExtraBillNo, @ItemCode, @ItemCategoryID, @ItemName, @BillMode, @BillNo, @BillToCustomerNo, @BillToCustomerName,
	 @OriginalQuantity, @OriginalPrice, @ExtraBillRecordDateTime, @ReferenceNo, @SyncStatus)
	return 0
end
go

/******************************************************************************************************
exec uspEditINTExtraBillItems 'SMART', 'P160000200001', 'LAB010', '7T'
exec uspEditINTExtraBillItems 'SMART', 'P160000200001', 'LAB010', '7T', null, 0
******************************************************************************************************/
-- select * from ExtraBillItems
-- select * from INTExtraBillItems

-- delete from INTExtraBillItems



------------ Get Not Synced ExtraBillItems -----------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetNotSyncedExtraBillItems')
drop proc uspGetNotSyncedExtraBillItems
go

create proc uspGetNotSyncedExtraBillItems(
@AgentNo Varchar(20),
@VisitTypeID varchar(10),
@VisitNo Varchar(20) = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @NotPaidStatusID varchar(10)
declare @OPDVisitTypeID varchar(10)
declare @IPDVisitTypeID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)

---------------------------------------------------------------------------------------------------------------------------------------------------
set @NotPaidStatusID =  dbo.GetLookupDataID('PayStatus', 'NP')
set @OPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'OPD')
set @IPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'IPD')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'

if not exists(select AgentNo from INTAgents where AgentNo = @AgentNo)
begin
set @ErrorMSG = 'The Agent with %s: %s, you are trying to enter does not exist in the registered %s'
raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'Agents')
return 1
end
begin
 if @VisitNo is not null
 begin
	 select ExtraBillItems.ExtraBillNo, ExtraBills.VisitNo, ExtraBills.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, UnitPrice, Quantity, 
	 ExtraBillItems.Amount, (UnitPrice*Quantity) as TotalAmount,
	 Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes,
	 1 as AutoInclude, ExtraBillItems.LoginID, INTExtraBillItems.SyncStatus
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  ExtraBills.VisitNo = @VisitNo and (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus= 0) and PayStatusID = @NotPaidStatusID
	 and ExtraBills.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and VisitTypeID = @VisitTypeID and VisitTypeID = @OPDVisitTypeID
	 union
	  select ExtraBillItems.ExtraBillNo, ExtraBills.VisitNo, ExtraBills.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, UnitPrice, Quantity, 
	 ExtraBillItems.Amount, (UnitPrice*Quantity) as TotalAmount,
	 Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes,
	 1 as AutoInclude, ExtraBillItems.LoginID, INTExtraBillItems.SyncStatus
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  ExtraBills.VisitNo = @VisitNo and (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus= 0) and PayStatusID = @NotPaidStatusID
	 and ExtraBills.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and VisitTypeID = @VisitTypeID and VisitTypeID = @IPDVisitTypeID

end
else
 begin
	  select ExtraBillItems.ExtraBillNo, ExtraBills.VisitNo, ExtraBills.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, UnitPrice, Quantity, 
	 ExtraBillItems.Amount, (UnitPrice*Quantity) as TotalAmount,
	 Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes,
	 1 as AutoInclude, ExtraBillItems.LoginID, INTExtraBillItems.SyncStatus
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus= 0) and PayStatusID = @NotPaidStatusID
	 and ExtraBills.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and VisitTypeID = @VisitTypeID and VisitTypeID = @OPDVisitTypeID
	 union
	  select ExtraBillItems.ExtraBillNo, ExtraBills.VisitNo, ExtraBills.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, UnitPrice, Quantity, 
	 ExtraBillItems.Amount, (UnitPrice*Quantity) as TotalAmount,
	 Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes,
	 1 as AutoInclude, ExtraBillItems.LoginID, INTExtraBillItems.SyncStatus
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus= 0) and PayStatusID = @NotPaidStatusID
	 and ExtraBills.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and VisitTypeID = @VisitTypeID and VisitTypeID = @IPDVisitTypeID
end
return 0
end
go

/****************************************************************************************
exec uspGetNotSyncedExtraBillItems 'SMART','110OPD', '0700041002'
exec uspGetNotSyncedExtraBillItems 'SMART', '110OPD'
exec uspGetNotSyncedExtraBillItems 'SMART','110IPD', '2100748001'
exec uspGetNotSyncedExtraBillItems 'SMART', '110IPD'

 select * from INTExtraBillItems
  select * from ExtraBillItemsCASH
select * from ExtraBills where ExtraBillNo ='P160001850002'
****************************************************************************************/

--- select * from Admissions where VisitNo = 'P1800355870001'

if exists (select * from sysobjects where name = 'uspGetCountNotSyncedExtraBillItems')
drop proc uspGetCountNotSyncedExtraBillItems
go

create proc uspGetCountNotSyncedExtraBillItems(
@AgentNo Varchar(20),
@VisitTypeID varchar(10),
@VisitNo varchar(20) = null,
@Records int = null output
)with encryption as

declare @ErrorMSG varchar(200)
declare @OPDCount int
declare @IPDCount int
declare @NotPaidStatusID varchar(10)
declare @OPDVisitTypeID varchar(10)
declare @IPDVisitTypeID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)
---------------------------------------------------------------------------------------------------------------------------------------------------
set @NotPaidStatusID =  dbo.GetLookupDataID('PayStatus', 'NP')
set @OPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'OPD')
set @IPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'IPD')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'

if not exists(select AgentNo from INTAgents where AgentNo = @AgentNo)
begin
set @ErrorMSG = 'The Agent with %s: %s, you are trying to enter does not exist in the registered %s'
raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'Agents')
return 1
end

if @VisitNo is null
begin
	set @Records = (select isnull(count(ExtraBillItems.ExtraBillNo), 0)
	from  ExtraBillItems
	inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
	inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	and INTExtraBillItems.AgentNo = @AgentNo
	WHERE (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus = 0) and PayStatusID = @NotPaidStatusID
	and VisitTypeID = @VisitTypeID and ExtraBills.SmartCardApplicable = 1
	and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode) and VisitTypeID = @IPDVisitTypeID)
	+
	(select isnull(count(ExtraBillItems.ExtraBillNo), 0)
	from  ExtraBillItems
	inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	and INTExtraBillItems.AgentNo = @AgentNo
	WHERE (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus = 0) and PayStatusID = @NotPaidStatusID
	and VisitTypeID = @VisitTypeID and ExtraBills.SmartCardApplicable = 1
	and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode) and VisitTypeID = @OPDVisitTypeID)
	return 0
end
else
begin
	set @Records = (select isnull(count(ExtraBillItems.ExtraBillNo), 0)
	from  ExtraBillItems
	inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
	inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	and INTExtraBillItems.AgentNo = @AgentNo
	WHERE (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus = 0) and PayStatusID = @NotPaidStatusID
	and VisitTypeID = @VisitTypeID and ExtraBills.SmartCardApplicable = 1
	and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode) and VisitTypeID = @IPDVisitTypeID
	and ExtraBills.VisitNo = @VisitNo)
	+
	(select isnull(count(ExtraBillItems.ExtraBillNo), 0)
	from  ExtraBillItems
	inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	and INTExtraBillItems.AgentNo = @AgentNo
	WHERE (INTExtraBillItems.SyncStatus is null or INTExtraBillItems.SyncStatus = 0) and PayStatusID = @NotPaidStatusID
	and VisitTypeID = @VisitTypeID and ExtraBills.SmartCardApplicable = 1
	and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode) and VisitTypeID = @OPDVisitTypeID
	and ExtraBills.VisitNo = @VisitNo)
	return 0
end
go

/****************************************************************************************
exec uspGetCountNotSyncedExtraBillItems 'SMART', '110OPD'
exec uspGetCountNotSyncedExtraBillItems 'SMART', '110IPD'

 select * from INTExtraBillItems
  select * from ExtraBillItemsCASH
select * from ExtraBills where ExtraBillNo ='P160001850002'
****************************************************************************************/

-------------- Get Not Synced Items -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetINTSmartItems')
	drop proc uspGetINTSmartItems
go

create proc uspGetINTSmartItems(
@StartDateTime smalldatetime,
@EndDateTime smalldatetime,
@BillNo varchar(20) = null
)with encryption as

declare @NotPaidStatusID varchar(10)
declare @OfferedItemStatusID varchar(10)
declare @ProcessingItemStatusID varchar(10)
declare @DoneItemStatusID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)
declare @AgentNo varchar(20)

-------------------------------------------------------------------------------------------------------------------------------------------
set @OfferedItemStatusID = dbo.GetLookupDataID('ItemStatus','O')
set @DoneItemStatusID = dbo.GetLookupDataID('ItemStatus','D')
set @ProcessingItemStatusID = dbo.GetLookupDataID('ItemStatus','R')
set @NotPaidStatusID = dbo.GetLookupDataID('PayStatus','NP')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'
set @AgentNo = 'Smart'
-------------------------------------------------------------------------------------------------------------------------------------------

if @BillNo is null
 begin
    select  Items.VisitNo, Visits.BillNo, Items.ItemCode, Items.ItemName, 
	Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, Visits.BillModesID, dbo.GetLookupDataDes(Visits.BillModesID) as BillModes, 
	INTVisits.InsuranceNo,  dbo.GetBillName(Visits.BillModesID, INTVisits.BillNo) as BillCustomerName, Visits.ClaimReferenceNo, INTVisits.MemberCardNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, 
	Quantity, UnitPrice, ((isnull(INTItems.OriginalQuantity, Quantity) * isnull(INTItems.OriginalPrice, UnitPrice))- isnull(CashAmount,0)) as Amount, 
	isnull(INTItems.OriginalQuantity,Quantity) * isnull (INTItems.OriginalPrice,UnitPrice) as TotalAmount, Items.OriginalQuantity, Items.OriginalPrice, 
	Items.OriginalQuantity * Items.OriginalPrice as OriginalAmount, ItemDetails, Items.ItemCategoryID, dbo.GetLookupDataDes(Items.ItemCategoryID) as ItemCategory,
	PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, 
	ItemStatusID, dbo.GetLookupDataDes(ItemStatusID) as ItemStatus, isnull(INTItems.SyncStatus, 0) as SyncStatus, INTItems.ReferenceNo
	from Items
	inner join Visits on Visits.VisitNo =  Items.VisitNo
	inner join INTVisits on INTVisits.VisitNo = Items.VisitNo
	inner join Patients on Patients.PatientNo = Visits.PatientNo
	left outer join ItemsCASH on ItemsCASH.VisitNo = Items.VisitNo and ItemsCASH.ItemCode = Items.ItemCode and ItemsCASH.ItemCategoryID = Items.ItemCategoryID
	left outer join INTItems on INTItems.VisitNo = Items.VisitNo and INTItems.ItemCode = Items.ItemCode and INTItems.ItemCategoryID = Items.ItemCategoryID 
	and INTItems.AgentNo = @AgentNo
	where ItemStatusID in (@OfferedItemStatusID, @DoneItemStatusID, @ProcessingItemStatusID)
	and PayStatusID = @NotPaidStatusID 
	and not (Items.ItemCategoryID = @ExtraChargeItemCategoryID and Items.ItemCode = @CoPayItemCode)
	and Items.RecordDateTime between @StartDateTime and @EndDateTime
   return 0
end
else
begin
    select  Items.VisitNo, Visits.BillNo, Items.ItemCode, Items.ItemName, 
	Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, Visits.BillModesID, dbo.GetLookupDataDes(Visits.BillModesID) as BillModes, 
	INTVisits.InsuranceNo,  dbo.GetBillName(Visits.BillModesID, INTVisits.BillNo) as BillCustomerName, Visits.ClaimReferenceNo, INTVisits.MemberCardNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, 
	Quantity, UnitPrice, ((isnull(INTItems.OriginalQuantity, Quantity) * isnull(INTItems.OriginalPrice, UnitPrice))- isnull(CashAmount,0)) as Amount, 
	isnull(INTItems.OriginalQuantity,Quantity) * isnull (INTItems.OriginalPrice,UnitPrice) as TotalAmount, Items.OriginalQuantity, Items.OriginalPrice, 
	Items.OriginalQuantity * Items.OriginalPrice as OriginalAmount, ItemDetails, Items.ItemCategoryID, dbo.GetLookupDataDes(Items.ItemCategoryID) as ItemCategory,
	PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, 
	ItemStatusID, dbo.GetLookupDataDes(ItemStatusID) as ItemStatus, isnull(INTItems.SyncStatus, 0) as SyncStatus, INTItems.ReferenceNo
	from Items
	inner join Visits on Visits.VisitNo =  Items.VisitNo
	inner join INTVisits on INTVisits.VisitNo = Items.VisitNo
	inner join Patients on Patients.PatientNo = Visits.PatientNo
	left outer join ItemsCASH on ItemsCASH.VisitNo = Items.VisitNo and ItemsCASH.ItemCode = Items.ItemCode and ItemsCASH.ItemCategoryID = Items.ItemCategoryID
	left outer join INTItems on INTItems.VisitNo = Items.VisitNo and INTItems.ItemCode = Items.ItemCode and INTItems.ItemCategoryID = Items.ItemCategoryID 
	and INTItems.AgentNo = @AgentNo
	where ItemStatusID in (@OfferedItemStatusID, @DoneItemStatusID, @ProcessingItemStatusID)
	and PayStatusID = @NotPaidStatusID 
	and not (Items.ItemCategoryID = @ExtraChargeItemCategoryID and Items.ItemCode = @CoPayItemCode)
	and Items.RecordDateTime between @StartDateTime and @EndDateTime and INTVisits.BillNo = @BillNo
   return 0
end
go

/******************************************************************************************************
exec uspGetINTSmartItems '01 Apr 2021', '04 Jun 2021'
exec uspGetINTSmartItems '01 Apr 2021', '04 Jun 2021', '4GTL'
exec uspGetINTSmartItems '01 Apr 2021', '04 Jun 2021', 'A21005'

******************************************************************************************************/



------------ Get INT Samrt ExtraBillItems -----------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetINTSmartExtraBillItems')
drop proc uspGetINTSmartExtraBillItems
go

create proc uspGetINTSmartExtraBillItems(
@StartDateTime smalldatetime,
@EndDateTime smalldatetime,
@VisitTypeID varchar(10) = null,
@BillNo varchar(20) = null
)with encryption as

declare @ErrorMSG varchar(200)
declare @NotPaidStatusID varchar(10)
declare @OPDVisitTypeID varchar(10)
declare @IPDVisitTypeID varchar(10)
declare @ExtraChargeItemCategoryID varchar(10)
declare @CoPayItemCode varchar(20)
declare @AgentNo varchar(20)
---------------------------------------------------------------------------------------------------------------------------------------------------
set @NotPaidStatusID =  dbo.GetLookupDataID('PayStatus', 'NP')
set @OPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'OPD')
set @IPDVisitTypeID =   dbo.GetLookupDataID('VisitType', 'IPD')
set @ExtraChargeItemCategoryID = dbo.GetLookupDataID('ItemCategory', 'E')
set @CoPayItemCode = 'CPV'
set @AgentNo = 'SMART'

if not exists(select AgentNo from INTAgents where AgentNo = @AgentNo)
begin
set @ErrorMSG = 'The Agent with %s: %s, you are trying to enter does not exist in the registered %s'
raiserror(@ErrorMSG, 16, 1, 'Agent No', @AgentNo, 'Agents')
return 1
end

if @VisitTypeID is null and @BillNo is null
 begin
	select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, 
	dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, 
	ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, 
	ExtraBillItems.ItemCategoryID,  dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory,
	ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 inner join Visits on Visits.VisitNo = INTVisits.VisitNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @OPDVisitTypeID
	 union
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Visits on Visits.VisitNo = ExtraBills.VisitNo
	 inner join Admissions on Admissions.VisitNo =  Visits.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @IPDVisitTypeID
	 order by ExtraBillItems.RecordDateTime desc

  
return 0
end
else if @VisitTypeID is not null and @BillNo is null
 begin
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 inner join Visits on Visits.VisitNo = INTVisits.VisitNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @OPDVisitTypeID and VisitTypeID = @VisitTypeID
	 union
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Visits on Visits.VisitNo = ExtraBills.VisitNo
	 inner join Admissions on Admissions.VisitNo =  Visits.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @IPDVisitTypeID and VisitTypeID = @VisitTypeID
	 order by ExtraBillItems.RecordDateTime desc
  
return 0
end

else if @VisitTypeID is null and @BillNo is not null
 begin
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 inner join Visits on Visits.VisitNo = INTVisits.VisitNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @OPDVisitTypeID 
	 and Visits.BillNo = @BillNo
	 union
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Visits on Visits.VisitNo = ExtraBills.VisitNo
	 inner join Admissions on Admissions.VisitNo =  Visits.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @IPDVisitTypeID
	 and ExtraBills.BillNo = @BillNo
	 order by ExtraBillItems.RecordDateTime desc
  
return 0
end

else if @VisitTypeID is not null and @BillNo is not null
 begin
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join INTVisits on INTVisits.VisitNo = ExtraBills.VisitNo
	 inner join Visits on Visits.VisitNo = INTVisits.VisitNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @OPDVisitTypeID 
	 and VisitTypeID = @VisitTypeID and Visits.BillNo = @BillNo
	 union
	 select Visits.VisitDate, Patients.PatientNo, dbo.GetHidePatientDetails(FirstName,HideDetails) as FirstName, dbo.GetHidePatientDetails(LastName,HideDetails) as LastName, 
	dbo.GetHidePatientDetails(MiddleName,HideDetails) as MiddleName, ExtraBills.BillModesID, dbo.GetLookupDataDes(ExtraBills.BillModesID) as BillModes, 
	ExtraBills.ExtraBillNo, ExtraBills.InsuranceNo,  dbo.GetBillName(ExtraBills.BillModesID, ExtraBills.BillNo) as BillCustomerName,
	ExtraBills.MemberCardNo, ExtraBills.ClaimReferenceNo,
	dbo.GetHidePatientDetails(dbo.GetFullName(LastName, MiddleName, FirstName),HideDetails) as FullName, ExtraBillItems.ExtraBillNo, 
	ExtraBills.VisitNo, ExtraBills.VisitTypeID, dbo.GetLookupDataDes( ExtraBills.VisitTypeID) as VisitType,   ExtraBillItems.RecordDateTime, ExtraBillItems.ItemCategoryID,
	 dbo.GetLookupDataDes(ExtraBillItems.ItemCategoryID) as ItemCategory ,ExtraBillItems.ItemCode, ExtraBillItems.ItemName, 
	 isnull(INTExtraBillItems.OriginalQuantity, Quantity) as Quantity, 
	 isnull(INTExtraBillItems.OriginalPrice, UnitPrice) as UnitPrice, 
	 (isnull(INTExtraBillItems.OriginalPrice, UnitPrice) * isnull(INTExtraBillItems.OriginalPrice, UnitPrice)) as Amount, Notes, PayStatusID, dbo.GetLookupDataDes(PayStatusID) as PayStatus, EntryModeID, 
	 dbo.GetLookupDataDes(EntryModeID) as EntryMode, Notes, ExtraBillItems.LoginID, isnull(INTExtraBillItems.SyncStatus, 0) as SyncStatus, INTExtraBillItems.ReferenceNo
	 from  ExtraBillItems
	 inner join ExtraBills on ExtraBillItems.ExtraBillNo = ExtraBills.ExtraBillNo
	 inner join Visits on Visits.VisitNo = ExtraBills.VisitNo
	 inner join Admissions on Admissions.VisitNo =  Visits.VisitNo
	 inner join INTAdmissions on INTAdmissions.AdmissionNo = Admissions.AdmissionNo
	 inner join Patients on Patients.PatientNo = Visits.PatientNo
	 left outer join INTExtraBillItems on INTExtraBillItems.ExtraBillNo = ExtraBillItems.ExtraBillNo 
	 and INTExtraBillItems.ItemCode = ExtraBillItems.ItemCode and INTExtraBillItems.ItemCategoryID = ExtraBillItems.ItemCategoryID
	 and INTExtraBillItems.AgentNo = @AgentNo
	 WHERE  PayStatusID = @NotPaidStatusID and Visits.SmartCardApplicable = 1
	 and not (ExtraBillItems.ItemCategoryID = @ExtraChargeItemCategoryID and ExtraBillItems.ItemCode = @CoPayItemCode)
	 and ExtraBillItems.RecordDateTime between @StartDateTime and @EndDateTime and VisitTypeID = @IPDVisitTypeID and VisitTypeID = @VisitTypeID
	 and ExtraBills.BillNo = @BillNo
	 order by ExtraBillItems.RecordDateTime desc
  
return 0
end
go

/****************************************************************************************
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021'
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021', '110OPD'
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021', '110IPD'
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021', '110IPD', 'A21005'
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021', '110OPD', 'A21005'
exec uspGetINTSmartExtraBillItems  '01 Apr 2021', '08 Jun 2021', null, 'A21005'

 select * from INTExtraBillItems
  select * from ExtraBillItemsCASH
select * from ExtraBills where ExtraBillNo ='P160001850002'
****************************************************************************************/

