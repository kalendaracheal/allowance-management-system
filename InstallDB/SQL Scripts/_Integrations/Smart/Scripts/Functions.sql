
------------------------------------------------------------------------------------------------------------------------------------
--------Function GetBillToCustomerNo------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetBillToCustomerNo')
drop function GetBillToCustomerNo
go

create function GetBillToCustomerNo(@VistNo varchar(20)) returns varchar(20)
with encryption as

begin

declare @CashBillModesID varchar(10)
declare @AccountBillModesID varchar(10)
declare @InsuranceBillModesID varchar(10)
declare @BillNo varchar(20)
declare @BillInsuranceNo varchar(20)
declare @BillToCustomerNo varchar(20)
declare @BillModesID varchar(10)
----------------------------------------------------------------------------
set @CashBillModesID = dbo.GetLookupDataID('BillModes', 'C')
set @AccountBillModesID = dbo.GetLookupDataID('BillModes', 'A')
set @InsuranceBillModesID = dbo.GetLookupDataID('BillModes', 'I')
----------------------------------------------------------------------------

set @BillNo = (select BillNo from Visits where VisitNo = @VistNo)
set @BillModesID = (select BillModesID from Visits where VisitNo = @VistNo)

if (@BillModesID = @InsuranceBillModesID)
begin
	set @BillToCustomerNo = (select InsuranceSchemes.CompanyNo from SchemeMembers	
				inner join InsuranceSchemes on InsuranceSchemes.CompanyNo = SchemeMembers.CompanyNo
				and InsuranceSchemes.PolicyNo = SchemeMembers.PolicyNo
				inner join Companies on InsuranceSchemes.CompanyNo = Companies.CompanyNo
				where MedicalCardNo = @BillNo)
set @BillToCustomerNo=	UPPER(STUFF(@BillToCustomerNo, 5, 0, '-')) 
end

else if (@BillModesID = @AccountBillModesID)
	begin 
	  
	   set @BillInsuranceNo = (select InsuranceNo from Visits where VisitNo = @VistNo)
       
	  
	  if @BillInsuranceNo is null or @BillInsuranceNo = ''
	     begin  set @BillToCustomerNo =  @BillNo end
	  
	  else begin set @BillToCustomerNo =  @BillInsuranceNo  end
	  set @BillToCustomerNo=	UPPER(STUFF(@BillToCustomerNo, 5, 0, '-')) 
	
	end

else if (@BillModesID = @CashBillModesID)
begin
  set @BillToCustomerNo = (select PatientNo from Visits where VisitNo = @VistNo)
  set @BillToCustomerNo=	UPPER(STUFF(@BillToCustomerNo, 4, 0, '-')) 
end

else 
begin
  set @BillToCustomerNo = (select PatientNo from Visits where VisitNo = @VistNo)
  set @BillToCustomerNo=	UPPER(STUFF(@BillToCustomerNo, 4, 0, '-')) 
end

set @BillToCustomerNo = isnull(@BillToCustomerNo, '')
return @BillToCustomerNo
end
go

/*----------------------------------------------------------------------------------------------------------------------------
print dbo.GetBillToCustomerNo('P160000570001')
print dbo.GetBillToCustomerNo('P16000002001')
print dbo.GetBillToCustomerNo('P16000124001')
print dbo.GetBillToCustomerNo('P1800355890001')
select * from SchemeMembers
--- select top 100 * from Visits where   len(InsuranceNo)>1

----------------------------------------------------------------------------------------------------------------------------*/



--------Function GetInvoiceBillToCustomerNo------------------------------------------------------------------------------------------------
--------Function GetInvoiceBillToCustomerNo------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetInvoiceBillToCustomerNo')
drop function GetInvoiceBillToCustomerNo
go

create function GetInvoiceBillToCustomerNo(@VistNo varchar(20), @PayTypeID varchar(10)) returns varchar(20)
with encryption as

begin
declare @BillToCustomerNo varchar(20)
declare @VisitBillPayTypeID varchar(10)
declare @VisitBillCASHPayTypeID varchar(10)

declare @ExtraBillPayTypeID varchar(10)
declare @ExtraBillCASHPayTypeID varchar(10)


set @VisitBillPayTypeID = dbo.GetLookupDataID('PayType', 'CAS')
set @VisitBillCASHPayTypeID = dbo.GetLookupDataID('PayType', 'VCS')
set @ExtraBillPayTypeID = dbo.GetLookupDataID('PayType', 'EXT')
set @ExtraBillCASHPayTypeID = dbo.GetLookupDataID('PayType', 'EXC')


if @PayTypeID in (@VisitBillCASHPayTypeID, @ExtraBillCASHPayTypeID)
begin
  set @BillToCustomerNo = (select PatientNo  from Visits where VisitNo = @VistNo)
end
else begin set @BillToCustomerNo = dbo.GetBillToCustomerNo(@VistNo) end

set @BillToCustomerNo = isnull(@BillToCustomerNo, '')
return @BillToCustomerNo
end
go

/*----------------------------------------------------------------------------------------------------------------------------
print dbo.GetInvoiceBillToCustomerNo('PLH418295001', '47CAS')
print dbo.GetInvoiceBillToCustomerNo('PLH394642001', '47VCS')
select top 1 * from Invoices
--------------------------------------------------

print dbo.GetInvoiceBillToCustomerNo('PLH394642001', '47CAS')
print dbo.GetInvoiceBillToCustomerNo('PLH394642001', '47VCS')

--------------------------------------------------------------------------------------------------------------------------------*/

--------Function GetBillToCustomerName------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetBillToCustomerName')
drop function GetBillToCustomerName
go

create function GetBillToCustomerName(@VistNo varchar(20)) returns varchar(20)
with encryption as

begin

declare @CashBillModesID varchar(10)
declare @AccountBillModesID varchar(10)
declare @InsuranceBillModesID varchar(10)
declare @BillNo varchar(20)
declare @BillInsuranceNo varchar(20)
declare @BillToCustomerName varchar(60)
declare @BillModesID varchar(10)
----------------------------------------------------------------------------
set @CashBillModesID = dbo.GetLookupDataID('BillModes', 'C')
--set @AccountBillModesID = dbo.GetLookupDataID('BillModes', 'A')
--set @InsuranceBillModesID = dbo.GetLookupDataID('BillModes', 'I')
----------------------------------------------------------------------------

set @BillNo = (select BillNo from Visits where VisitNo = @VistNo)
set @BillModesID = (select BillModesID from Visits where VisitNo = @VistNo)



 if (@BillModesID = @CashBillModesID)
begin
  set @BillToCustomerName = (select dbo.GetFullName(LastName, MiddleName, FirstName) from Patients
  inner join Visits on Visits.PatientNo = Patients.PatientNo
   where VisitNo = @VistNo)end

else 
begin

   set @BillToCustomerName = dbo.GetBillName(@BillModesID, @BillNo)
end

set @BillToCustomerName = isnull(@BillToCustomerName, '')
return @BillToCustomerName
end
go


--------Function GetBillToCustomerNoByExtraBillNo------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetBillToCustomerNoByExtraBillNo')
drop function GetBillToCustomerNoByExtraBillNo
go

create function GetBillToCustomerNoByExtraBillNo(@ExtraBillNo varchar(20)) returns varchar(20)
with encryption as

begin

declare @CashBillModesID varchar(10)
declare @AccountBillModesID varchar(10)
declare @InsuranceBillModesID varchar(10)
declare @BillNo varchar(20)
declare @BillInsuranceNo varchar(20)
declare @BillToCustomerNo varchar(20)
declare @BillModesID varchar(10)
declare @MainMemberNo varchar(20)

----------------------------------------------------------------------------
set @CashBillModesID = dbo.GetLookupDataID('BillModes', 'C')
set @AccountBillModesID = dbo.GetLookupDataID('BillModes', 'A')
set @InsuranceBillModesID = dbo.GetLookupDataID('BillModes', 'I')

----------------------------------------------------------------------------

begin
set @BillNo = (select BillNo from ExtraBills where ExtraBillNo = @ExtraBillNo)
set @BillModesID = (select BillModesID from ExtraBills where ExtraBillNo = @ExtraBillNo)

if (@BillModesID = @InsuranceBillModesID)
begin
	set @BillToCustomerNo = (select InsuranceSchemes.CompanyNo from SchemeMembers	
				inner join InsuranceSchemes on InsuranceSchemes.CompanyNo = SchemeMembers.CompanyNo
				and InsuranceSchemes.PolicyNo = SchemeMembers.PolicyNo
				inner join Companies on InsuranceSchemes.CompanyNo = Companies.CompanyNo
				where MedicalCardNo = @BillNo)
end
else if (@BillModesID = @AccountBillModesID)
	begin 
	  set @BillInsuranceNo = (select InsuranceNo from ExtraBills where ExtraBillNo = @ExtraBillNo)
	  if LEN(@BillInsuranceNo) > 0 begin set @BillToCustomerNo =  @BillInsuranceNo end
	  else begin set @BillToCustomerNo =  @BillNo end

	end
else if (@BillModesID = @CashBillModesID)
begin
  set @BillToCustomerNo = (select PatientNo from Visits inner join ExtraBills on ExtraBills.VisitNo = Visits.VisitNo where ExtraBillNo = @ExtraBillNo)
end
else 
begin
  set @BillToCustomerNo = (select PatientNo from Visits inner join ExtraBills on ExtraBills.VisitNo = Visits.VisitNo where ExtraBillNo = @ExtraBillNo)
end 
end
return @BillToCustomerNo
end
go

/*----------------------------------------------------------------------------------------------------------------------------
print dbo.GetBillToCustomerNoByExtraBillNo('P160009070001')
print dbo.GetBillToCustomerNoByExtraBillNo('P160000200003')
print dbo.GetBillToCustomerNoByExtraBillNo('P160000200009')
print dbo.GetBillToCustomerNoByExtraBillNo('P160001240001')
select * from SchemeMembers

select AdmissionNo, ExtraBillNo, BillModesID, BillNo 
from ExtraBills inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
where BillModesID = '17I' and VisitTypeID= '110OPD'
--- select top 100 * from Visits where BillModesID = '17A'
select * from ExtraBills where BillModesID = '17I' and VisitTypeID = '110OPD'

----------------------------------------------------------------------------------------------------------------------------*/
--------Function GetBillToCustomerNameByExtraBillNo------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'GetBillToCustomerNameByExtraBillNo')
drop function GetBillToCustomerNameByExtraBillNo
go

create function GetBillToCustomerNameByExtraBillNo(@ExtraBillNo varchar(20)) returns varchar(20)
with encryption as

begin

declare @CashBillModesID varchar(10)
declare @AccountBillModesID varchar(10)
declare @InsuranceBillModesID varchar(10)
declare @BillNo varchar(20)
declare @BillInsuranceNo varchar(20)
declare @BillToCustomerName varchar(100)
declare @BillModesID varchar(10)
declare @MainMemberNo varchar(20)
declare @VisitNo varchar(20)
----------------------------------------------------------------------------
set @CashBillModesID = dbo.GetLookupDataID('BillModes', 'C')
set @AccountBillModesID = dbo.GetLookupDataID('BillModes', 'A')
set @InsuranceBillModesID = dbo.GetLookupDataID('BillModes', 'I')

begin
set @VisitNo = (select VisitNo from ExtraBills where ExtraBillNo = @ExtraBillNo)
set @BillNo = (select BillNo from Visits where VisitNo = @VisitNo)
set @BillModesID = (select BillModesID from Visits where VisitNo = @VisitNo)

if (@BillModesID = @InsuranceBillModesID)
begin
	set @BillToCustomerName = (select CompanyName from SchemeMembers	
				inner join InsuranceSchemes on InsuranceSchemes.CompanyNo = SchemeMembers.CompanyNo
				and InsuranceSchemes.PolicyNo = SchemeMembers.PolicyNo
				inner join Companies on InsuranceSchemes.CompanyNo = Companies.CompanyNo
				where MedicalCardNo = @BillNo)
end
else if (@BillModesID = @AccountBillModesID)
	begin 
	  set @BillInsuranceNo = (select InsuranceNo from Visits where VisitNo = @VisitNo)
	  if LEN(@BillInsuranceNo) > 0 begin set @BillToCustomerName =  (select BillCustomerName from BillCustomers where AccountNo = @BillInsuranceNo) end
	  else begin set @BillToCustomerName =  (select BillCustomerName from BillCustomers where AccountNo = @BillNo) end

	end
else if (@BillModesID = @CashBillModesID)
begin
  set @BillToCustomerName = (select dbo.GetFullName(LastName, MiddleName, FirstName) from Visits
                             inner join Patients on Patients.PatientNo  = Visits.PatientNo where VisitNo = @VisitNo)
end

else 
begin
  set @BillToCustomerName = (select dbo.GetFullName(LastName, MiddleName, FirstName) from Visits
                             inner join Patients on Patients.PatientNo  = Visits.PatientNo where VisitNo = @VisitNo)
end 
end
return @BillToCustomerName
end
go

/*----------------------------------------------------------------------------------------------------------------------------
print dbo.GetBillToCustomerNameByExtraBillNo('P16000790001')
print dbo.GetBillToCustomerNameByExtraBillNo('P1800165690001')
print dbo.GetBillToCustomerNameByExtraBillNo('P160086530001')
print dbo.GetBillToCustomerNameByExtraBillNo('PLH003768001')
select * from SchemeMembers

select AdmissionNo, ExtraBillNo, BillModesID, BillNo 
from ExtraBills inner join Admissions on Admissions.VisitNo = ExtraBills.VisitNo
where BillModesID = '17C' and VisitTypeID= '110IPD'
--- select top 100 * from Visits where BillModesID = '17C'
----------------------------------------------------------------------------------------------------------------------------*/
